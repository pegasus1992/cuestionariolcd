-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-08-2016 a las 11:49:23
-- Versión del servidor: 5.5.50-cll
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `liceocd_2016`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AREAS`
--

CREATE TABLE IF NOT EXISTS `AREAS` (
  `id_area` int(2) NOT NULL AUTO_INCREMENT,
  `orden` int(2) DEFAULT NULL,
  `nombre_area` varchar(50) NOT NULL,
  `id_docente` varchar(100) NOT NULL,
  PRIMARY KEY (`id_area`),
  UNIQUE KEY `nombre_area` (`nombre_area`),
  UNIQUE KEY `orden` (`orden`),
  KEY `AREAS_ibfk_1` (`id_docente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `AREAS`
--

INSERT INTO `AREAS` (`id_area`, `orden`, `nombre_area`, `id_docente`) VALUES
(23, 3, 'MATEMÁTICAS Y GEOMETRÍA', '53133678'),
(24, 4, 'CIENCIAS SOCIALES', '1032424628'),
(25, 1, 'CIENCIAS NATURALES', '1015415415'),
(26, 7, 'EDUCACIÓN ARTÍSTICA', '79920877'),
(27, 2, 'HUMANIDADES', '1026275092'),
(28, 6, 'EDUCACIÓN FÍSICA, RECREACIÓN Y DEPORTES', '11443121'),
(29, 10, 'PREESCOLAR', '1022348906'),
(30, 5, 'INFORMÁTICA Y TECNOLOGÍA', '1032386121'),
(31, 9, 'COMPORTAMIENTO Y/OCONVIVENCIA', '1010168725'),
(32, 8, 'EDUCACIÓN RELIGIOSA,ÉTICA Y VALORES', '1026277164');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ASIGNATURAS`
--

CREATE TABLE IF NOT EXISTS `ASIGNATURAS` (
  `id_curso` char(1) NOT NULL,
  `id_grado` int(2) NOT NULL,
  `id_materia` int(2) NOT NULL,
  `id_area` int(2) NOT NULL,
  `id_docente` varchar(12) NOT NULL,
  `carga_academica` decimal(3,1) NOT NULL DEFAULT '0.0',
  `cod_dimension` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_curso`,`id_grado`,`id_materia`,`id_area`),
  KEY `ASIGNATURAS_ibfk_4` (`id_materia`,`id_area`),
  KEY `ASIGNATURAS_ibfk_5` (`id_docente`),
  KEY `cod_dimension` (`cod_dimension`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ASIGNATURAS`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DOCENTES`
--

CREATE TABLE IF NOT EXISTS `DOCENTES` (
  `id_docente` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tid_docente` varchar(30) CHARACTER SET latin1 NOT NULL,
  `nid_docente` varchar(12) CHARACTER SET latin1 NOT NULL,
  `contrasena` varchar(40) CHARACTER SET latin1 NOT NULL,
  `escalafon` varchar(2) CHARACTER SET latin1 NOT NULL,
  `titulo` varchar(100) CHARACTER SET latin1 NOT NULL,
  `universidad` varchar(100) CHARACTER SET latin1 NOT NULL,
  `anio_graduacion` varchar(4) CHARACTER SET latin1 NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `nucleo_familiar` varchar(200) CHARACTER SET latin1 NOT NULL,
  `colegio` varchar(200) CHARACTER SET latin1 NOT NULL,
  `especialidad` varchar(200) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id_docente`),
  KEY `tid_docente` (`tid_docente`,`nid_docente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `DOCENTES`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MATERIAS`
--

CREATE TABLE IF NOT EXISTS `MATERIAS` (
  `codigo_materia` int(2) NOT NULL AUTO_INCREMENT,
  `nombre_materia` varchar(200) NOT NULL,
  `orden` int(2) DEFAULT NULL,
  `id_area` int(2) NOT NULL,
  `tieneCompetencias` int(1) NOT NULL DEFAULT '0',
  `tieneParciales` int(1) NOT NULL DEFAULT '0',
  `tienePruebaPi` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_materia`,`id_area`),
  UNIQUE KEY `nombre_materia` (`nombre_materia`),
  UNIQUE KEY `unique_mat` (`orden`,`id_area`),
  KEY `MATERIAS_ibfk_1` (`id_area`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Volcado de datos para la tabla `MATERIAS`
--

INSERT INTO `MATERIAS` (`codigo_materia`, `nombre_materia`, `orden`, `id_area`, `tieneCompetencias`, `tieneParciales`, `tienePruebaPi`) VALUES
(37, 'MATEMÁTICAS Y GEOMETRÍA', 8, 23, 1, 1, 1),
(38, 'GEOMETRÍA', 9, 23, 1, 1, 0),
(39, 'Filosofía', 15, 24, 1, 1, 0),
(40, 'Ciencias políticas', 16, 24, 0, 0, 0),
(41, 'Ciencias económicas', 14, 24, 0, 0, 0),
(42, 'Historia', 11, 24, 1, 1, 0),
(43, 'Geografía', 12, 24, 1, 1, 0),
(44, 'Constitución', 13, 24, 0, 0, 0),
(45, 'Ciencias sociales', 10, 24, 1, 1, 0),
(46, 'Ciencias naturales', 1, 25, 1, 1, 0),
(47, 'Biología', 4, 25, 1, 1, 1),
(48, 'Química', 2, 25, 1, 1, 1),
(49, 'Física', 3, 25, 1, 1, 1),
(50, 'Música', 19, 26, 0, 0, 0),
(51, 'EXPRESIÓN ARTÍSTICA', 20, 26, 0, 0, 0),
(52, 'Video y fotografía', 21, 26, 0, 0, 0),
(53, 'Lenguaje', 5, 27, 1, 1, 1),
(54, 'Inglés', 6, 27, 1, 1, 0),
(55, 'Francés', 7, 27, 1, 1, 0),
(56, 'EDUCACIÓN FÍSICA, RECREACIÓN Y DEPORTES', 18, 28, 0, 0, 0),
(57, 'Dimensión Corporal', 28, 29, 0, 0, 0),
(58, 'Dimensión comunicativa', 25, 29, 0, 0, 0),
(59, 'Dimensión cognitiva', 27, 29, 0, 0, 0),
(60, 'Dimensión ética y valores huma', 24, 29, 0, 0, 0),
(61, 'Dimensión estética', 26, 29, 0, 0, 0),
(62, 'Dimensión afectiva', 29, 29, 0, 0, 0),
(63, 'Tecnología e informática', 17, 30, 0, 0, 0),
(64, 'COMPORTAMIENTO Y/O CONVIVENCIA', 23, 31, 0, 0, 0),
(65, 'EDUCACIÓN RELIGIOSA,ÉTICA Y VALORES', 22, 32, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PERSONAS`
--

CREATE TABLE IF NOT EXISTS `PERSONAS` (
  `tid` varchar(30) CHARACTER SET latin1 NOT NULL,
  `nid` varchar(12) CHARACTER SET latin1 NOT NULL,
  `nombres` varchar(50) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(50) CHARACTER SET latin1 NOT NULL,
  `lugar_nacimiento` varchar(20) CHARACTER SET latin1 NOT NULL,
  `direccion` varchar(50) CHARACTER SET latin1 NOT NULL,
  `barrio` varchar(50) CHARACTER SET latin1 NOT NULL,
  `telefono` int(7) NOT NULL,
  `sexo` char(1) CHARACTER SET latin1 NOT NULL,
  `correo` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `celular` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `rol` char(2) CHARACTER SET latin1 NOT NULL,
  `estado` int(1) NOT NULL COMMENT '1->Activo;0->inactivo',
  `fecha_registro` date NOT NULL,
  `fecha_inactivo` date DEFAULT NULL,
  PRIMARY KEY (`tid`,`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `PERSONAS`
--


--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `AREAS`
--
ALTER TABLE `AREAS`
  ADD CONSTRAINT `AREAS_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `DOCENTES` (`id_docente`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ASIGNATURAS`
--
ALTER TABLE `ASIGNATURAS`
  ADD CONSTRAINT `ASIGNATURAS_ibfk_1` FOREIGN KEY (`id_curso`, `id_grado`) REFERENCES `CURSOS` (`id_curso`, `id_grado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ASIGNATURAS_ibfk_4` FOREIGN KEY (`id_materia`, `id_area`) REFERENCES `MATERIAS` (`codigo_materia`, `id_area`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ASIGNATURAS_ibfk_5` FOREIGN KEY (`id_docente`) REFERENCES `DOCENTES` (`id_docente`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ASIGNATURAS_ibfk_6` FOREIGN KEY (`cod_dimension`) REFERENCES `DIMENSIONES` (`cod_dimension`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `DOCENTES`
--
ALTER TABLE `DOCENTES`
  ADD CONSTRAINT `DOCENTES_ibfk_1` FOREIGN KEY (`tid_docente`, `nid_docente`) REFERENCES `PERSONAS` (`tid`, `nid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `MATERIAS`
--
ALTER TABLE `MATERIAS`
  ADD CONSTRAINT `MATERIAS_ibfk_1` FOREIGN KEY (`id_area`) REFERENCES `AREAS` (`id_area`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
