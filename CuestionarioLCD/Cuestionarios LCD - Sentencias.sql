-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Areas
--

CREATE TABLE Areas(id_area number(2) NOT NULL);
ALTER TABLE Areas ADD orden number(2) DEFAULT NULL;
ALTER TABLE Areas ADD nombre_area varchar(50) NOT NULL;
ALTER TABLE Areas ADD id_docente varchar(100) NOT NULL;
ALTER TABLE Areas ADD CONSTRAINT areas_pk PRIMARY KEY (id_area);
ALTER TABLE Areas ADD CONSTRAINT nombre_area_uk UNIQUE(nombre_area);
ALTER TABLE Areas ADD CONSTRAINT orden_uk UNIQUE(orden);

--
-- Volcado de datos para la tabla Areas
--

INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (23, 3, 'MATEM�TICAS Y GEOMETR�A', '53133678');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (24, 4, 'CIENCIAS SOCIALES', '1032424628');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (25, 1, 'CIENCIAS NATURALES', '1015415415');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (26, 7, 'EDUCACI�N ART�STICA', '79920877');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (27, 2, 'HUMANIDADES', '1026275092');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (28, 6, 'EDUCACI�N F�SICA, RECREACI�N Y DEPORTES', '11443121');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (29, 10, 'PREESCOLAR', '1022348906');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (30, 5, 'INFORM�TICA Y TECNOLOG�A', '1032386121');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (31, 9, 'COMPORTAMIENTO Y/OCONVIVENCIA', '1010168725');
INSERT INTO Areas (id_area, orden, nombre_area, id_docente) VALUES (32, 8, 'EDUCACI�N RELIGIOSA,�TICA Y VALORES', '1026277164');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Asignaturas
--

CREATE TABLE Asignaturas(id_curso char(1) NOT NULL);
ALTER TABLE Asignaturas ADD id_grado number(2) NOT NULL;
ALTER TABLE Asignaturas ADD id_materia number(2) NOT NULL;
ALTER TABLE Asignaturas ADD id_area number(2) NOT NULL;
ALTER TABLE Asignaturas ADD id_docente varchar(12) NOT NULL;
ALTER TABLE Asignaturas ADD carga_academica number(3,1) DEFAULT 0.0 NOT NULL;
ALTER TABLE Asignaturas ADD cod_dimension number(11) DEFAULT NULL;
ALTER TABLE Asignaturas ADD CONSTRAINT asignaturas_pk PRIMARY KEY (id_curso, id_grado, id_materia, id_area);

--
-- Volcado de datos para la tabla Asignaturas
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Docentes
--

CREATE TABLE Docentes(id_docente varchar(100) NOT NULL);
ALTER TABLE Docentes ADD tid_docente varchar(30) NOT NULL;
ALTER TABLE Docentes ADD nid_docente varchar(12) NOT NULL;
ALTER TABLE Docentes ADD contrasena varchar(40) NOT NULL;
ALTER TABLE Docentes ADD escalafon varchar(2);
ALTER TABLE Docentes ADD titulo varchar(100);
ALTER TABLE Docentes ADD universidad varchar(100);
ALTER TABLE Docentes ADD anio_graduacion varchar(4);
ALTER TABLE Docentes ADD fecha_contratacion varchar(10);
ALTER TABLE Docentes ADD nucleo_familiar varchar(200);
ALTER TABLE Docentes ADD colegio varchar(200);
ALTER TABLE Docentes ADD especialidad varchar(200);
ALTER TABLE Docentes ADD CONSTRAINT docentes_pk PRIMARY KEY (id_docente);
ALTER TABLE Docentes ADD CONSTRAINT docentes_uk UNIQUE (tid_docente, nid_docente);

--
-- Volcado de datos para la tabla Docentes
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Grados
--

CREATE TABLE Grados (id_grado number(2) NOT NULL);
ALTER TABLE Grados ADD cod_grado varchar(10) NOT NULL;
ALTER TABLE Grados ADD nombre varchar(20) NOT NULL;
ALTER TABLE Grados ADD nivel varchar(20) NOT NULL;
ALTER TABLE Grados ADD CONSTRAINT grados_pk PRIMARY KEY(id_grado);
ALTER TABLE Grados ADD CONSTRAINT cod_grado_uk UNIQUE(cod_grado);
ALTER TABLE Grados ADD CONSTRAINT nombre_grado_uk UNIQUE(nombre);

--
-- Volcado de datos para la tabla Grados
--

INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (1, 'P1', 'PRIMERO', 'B�SICA PRIMARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (2, 'P2', 'SEGUNDO', 'B�SICA PRIMARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (3, 'P3', 'TERCERO', 'B�SICA PRIMARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (4, 'P4', 'CUARTO', 'B�SICA PRIMARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (5, 'P5', 'QUINTO', 'B�SICA PRIMARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (6, 'S6', 'SEXTO', 'B�SICA SECUNDARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (7, 'S7', 'S�PTIMO', 'B�SICA SECUNDARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (8, 'S8', 'OCTAVO', 'B�SICA SECUNDARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (9, 'S9', 'NOVENO', 'B�SICA SECUNDARIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (10, 'M10', 'D�CIMO', 'MEDIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (11, 'M11', 'UND�CIMO', 'MEDIA');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (12, 'PRE-JARD', 'PREJARD�N', 'PREESCOLAR');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (13, 'JARD', 'JARD�N', 'PREESCOLAR');
INSERT INTO Grados (id_grado, cod_grado, nombre, nivel) VALUES (14, 'TRAN', 'TRANSICI�N', 'PREESCOLAR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Materias
--

CREATE TABLE Materias (codigo_materia number(2) NOT NULL);
ALTER TABLE Materias ADD nombre_materia varchar(200) NOT NULL;
ALTER TABLE Materias ADD orden number(2) DEFAULT NULL;
ALTER TABLE Materias ADD id_area number(2) NOT NULL;
ALTER TABLE Materias ADD tieneCompetencias number(1) DEFAULT 0 NOT NULL;
ALTER TABLE Materias ADD tieneParciales number(1) DEFAULT 0 NOT NULL;
ALTER TABLE Materias ADD tienePruebaPi number(1) DEFAULT 0 NOT NULL;
ALTER TABLE Materias ADD CONSTRAINT materias_pk UNIQUE (codigo_materia);
ALTER TABLE Materias ADD CONSTRAINT materias_uk UNIQUE (codigo_materia, id_area);
ALTER TABLE Materias ADD CONSTRAINT nombre_materia_materias_uk UNIQUE (nombre_materia);
ALTER TABLE Materias ADD CONSTRAINT unique_mat UNIQUE (orden, id_area);

--
-- Volcado de datos para la tabla Materias
--

INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (37, 'MATEM�TICAS Y GEOMETR�A', 8, 23, 1, 1, 1);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (38, 'GEOMETR�A', 9, 23, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (39, 'FILOSOF�A', 15, 24, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (40, 'CIENCIAS POL�TICAS', 16, 24, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (41, 'CIENCIAS ECON�MICAS', 14, 24, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (42, 'HISTORIA', 11, 24, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (43, 'GEOGRAF�A', 12, 24, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (44, 'CONSTITUCI�N', 13, 24, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (45, 'CIENCIAS SOCIALES', 10, 24, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (46, 'CIENCIAS NATURALES', 1, 25, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (47, 'BIOLOG�A', 4, 25, 1, 1, 1);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (48, 'QU�MICA', 2, 25, 1, 1, 1);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (49, 'F�SICA', 3, 25, 1, 1, 1);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (50, 'M�SICA', 19, 26, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (51, 'EXPRESI�N ART�STICA', 20, 26, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (52, 'VIDEO Y FOTOGRAF�A', 21, 26, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (53, 'LENGUAJE', 5, 27, 1, 1, 1);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (54, 'INGL�S', 6, 27, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (55, 'FRANC�S', 7, 27, 1, 1, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (56, 'EDUCACI�N F�SICA, RECREACI�N Y DEPORTES', 18, 28, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (57, 'DIMENSI�N CORPORAL', 28, 29, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (58, 'DIMENSI�N COMUNICATIVA', 25, 29, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (59, 'DIMENSI�N COGNITIVA', 27, 29, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (60, 'DIMENSI�N �TICA Y VALORES HUMA', 24, 29, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (61, 'DIMENSI�N EST�TICA', 26, 29, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (62, 'DIMENSI�N AFECTIVA', 29, 29, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (63, 'TECNOLOG�A E INFORM�TICA', 17, 30, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (64, 'COMPORTAMIENTO Y/O CONVIVENCIA', 23, 31, 0, 0, 0);
INSERT INTO Materias (codigo_materia, nombre_materia, orden, id_area, tieneCompetencias, tieneParciales, tienePruebaPi) VALUES (65, 'EDUCACI�N RELIGIOSA,�TICA Y VALORES', 22, 32, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Personas
--

CREATE TABLE Personas (tid varchar(30) NOT NULL);
ALTER TABLE Personas ADD nid varchar(12) NOT NULL;
ALTER TABLE Personas ADD nombres varchar(50) NOT NULL;
ALTER TABLE Personas ADD apellidos varchar(50) NOT NULL;
ALTER TABLE Personas ADD lugar_nacimiento varchar(255);
ALTER TABLE Personas ADD direccion varchar(255);
ALTER TABLE Personas ADD barrio varchar(255);
ALTER TABLE Personas ADD telefono number(10);
ALTER TABLE Personas ADD sexo char(1);
ALTER TABLE Personas ADD correo varchar(100) DEFAULT NULL;
ALTER TABLE Personas ADD celular varchar(20) DEFAULT NULL;
ALTER TABLE Personas ADD rol char(2) NOT NULL;
ALTER TABLE Personas ADD estado number(1) NOT NULL;-- 1->Activo;0->inactivo
ALTER TABLE Personas ADD fecha_registro varchar(10) NOT NULL;
ALTER TABLE Personas ADD fecha_inactivo varchar(10) DEFAULT NULL;
ALTER TABLE Personas ADD CONSTRAINT personas_pk PRIMARY KEY (tid, nid);

--
-- Volcado de datos para la tabla Personas
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Temas
--

CREATE TABLE Temas(idTema number NOT NULL);
ALTER TABLE Temas ADD nombre varchar2(255) NOT NULL;
ALTER TABLE Temas ADD idMateria number(2) NOT NULL;
ALTER TABLE Temas ADD temaPadre number DEFAULT NULL;
ALTER TABLE Temas ADD CONSTRAINT temas_pk PRIMARY KEY (idTema);
ALTER TABLE Temas ADD CONSTRAINT nombre_tema_uk UNIQUE (nombre);

--
-- Volcado de datos para la tabla Temas
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Preguntas
--

CREATE TABLE Preguntas(idPregunta number NOT NULL);
ALTER TABLE Preguntas ADD competencia varchar2(30) NOT NULL;
ALTER TABLE Preguntas ADD nivel varchar2(20) NOT NULL;
ALTER TABLE Preguntas ADD idDocente varchar2(100) NOT NULL;
ALTER TABLE Preguntas ADD idGrado number(2) NOT NULL;
ALTER TABLE Preguntas ADD idArea number(2) NOT NULL;
ALTER TABLE Preguntas ADD idMateria number(2) NOT NULL;
ALTER TABLE Preguntas ADD idSubtema number NOT NULL;
ALTER TABLE Preguntas ADD enunciado varchar2(255) NOT NULL;
ALTER TABLE Preguntas ADD imagen varchar2(255);
ALTER TABLE Preguntas ADD enunciado2 varchar2(255);
ALTER TABLE Preguntas ADD fechacreacion date;
ALTER TABLE Preguntas ADD CONSTRAINT preguntas_pk PRIMARY KEY (idPregunta);

--
-- Volcado de datos para la tabla Preguntas
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla Opciones
--

CREATE TABLE Opciones(idOpcion number NOT NULL);
ALTER TABLE Opciones ADD respuesta varchar2(255) NOT NULL;
ALTER TABLE Opciones ADD correcta char(1) DEFAULT 'N' NOT NULL;
ALTER TABLE Opciones ADD idPregunta number NOT NULL;
ALTER TABLE Opciones ADD posicion char(1);
--
-- Volcado de datos para la tabla Opciones
--

-- --------------------------------------------------------

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla Areas
--
ALTER TABLE Areas ADD CONSTRAINT AREAS_ibfk_1 FOREIGN KEY (id_docente) REFERENCES Docentes (id_docente);

--
-- Filtros para la tabla Asignaturas
--
ALTER TABLE Asignaturas ADD CONSTRAINT ASIGNATURAS_ibfk_4 FOREIGN KEY (id_materia, id_area) REFERENCES Materias (codigo_materia, id_area);
ALTER TABLE Asignaturas ADD CONSTRAINT ASIGNATURAS_ibfk_5 FOREIGN KEY (id_docente) REFERENCES Docentes (id_docente);

--
-- Filtros para la tabla Docentes
--
--ALTER TABLE Docentes ADD CONSTRAINT DOCENTES_ibfk_1 FOREIGN KEY (tid_docente, nid_docente) REFERENCES Personas (tid, nid);

--
-- Filtros para la tabla Materias
--
ALTER TABLE Materias ADD CONSTRAINT MATERIAS_ibfk_1 FOREIGN KEY (id_area) REFERENCES Areas (id_area);

--
-- Filtros para la Tabla Temas
--
ALTER TABLE Temas ADD CONSTRAINT temas_materia_fk FOREIGN KEY (idMateria) REFERENCES Materias(codigo_materia);
ALTER TABLE Temas ADD CONSTRAINT temas_padre_fk FOREIGN KEY (temaPadre) REFERENCES Temas(idTema);

--
-- Filtros para la Tabla Preguntas
--

ALTER TABLE Preguntas ADD CONSTRAINT preguntas_docentes_fk FOREIGN KEY (idDocente) REFERENCES Docentes(id_docente);
ALTER TABLE Preguntas ADD CONSTRAINT preguntas_grados_fk FOREIGN KEY (idGrado) REFERENCES Grados(id_grado);
ALTER TABLE Preguntas ADD CONSTRAINT preguntas_areas_fk FOREIGN KEY (idArea) REFERENCES Areas(id_area);
ALTER TABLE Preguntas ADD CONSTRAINT preguntas_materias_fk FOREIGN KEY (idMateria) REFERENCES Materias(codigo_materia);
ALTER TABLE Preguntas ADD CONSTRAINT preguntas_temas_fk FOREIGN KEY (idSubtema) REFERENCES Temas(idTema);

--
-- Filtros para la Tabla Opciones
--

ALTER TABLE Opciones ADD CONSTRAINT opciones_preguntas_fk FOREIGN KEY (idPregunta) REFERENCES Preguntas(idPregunta);





--
-- Estructura de tabla para la tabla CUESTIONARIO
--

CREATE TABLE Cuestionario(idCuestionario number NOT NULL);
ALTER TABLE Cuestionario ADD rutaPi varchar2(500);

ALTER TABLE Cuestionario ADD CONSTRAINT Cuestionario_pk PRIMARY KEY (idCuestionario);
--
-- 
--

--
-- Estructura de tabla para la tabla CUESTIONARIO
--

CREATE TABLE CuestionarioPregunta(idCuestionario number NOT NULL);
ALTER TABLE CuestionarioPregunta ADD idPregunta number NOT NULL;
ALTER TABLE CuestionarioPregunta ADD orden long;

ALTER TABLE CuestionarioPregunta ADD CONSTRAINT CP_pk PRIMARY KEY (idCuestionario,idPregunta);

ALTER TABLE CuestionarioPregunta ADD CONSTRAINT CP_fk_idC FOREIGN KEY (idCuestionario) REFERENCES Cuestionario(idCuestionario);
ALTER TABLE CuestionarioPregunta ADD CONSTRAINT CP_fk_idP FOREIGN KEY (idPregunta) REFERENCES Preguntas(idPregunta);
--
-- 
--



-- --------------------------------------------------------

--
-- Borrado de tablas
--

--DROP TABLE Areas CASCADE CONSTRAINTS;
--DROP TABLE Asignaturas CASCADE CONSTRAINTS;
--DROP TABLE Docentes CASCADE CONSTRAINTS;
--DROP TABLE Grados CASCADE CONSTRAINTS;
--DROP TABLE Materias CASCADE CONSTRAINTS;
--DROP TABLE Personas CASCADE CONSTRAINTS;
--DROP TABLE Temas CASCADE CONSTRAINTS;
--DROP TABLE Preguntas CASCADE CONSTRAINTS;
--DROP TABLE Opciones CASCADE CONSTRAINTS;

-- ---------------------------------------------------------

--
-- Credenciales de la BD
--

-- User: system
-- Clave: CD1986