package view;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.*;

import javax.swing.*;

import model.*;
import controller.*;
import view.utils.*;

public class CrearSubtema extends General {
	private static final long serialVersionUID = -7089217281716002665L;

	// Panel Formulario
	private JPanel panel_formulario;

	// Contenido Formulario
	private JLabel lbl_nombre, lbl_materia, lbl_tema;
	private JTextField txt_nombre;
	private JComboBox<String> combo_materias, combo_temas;

	// Panel Botones
	private JPanel panel_botones;
	private JButton btn_crear, btn_regresar;

	// Utilitarios
	private static final String titulo = "Crear Subtema";
	private CrearTemaController controller;

	@SuppressWarnings("unused")
	private Docente docente;

	/**
	 * Inicializa la ventana de crear subtema.
	 * 
	 * @param anterior
	 *            Ventana anterior.
	 * @param docente
	 */
	public CrearSubtema(JFrame anterior, Docente docente) {
		super(anterior, 3 * anchoPantalla / 9, 5 * altoPantalla / 17);
		this.docente = docente;
		controller = new CrearTemaController();

		prepareElementosPanelFormulario();
		definaAccionesFormulario();
		prepareElementosPanelBotones();
		definaAccionesBotones();
	}

	/**
	 * Prepara los elementos del panel de formulario.
	 */
	private void prepareElementosPanelFormulario() {
		int anchoP = anchoPantalla / 10;
		int x = anchoP / 7, alto = anchoP / 5;
		int aumentoX = anchoP / 5, aumentoY = anchoP / 3, reduccion = anchoP / 10;
		int anchoLbl = 3 * anchoP / 4, anchoTxt = 2 * anchoP;
		int comboGrande = anchoTxt;

		panel_formulario = new JPanel();
		panel_formulario.setLocation(0, panel_barraTitulos.getHeight());
		panel_formulario.setSize(this.getWidth(), this.getHeight()
				- panel_barraTitulos.getHeight() * 2);
		panel_formulario.setLayout(null);
		panel_formulario.setOpaque(false);
		this.add(panel_formulario);

		lbl_nombre = new JLabel("Nombre: ", JLabel.LEFT);
		lbl_nombre.setLocation(x, panel_formulario.getY() - reduccion);
		lbl_nombre.setSize(anchoLbl, alto);
		lbl_nombre.setForeground(letra.getColorLabel());
		lbl_nombre.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_nombre);

		txt_nombre = new JTextField();
		txt_nombre.setLocation(lbl_nombre.getWidth() + aumentoX,
				lbl_nombre.getY());
		txt_nombre.setSize(comboGrande, alto);
		txt_nombre.setFont(letra.getFuenteTxt());
		txt_nombre.setForeground(letra.getColorTxt());
		txt_nombre.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_nombre);

		lbl_materia = new JLabel("Asignatura: ", JLabel.LEFT);
		lbl_materia
				.setLocation(lbl_nombre.getX(), lbl_nombre.getY() + aumentoY);
		lbl_materia.setSize(lbl_nombre.getWidth(), lbl_nombre.getHeight());
		lbl_materia.setForeground(letra.getColorLabel());
		lbl_materia.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_materia);

		combo_materias = new JComboBox<String>();
		combo_materias.setLocation(txt_nombre.getX(), lbl_materia.getY());
		combo_materias.setSize(comboGrande, txt_nombre.getHeight());
		combo_materias.setForeground(letra.getColorCombo());
		combo_materias.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_materias);

		// TODO Poblar combo con materias
		ArrayList<Materia> materias = controller.obtenerMaterias();
		for (int i = 0; i < materias.size(); i++) {
			combo_materias.addItem(materias.get(i).getNombre());
		}
		combo_materias.setSelectedIndex(-1);

		lbl_tema = new JLabel("Tema: ", JLabel.LEFT);
		lbl_tema.setLocation(lbl_materia.getX(), lbl_materia.getY() + aumentoY);
		lbl_tema.setSize(lbl_materia.getWidth(), lbl_materia.getHeight());
		lbl_tema.setForeground(letra.getColorLabel());
		lbl_tema.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_tema);

		combo_temas = new JComboBox<String>();
		combo_temas.setLocation(combo_materias.getX(), lbl_tema.getY());
		combo_temas.setSize(comboGrande, combo_materias.getHeight());
		combo_temas.setForeground(letra.getColorCombo());
		combo_temas.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_temas);
	}

	/**
	 * Define las acciones dentro del panel de formulario.
	 */
	private void definaAccionesFormulario() {
		txt_nombre.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent evt) {
				char KeyChar = evt.getKeyChar();
				if (Character.isLowerCase(KeyChar)) {
					evt.setKeyChar(Character.toUpperCase(KeyChar));
				}
			}

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_ENTER:
				case KeyEvent.VK_DOWN: {
					combo_materias.grabFocus();
				}
					break;
				}
			}
		});

		combo_materias.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					txt_nombre.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_temas.grabFocus();
				}
					break;
				}
			}
		});
		combo_materias.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent evt) {
				// TODO Poblar combo con temas
				combo_temas.removeAllItems();
				int materiaSeleccionada = combo_materias.getSelectedIndex();
				if (materiaSeleccionada != -1) {
					ArrayList<Tema> temas = controller
							.obtenerTemas(combo_materias
									.getItemAt(materiaSeleccionada));
					for (int i = 0; i < temas.size(); i++) {
						combo_temas.addItem(temas.get(i).getNombre());
					}
					combo_temas.setSelectedIndex(-1);
				}
			}
		});

		combo_temas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_materias.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					btn_crear.grabFocus();
				}
					break;
				}
			}
		});
	}

	/**
	 * Prepara los elementos del panel de botones.
	 */
	private void prepareElementosPanelBotones() {
		int anchoBtn = 150;
		int altoBtn = 30;

		panel_botones = new JPanel();
		panel_botones.setLocation(panel_formulario.getWidth() / 4,
				panel_formulario.getHeight());
		panel_botones.setSize(panel_formulario.getWidth() / 2, altoBtn);
		panel_botones.setLayout(new GridLayout(1, 0));
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_crear = new Boton(0, 0, anchoBtn, altoBtn, "Crear Subtema",
				panel_botones, this) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonCrearSubtema();
			}
		};

		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar",
				panel_botones, this) {
			private static final long serialVersionUID = -4456446890553142037L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
	}

	/**
	 * Define las acciones dentro del panel de botones.
	 */
	private void definaAccionesBotones() {
		btn_crear.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					combo_temas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT: {
					btn_regresar.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_crear.doClick();
				}
					break;
				}
			}
		});

		btn_regresar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					btn_crear.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_regresar.doClick();
				}
					break;
				}
			}
		});
	}

	/**
	 * Permite crear el subtema.
	 */
	private void accionBotonCrearSubtema() {// TODO Boton Crear Subtema
		String nombre = txt_nombre.getText().trim();

		int materiaSeleccionada = combo_materias.getSelectedIndex();
		int temaSeleccionado = combo_temas.getSelectedIndex();
		if (!nombre.isEmpty() && materiaSeleccionada != -1
				&& temaSeleccionado != -1) {

			Materia materia = controller.obtenerMateria(combo_materias.getItemAt(materiaSeleccionada));			
			Tema tema = controller.obtenerTema(combo_temas.getItemAt(temaSeleccionado));
			int id = controller.obtenerIdMaximoTema();
			try {
				controller.agregarTemaHijo(id, nombre, materia, tema);
				JOptionPane.showMessageDialog(null,
						"Subtema guardado exitosamente", "Información",
						JOptionPane.INFORMATION_MESSAGE);
				cerrarVentana();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error",
						JOptionPane.INFORMATION_MESSAGE);
				e.printStackTrace();
			}

		} else {
			if (nombre.isEmpty()) {
				JOptionPane.showOptionDialog(null,
						"Debe ingresar el nombre del subtema.", "Falta nombre",
						JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE,
						null, null, "aceptar");
				txt_nombre.grabFocus();
			} else if (materiaSeleccionada == -1) {
				JOptionPane.showOptionDialog(null,
						"Debe seleccionar la materia del subtema.",
						"Falta materia", JOptionPane.DEFAULT_OPTION,
						JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_materias.grabFocus();
			} else if (temaSeleccionado == -1) {
				JOptionPane
						.showOptionDialog(
								null,
								"Debe seleccionar el tema al que pertenecerá el subtema.",
								"Falta tema", JOptionPane.DEFAULT_OPTION,
								JOptionPane.ERROR_MESSAGE, null, null,
								"aceptar");
				combo_materias.grabFocus();
			}
		}
	}

	/**
	 * Regresar a la ventana anterior.
	 */
	private void accionBotonRegresar() {// TODO Boton Regresar
		cerrarVentana();
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
	}
}
