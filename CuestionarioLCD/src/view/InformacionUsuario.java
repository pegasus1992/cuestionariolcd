package view;

import java.awt.event.*;

import javax.swing.*;

import model.*;
import controller.*;
import view.utils.*;

public class InformacionUsuario extends JDialog {
	private static final long serialVersionUID = -8226678755294956807L;

	protected static final String imagenFondo = "imagenes/fondo.png";

	private JLabel lbl_nombres, lbl_apellidos, lbl_correo, lbl_especialidad;
	private JLabel txt_nombres, txt_apellidos, txt_correo, txt_especialidad;

	private JButton btn_informacion;
	private JPanel panel_informacion;
	private JFrame frame;

	// Utilitarios
	private Docente docente;
	private static Letras letra;
	private InformacionUsuarioController controller;

	public InformacionUsuario(int x, int y, int width, int height,
			Docente docente, JFrame frame) {
		this.docente = docente;
		letra = new Letras();
		controller = new InformacionUsuarioController();
		this.frame = frame;

		prepareDialogo(x, y, width, height);
		prepareElementos();
		prepareBoton(x, y);
	}

	private void prepareBoton(int x, int y) {
		btn_informacion = new JButton("Info");
		btn_informacion.setLocation(x, y);
		btn_informacion.setSize(50, 25);
		btn_informacion.setFocusable(false);
		frame.add(btn_informacion);

		btn_informacion.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseExited(MouseEvent e) {
				setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				repaint();
				setVisible(true);
			}
		});
	}

	private void prepareDialogo(int x, int y, int width, int height) {
		setLocation(frame.getX() + frame.getWidth() - x, frame.getY() + y + 25);
		setSize(frame.getWidth() * 6 / 8, height * 9 / 7);

		setLayout(null);
		setUndecorated(true);
		setAlwaysOnTop(true);
		setFocusable(false);
		setVisible(false);

		BackImage.setFondoDialog(imagenFondo, this.getWidth(),
				this.getHeight(), this);

		panel_informacion = new JPanel();
		panel_informacion.setLocation(0, 0);
		panel_informacion.setSize(this.getWidth(), this.getHeight());
		panel_informacion.setLayout(null);
		panel_informacion.setOpaque(false);
		panel_informacion.setBorder(new BordeTitulado(""));
		this.add(panel_informacion);
	}

	private void prepareElementos() {
		Persona persona = controller.obtenerPersona(docente);

		int anchoP = General.anchoPantalla / 10;
		int x = anchoP / 7, alto = anchoP / 4;
		int aumentoX = anchoP / 5, aumentoY = anchoP / 3;
		int anchoLbl = anchoP, anchoTxt = 25 * anchoP;

		lbl_nombres = new JLabel("Nombres: ", JLabel.LEFT);
		panel_informacion.add(lbl_nombres);
		lbl_nombres.setLocation(x, panel_informacion.getY());
		lbl_nombres.setSize(anchoLbl, alto);
		lbl_nombres.setForeground(letra.getColorLabel());
		lbl_nombres.setFont(letra.getFuenteLabel());

		txt_nombres = new JLabel(persona.getNombres(), JLabel.LEFT);
		panel_informacion.add(txt_nombres);
		txt_nombres.setLocation(lbl_nombres.getWidth() + aumentoX,
				lbl_nombres.getY());
		txt_nombres.setSize(anchoTxt, alto);
		txt_nombres.setForeground(letra.getColorLabelVariable());
		txt_nombres.setFont(letra.getFuenteLabelVariable());

		lbl_apellidos = new JLabel("Apellidos: ", JLabel.LEFT);
		panel_informacion.add(lbl_apellidos);
		lbl_apellidos.setLocation(lbl_nombres.getX(), lbl_nombres.getY()
				+ aumentoY);
		lbl_apellidos.setSize(lbl_nombres.getWidth(), lbl_nombres.getHeight());
		lbl_apellidos.setForeground(letra.getColorLabel());
		lbl_apellidos.setFont(letra.getFuenteLabel());

		txt_apellidos = new JLabel(persona.getApellidos(), JLabel.LEFT);
		panel_informacion.add(txt_apellidos);
		txt_apellidos.setLocation(txt_nombres.getX(), lbl_apellidos.getY());
		txt_apellidos.setSize(txt_nombres.getWidth(), txt_nombres.getHeight());
		txt_apellidos.setForeground(letra.getColorLabelVariable());
		txt_apellidos.setFont(letra.getFuenteLabelVariable());

		lbl_correo = new JLabel("Correo: ", JLabel.LEFT);
		panel_informacion.add(lbl_correo);
		lbl_correo.setLocation(lbl_apellidos.getX(), lbl_apellidos.getY()
				+ aumentoY);
		lbl_correo.setSize(lbl_apellidos.getWidth(), lbl_apellidos.getHeight());
		lbl_correo.setForeground(letra.getColorLabel());
		lbl_correo.setFont(letra.getFuenteLabel());

		txt_correo = new JLabel(persona.getCorreo(), JLabel.LEFT);
		panel_informacion.add(txt_correo);
		txt_correo.setLocation(txt_apellidos.getX(), lbl_correo.getY());
		txt_correo.setSize(txt_apellidos.getWidth(), txt_apellidos.getHeight());
		txt_correo.setForeground(letra.getColorLabelVariable());
		txt_correo.setFont(letra.getFuenteLabelVariable());

		lbl_especialidad = new JLabel("Especialidad: ", JLabel.LEFT);
		panel_informacion.add(lbl_especialidad);
		lbl_especialidad.setLocation(lbl_correo.getX(), lbl_correo.getY()
				+ aumentoY);
		lbl_especialidad.setSize(lbl_correo.getWidth(), lbl_correo.getHeight());
		lbl_especialidad.setForeground(letra.getColorLabel());
		lbl_especialidad.setFont(letra.getFuenteLabel());

		txt_especialidad = new JLabel(docente.getEspecialidad(), JLabel.LEFT);
		panel_informacion.add(txt_especialidad);
		txt_especialidad
				.setLocation(txt_correo.getX(), lbl_especialidad.getY());
		txt_especialidad.setSize(txt_correo.getWidth(), txt_correo.getHeight());
		txt_especialidad.setForeground(letra.getColorLabelVariable());
		txt_especialidad.setFont(letra.getFuenteLabelVariable());
	}
}
