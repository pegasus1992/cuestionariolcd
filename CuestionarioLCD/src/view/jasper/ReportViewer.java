package view.jasper;

import java.awt.*;

import javax.swing.*;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.*;
import net.sf.jasperreports.view.save.*;

public class ReportViewer extends JFrame {
	private static final long serialVersionUID = 8809045998247567142L;

	private JRViewer jrViewer;

	public ReportViewer(JasperPrint reportFilled, String title) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double ancho = screenSize.getWidth();
		double altura = screenSize.getHeight();
		this.setSize((int) ancho - 50, (int) altura - 50);

		this.setTitle(title + " - Vista Previa");
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setExtendedState(Frame.MAXIMIZED_BOTH);

		jrViewer = new ReportPanelViewer(reportFilled);
		this.add(jrViewer);
	}

	private class ReportPanelViewer extends JRViewer {
		private static final long serialVersionUID = 7149138302657273528L;

		public ReportPanelViewer(JasperPrint reportFilled) {
			super(reportFilled);
			JRSaveContributor[] contrbs = this.getSaveContributors();
			for (JRSaveContributor saveContributor : contrbs) {
				if (!(saveContributor instanceof JRPdfSaveContributor)) {
					this.removeSaveContributor(saveContributor);
				}
			}
		}
	}
}
