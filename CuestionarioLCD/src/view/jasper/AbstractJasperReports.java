package view.jasper;

import java.io.*;
import java.sql.Connection;
import java.util.*;

import javax.swing.*;

import persistance.Oracle;


import exception.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.*;
import net.sf.jasperreports.engine.util.*;
import net.sf.jasperreports.engine.xml.*;
import net.sf.jasperreports.view.JasperViewer;

public abstract class AbstractJasperReports {

	public static final String mensajeErrorGenerarDisenio = "Ha ocurrido un error al traer el dise�o del reporte solicitado.";


	/** Archivo editor de formatos */
	public static final String rutaEditorFormatos = "formatos/iReport-5.6.0/bin/ireport.exe";

	// Paginas de reportes
	public static final String verCuestionario = "verCuestionario";
	public static final String verPregunta = "verPregunta";
	
	private static JasperReport	report;
	private static JasperPrint	reportFilled;
	private static net.sf.jasperreports.view.JasperViewer	viewer;

	/**
	 * Crea un reporte jasper a partir del contenido de una fuente de datos de
	 * Java.
	 * 
	 * @param dataSource
	 *            <p>
	 *            Contenido de la fuente de datos de Java. Por ejemplo:
	 *            </p>
	 *            <p>
	 *            <code>new JRTableModelDataSource( JTable.getModel() )</code>
	 *            </p>
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param file
	 *            <p>
	 *            Archivo del reporte (sin extension alguna <b>(ni .jrxml ni
	 *            .jasper ni ninguna otra :D )</b> ).
	 *            </p>
	 * @return
	 */
	private static JasperPrint createReport(JRDataSource dataSource,
			Map<String, Object> parameters, String file) {
		JasperPrint reportFilled = null;
		try {
			JasperReport report = compileXMLFile(parameters, file);
			reportFilled = JasperFillManager.fillReport(report, parameters,
					dataSource);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return reportFilled;
	}

	/**
	 * Crea un reporte jasper.
	 * 
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param file
	 *            <p>
	 *            Archivo del reporte (sin extension alguna <b>(ni .jrxml ni
	 *            .jasper ni ninguna otra :D )</b> ).
	 *            </p>
	 * @return
	 */
	private static JasperPrint createReport(Map<String, Object> parameters,
			String file) {
		JasperPrint reportFilled = null;
		try {
			JasperReport report = compileXMLFile(parameters, file);
			reportFilled = JasperFillManager.fillReport(report, parameters);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return reportFilled;
	}

	/**
	 * Compila los archivos XML.
	 * 
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param file
	 *            <p>
	 *            Archivo del reporte (sin extension alguna <b>(ni .jrxml ni
	 *            .jasper ni ninguna otra :D )</b> ).
	 *            </p>
	 * @return
	 * @throws JasperException
	 */
	private static JasperReport compileXMLFile(Map<String, Object> parameters,
			String file) throws JasperException {
		String fichero = Oracle.rutaRepositorioBaseReportes;
		String archivoJrxml = fichero + addExtensionJrxml(file);
		String archivoJasper = fichero + addExtensionJasper(file);

		try {
			// Compila el reporte ingresado.
			JasperDesign fileDesign = JRXmlLoader.load(archivoJrxml);
			JasperCompileManager.compileReportToFile(fileDesign, archivoJasper);
		} catch (JRException ex) {
			throw new JasperException(JasperException.mensajeErrorCompilacion);
		}
		try {
			return (JasperReport) JRLoader.loadObjectFromFile(archivoJasper);
		} catch (JRException ex) {
			throw new JasperException(JasperException.mensajeErrorCarga);
		}
	}

	/**
	 * Muestra en pantalla el contenido del reporte, a partir de una fuente de
	 * datos, con adiciones al nombre del archivo.
	 * 
	 * @param dataSource
	 *            <p>
	 *            Contenido de la fuente de datos de Java. Por ejemplo:
	 *            </p>
	 *            <p>
	 *            <code>new JRTableModelDataSource( JTable.getModel() )</code>
	 *            </p>
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param pagina
	 *            <p>
	 *            Nombre del archivo del reporte a mostrar.
	 *            </p>
	 * @param reporte
	 *            <p>
	 *            Nombre del reporte a mostrar.
	 *            </p>
	 * @param filled
	 *            <p>
	 *            Indica si se ha creado exitosamente la fuente de datos.
	 *            </p>
	 * @throws JasperException
	 */
	protected static void showReport(JRDataSource dataSource,
			Map<String, Object> parameters, String pagina, String reporte,
			boolean filled) throws JasperException {
		if (filled) {
			JasperPrint reportFilled = createReport(dataSource, parameters,
					pagina);
			if (reportFilled != null) {
				new ReportViewer(reportFilled, reporte);
			}
		} else {
			throw new JasperException(JasperException.mensajeErrorTablaVacia);
		}
	}

	/**
	 * Muestra en pantalla el contenido del reporte.
	 * 
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param pagina
	 *            <p>
	 *            Nombre del archivo del reporte a mostrar.
	 *            </p>
	 * @param reporte
	 *            <p>
	 *            Nombre del reporte a mostrar.
	 *            </p>
	 * @throws JasperException
	 */
	protected static void showReport(Map<String, Object> parameters,
			String pagina, String reporte) throws JasperException {

		JasperPrint reportFilled = createReport(parameters, pagina);
		if (reportFilled != null) {
			new ReportViewer(reportFilled, reporte);
		}
	}

	/**
	 * Exporta un reporte a PDF.
	 * 
	 * @param dataSource
	 *            <p>
	 *            Contenido de la fuente de datos de Java. Por ejemplo:
	 *            </p>
	 *            <p>
	 *            <code>new JRTableModelDataSource( JTable.getModel() )</code>
	 *            </p>
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param pagina
	 *            <p>
	 *            Nombre del archivo del reporte a mostrar.
	 *            </p>
	 * @param reporte
	 *            <p>
	 *            Nombre del reporte a mostrar.
	 *            </p>
	 * @param filled
	 *            <p>
	 *            Indica si se ha creado exitosamente la fuente de datos.
	 *            </p>
	 * @param destination
	 *            <p>
	 *            Archivo de destino del reporte.
	 *            </p>
	 * @throws JasperException
	 */
	protected static void exportToPDF(JRDataSource dataSource,
			Map<String, Object> parameters, String pagina, String reporte,
			boolean filled, String destination) throws JasperException {
		if (filled) {
			JasperPrint reportFilled = createReport(dataSource, parameters,
					pagina);
			if (reportFilled != null) {
				try {
					JasperExportManager.exportReportToPdfFile(reportFilled,
							destination);
				} catch (JRException ex) {
					throw new JasperException(ex.getMessage());
				}
			}
		} else {
			throw new JasperException(JasperException.mensajeErrorTablaVacia);
		}
	}

	/**
	 * Exporta un reporte a PDF.
	 * 
	 * @param parameters
	 *            <p>
	 *            Parametros del reporte.
	 *            </p>
	 * @param pagina
	 *            <p>
	 *            Nombre del archivo del reporte a mostrar.
	 *            </p>
	 * @param reporte
	 *            <p>
	 *            Nombre del reporte a mostrar.
	 *            </p>
	 * @param destination
	 *            <p>
	 *            Archivo de destino del reporte.
	 *            </p>
	 * @throws JasperException
	 */
	protected static void exportToPDF(Map<String, Object> parameters,
			String pagina, String reporte, String destination)
			throws JasperException {

		JasperPrint reportFilled = createReport(parameters, pagina);
		if (reportFilled != null) {
			try {
				JasperExportManager.exportReportToPdfFile(reportFilled,
						destination);
			} catch (JRException ex) {
				throw new JasperException(ex.getMessage());
			}
		}
	}

	/**
	 * Abre el programa editor de reportes.
	 * 
	 * @param file
	 *            <p>
	 *            Nombre del archivo del reporte a mostrar.
	 *            </p>
	 */
	public static void openEditor(String file) {
		try {
			File editor = new File(rutaEditorFormatos);
			File destino = new File(Oracle.rutaRepositorioBaseReportes + file);
			String[] command = { "cmd", "/c", "start",
					editor.getAbsolutePath(), destino.getAbsolutePath() };
			Runtime.getRuntime().exec(command);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, mensajeErrorGenerarDisenio,
					"Error al Abrir", JOptionPane.ERROR_MESSAGE);
			ex.printStackTrace();
		}
	}

	/**
	 * Le adiciona al final la extension <b>.jrxml</b> al archivo ingresado.
	 * 
	 * @param file
	 *            <p>
	 *            Nombre del archivo sin extension alguna.
	 *            </p>
	 * @return <p>
	 *         Nombre del archivo con la extension <b>.jrxml</b> adicionada.
	 *         </p>
	 */
	public static String addExtensionJrxml(String file) {
		return file + ".jrxml";
	}

	/**
	 * Le adiciona al final la extension <b>.jasper</b> al archivo ingresado.
	 * 
	 * @param file
	 *            <p>
	 *            Nombre del archivo sin extension alguna.
	 *            </p>
	 * @return <p>
	 *         Nombre del archivo con la extension <b>.jasper</b> adicionada.
	 *         </p>
	 */
	protected static String addExtensionJasper(String file) {
		return file + ".jasper";
	}

	/**
	 * Le adiciona al final la extension <b>.pdf</b> al archivo ingresado.
	 * 
	 * @param file
	 *            <p>
	 *            Nombre del archivo sin extension alguna.
	 *            </p>
	 * @return <p>
	 *         Nombre del archivo con la extension <b>.pdf</b> adicionada.
	 *         </p>
	 */
	protected static String addExtensionPdf(String file) {
		return file + ".pdf";
	}
	
	
	
	
	
	/**
	 * Crear reporte  2
	 * @param path
	 * @param arreglo
	 */
	public static void  createReportMec(Map<String, Object> arreglo ){
		Oracle oracle = Oracle.getInstance();
		oracle.conectar();
		Connection con = (Connection) oracle.getConexion();
		try {
			report = (JasperReport) JRLoader.loadObjectFromFile( Oracle.rutaRepositorioBaseReportes+"/MEC.jasper" );
			reportFilled = JasperFillManager.fillReport( report, arreglo, con );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
		oracle.desconectar();
	}

	/**
	 * Ver reporte MEC
	 */
	public static void showViewerMEC(){
		viewer = new JasperViewer( reportFilled, false );
		viewer.setVisible( true );
	}
}
