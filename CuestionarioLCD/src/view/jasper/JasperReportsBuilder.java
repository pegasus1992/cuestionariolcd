package view.jasper;

import java.io.*;
import java.sql.ResultSet;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import persistance.*;
import exception.*;
import view.utils.*;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.*;

public class JasperReportsBuilder extends AbstractJasperReports {

	private static final String logoColegio = "imagenes/escudo.png";

	private Map<String, Object> parameters;
	private String reporte, pagina, hoy;
	private Fecha fecha;
	private JRDataSource dataSource;

	private Docente docente;
	private Oracle oracle;

	public JasperReportsBuilder(Docente docente) {
		this.docente = docente;

		fecha = new Fecha();
		oracle = Oracle.getInstance();
		dataSource = null;

		reporte = "";
		pagina = "";
		hoy = fecha.fechaHoraActual();
	}

	/**
	 * Mapea los datos del colegio.
	 * 
	 * @return Mapa con los datos del colegio.
	 */
	private static Map<String, Object> mapearDatosColegio() {
		// TODO Mapear datos del colegio

		Map<String, Object> param = new HashMap<String, Object>();

		String logo = logoColegio;
		File fileLogo = new File(logo);
		if (Utils.stringIsNotNull(logo) && fileLogo.exists()) {
			param.put("logoColegio", fileLogo.getAbsolutePath());
		}

		param.put("nombreColegio", "Liceo Campo David");

		return param;
	}

	/**
	 * Mostrar el reporte de preguntas.
	 * 
	 * @param pregunta
	 * @param grado
	 * @param area
	 * @param materia
	 * @param tema
	 * @param subtema
	 * @param opciones
	 * @throws JasperException
	 */
	public void verPreguntasFiller(Pregunta pregunta, Grado grado, Area area,
			Materia materia, Tema tema, Tema subtema, ArrayList<Opcion> opciones)
			throws JasperException {
		// TODO Ver Preguntas

		reporte = "Ver Pregunta N� " + pregunta.getId();
		pagina = verPregunta;

		parameters = mapearDatosColegio();
		parameters.put("hoy", hoy);
		parameters.put("nombreReporte", reporte);
		boolean filled = false;

		parameters.put("codigoGrado", grado.getCodigo());
		parameters.put("nombreGrado", grado.getNombre());
		parameters.put("nivelGrado", grado.getNivel());

		parameters.put("nombreArea", area.getNombre());
		parameters.put("nombreAsignatura", materia.getNombre());
		parameters.put("nombreTema", tema.getNombre());
		parameters.put("nombreSubtema", subtema.getNombre());

		parameters.put("idPregunta", pregunta.getId());
		parameters.put("competenciaPregunta", pregunta.getCompetencia());
		parameters.put("nivelPregunta", pregunta.getNivel());
		parameters.put("enunciadoPregunta", pregunta.getEnunciado());
		parameters.put("enunciado2Pregunta", pregunta.getEnunciadoEspecifico());

		String imagenPregunta = pregunta.getRutaImagen();
		if (imagenPregunta != null) {
			File imagenPreg = new File(imagenPregunta);
			if (Utils.stringIsNotNull(imagenPregunta) && imagenPreg.exists()) {
				parameters.put("imagenPregunta", imagenPregunta);
			}
		}

		Persona persona = obtenerPersona(docente);
		parameters.put("tidDocente", persona.getTid());
		parameters.put("nidDocente", persona.getNid());
		parameters.put("nombresDocente", persona.getNombres());
		parameters.put("apellidosDocente", persona.getApellidos());

		DefaultTableModel dtmTablaOpciones = new DefaultTableModel();
		JTable tablaOpciones = new JTable(dtmTablaOpciones);

		dtmTablaOpciones.addColumn("idOpcion");
		int columnaId = 0;
		dtmTablaOpciones.addColumn("respuesta");
		int columnaRespuesta = 1;
		dtmTablaOpciones.addColumn("correcta");
		int columnaCorrecta = 2;

		if (opciones.size() > 0) {
			for (Opcion opcion : opciones) {
				Object[] temp = new Object[3];

				temp[columnaId] = opcion.getId();
				temp[columnaRespuesta] = opcion.getRespuesta();
				temp[columnaCorrecta] = opcion.getCorrecta();

				dtmTablaOpciones.addRow(temp);
			}
			dataSource = new JRTableModelDataSource(tablaOpciones.getModel());
			filled = true;
		}

		showReport(dataSource, parameters, pagina, reporte, filled);
	}

	/**
	 * Obtiene los datos basicos del docente.
	 * 
	 * @param docente
	 * @return
	 */
	private Persona obtenerPersona(Docente docente) {
		Persona persona = null;

		oracle.conectar();

		String sql = "SELECT * FROM Personas " + "WHERE nid = '"
				+ docente.getId() + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				persona = new Persona(rs.getString("tid"), rs.getString("nid"),
						rs.getString("nombres"), rs.getString("apellidos"),
						rs.getString("lugar_nacimiento"),
						rs.getString("direccion"), rs.getString("barrio"),
						rs.getString("telefono"), rs.getString("sexo"),
						rs.getString("correo"), rs.getString("celular"),
						rs.getString("rol"), rs.getString("estado"),
						rs.getString("fecha_registro"),
						rs.getString("fecha_inactivo"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return persona;
	}

	/**
	 * Muestra el cuestionario ensamblado en pantalla.
	 * 
	 * @param area
	 * @param asignatura
	 * @param grado
	 * @param preguntas
	 * @throws JasperException
	 */
	public void verCuestionarioFiller(String area, String asignatura,String grado, ArrayList<Map<String, String>> preguntas)
			throws JasperException {
		// TODO Ver Cuestionario

		reporte = "Ver Cuestionario";
		pagina = verCuestionario;

		parameters = mapearDatosColegio();
		parameters.put("hoy", hoy);
		parameters.put("nombreReporte", reporte);
		boolean filled = false;

		parameters.put("nombreArea", area);
		parameters.put("nombreAsignatura", asignatura);
		parameters.put("nombreGrado", grado);

		DefaultTableModel dtmTablaPreguntas = new DefaultTableModel();
		JTable tablaPreguntas = new JTable(dtmTablaPreguntas);

		dtmTablaPreguntas.addColumn("N�m. Pregunta");
		int columnaNumPregunta = 0;
		dtmTablaPreguntas.addColumn("Tema");
		int columnaTema = 1;
		dtmTablaPreguntas.addColumn("Subtema");
		int columnaSubtema = 2;
		dtmTablaPreguntas.addColumn("Competencia");
		int columnaCompetencia = 3;
		dtmTablaPreguntas.addColumn("Nivel");
		int columnaNivel = 4;
		dtmTablaPreguntas.addColumn("Enunc. General");
		int columnaEnuncGeneral = 5;
		dtmTablaPreguntas.addColumn("Enunc. Espec�fico");
		int columnaEnuncEspecifico = 6;
		dtmTablaPreguntas.addColumn("Ruta Imagen");
		int columnaRutaImagen = 7;
		dtmTablaPreguntas.addColumn("A");
		int columnaA = 8;
		dtmTablaPreguntas.addColumn("Opci�n A");
		int columnaOpcionA = 9;
		dtmTablaPreguntas.addColumn("B");
		int columnaB = 10;
		dtmTablaPreguntas.addColumn("Opci�n B");
		int columnaOpcionB = 11;
		dtmTablaPreguntas.addColumn("C");
		int columnaC = 12;
		dtmTablaPreguntas.addColumn("Opci�n C");
		int columnaOpcionC = 13;
		dtmTablaPreguntas.addColumn("D");
		int columnaD = 14;
		dtmTablaPreguntas.addColumn("Opci�n D");
		int columnaOpcionD = 15;
		dtmTablaPreguntas.addColumn("E");
		int columnaE = 16;
		dtmTablaPreguntas.addColumn("Opci�n E");
		int columnaOpcionE = 17;

		if (preguntas.size() > 0) {
			for (Map<String, String> pregunta : preguntas) {
				Object[] temp = new Object[dtmTablaPreguntas.getColumnCount()];

				temp[columnaNumPregunta] = pregunta.get("numeroPregunta");
				temp[columnaTema] = pregunta.get("tema");
				temp[columnaSubtema] = pregunta.get("subtema");
				temp[columnaCompetencia] = pregunta.get("competencia");
				temp[columnaNivel] = pregunta.get("nivel");
				temp[columnaEnuncGeneral] = pregunta.get("enunciadoGeneral");
				temp[columnaEnuncEspecifico] = pregunta
						.get("enunciadoEspecifico");
				temp[columnaRutaImagen] = pregunta.get("rutaImagen");
				temp[columnaA] = pregunta.get("A");
				temp[columnaOpcionA] = pregunta.get("opcionA");
				temp[columnaB] = pregunta.get("B");
				temp[columnaOpcionB] = pregunta.get("opcionB");
				temp[columnaC] = pregunta.get("C");
				temp[columnaOpcionC] = pregunta.get("opcionC");
				temp[columnaD] = pregunta.get("D");
				temp[columnaOpcionD] = pregunta.get("opcionD");
				temp[columnaE] = pregunta.get("E");
				temp[columnaOpcionE] = pregunta.get("opcionE");

				dtmTablaPreguntas.addRow(temp);
			}
			dataSource = new JRTableModelDataSource(tablaPreguntas.getModel());
			filled = true;
		}

		showReport(dataSource, parameters, pagina, reporte, filled);
	}
}
