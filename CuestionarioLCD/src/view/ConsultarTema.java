package view;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controller.Filtro;
import controller.CrearTemaController;
import model.Docente;
import model.Materia;
import view.utils.Boton;
import view.utils.General;
import view.utils.MiRender;
import view.utils.TablaNoEditable;

public class ConsultarTema extends General {

	private JFrame frame;
	private JTextField txt_tema, txt_idTema, txt_nombreTema;
	private JLabel lbl_nombreB,lbl_nombreM , lbl_asignatura;
	private JComboBox<String> combo_Asignatura;
	private JPanel panel_botones;
	
	private static final String titulo = "Consultar Tema";
	private CrearTemaController controller;
	///////////////////PANEL PREPARA TABLA PRINCIPAL///////////////////////
	public DefaultTableModel dtmTablaPrincipal;
	public JTable tablaPrincipal;
	public JScrollPane scrollpanePrincipal;
	public String DatosTablaPrincipal[];
	public TableRowSorter<DefaultTableModel> trsfiltrotablaPrincipal;
	
	private JButton btn_cancelar, btn_guardar, btn_eliminar, btn_modificar, btn_regresar;
	
	public Filtro filtros;
		
	int id =0;
	int nombre =1;
	int asignatura =2;
	
	private static final long serialVersionUID = -4054532557621116059L;

	public ConsultarTema(JFrame anterior, Docente docente) {
		super(anterior, 2 * anchoPantalla / 5, altoPantalla * 11 / 25);
		frame = this;
		controller = new CrearTemaController();
		filtros = new Filtro();
		prepararElementos();
		preparaElementosBotones();
		preparaTablaPrincipal();
		cargarDatosTabla();	
		accionesComponentes();
		deshacerFiltroTablaPrincipal();
	}

	/**
	 * metodo que prepara los elementos
	 */
	private void prepararElementos(){
		int posicionX = anchoPantalla/100*6;
		int posicionY = altoPantalla/100*5;
		int anchoTxt = anchoPantalla/100*20;
		
		lbl_nombreB = new JLabel("Nombre: ", JLabel.LEFT);
		lbl_nombreB.setLocation(posicionX , posicionY);
		lbl_nombreB.setSize(80, 25);
		lbl_nombreB.setForeground(letra.getColorLabel());
		lbl_nombreB.setFont(letra.getFuenteLabel());
		frame.add(lbl_nombreB);
		
		txt_idTema = new JTextField();
		
		txt_tema = new JTextField();
		txt_tema.setLocation(lbl_nombreB.getX()+lbl_nombreB.getWidth()/4*5,lbl_nombreB.getY());
		txt_tema.setSize(anchoTxt, 25);
		txt_tema.setFont(letra.getFuenteTxt());
		txt_tema.setForeground(letra.getColorTxt());
		txt_tema.setDisabledTextColor(letra.getColorTxtDisable());
		frame.add(txt_tema);
		
		// txt para la modiciacion del tema
		lbl_nombreM = new JLabel("Nombre: ", JLabel.LEFT);
		lbl_nombreM.setLocation(posicionX , posicionY*3);
		lbl_nombreM.setSize(80, 25);
		lbl_nombreM.setForeground(letra.getColorLabel());
		lbl_nombreM.setFont(letra.getFuenteLabel());
		frame.add(lbl_nombreM);
		lbl_nombreM.setVisible(false);
		
		txt_nombreTema = new JTextField();
		txt_nombreTema.setLocation(lbl_nombreM.getX()+lbl_nombreM.getWidth()/4*5,posicionY*3);
		txt_nombreTema.setSize(anchoTxt, 25);
		txt_nombreTema.setFont(letra.getFuenteTxt());
		txt_nombreTema.setForeground(letra.getColorTxt());
		txt_nombreTema.setDisabledTextColor(letra.getColorTxtDisable());
		frame.add(txt_nombreTema);
		txt_nombreTema.setVisible(false);
		
		lbl_asignatura = new JLabel("Asignatura: ", JLabel.LEFT);
		lbl_asignatura.setLocation(posicionX , posicionY*3+25*2);
		lbl_asignatura.setSize(80, 25);
		lbl_asignatura.setForeground(letra.getColorLabel());
		lbl_asignatura.setFont(letra.getFuenteLabel());
		frame.add(lbl_asignatura);
		lbl_asignatura.setVisible(false);
		
		//combo para modificar la el tema
		combo_Asignatura = new JComboBox<String>();
		combo_Asignatura.setFont(letra.getFuenteCombo());
		combo_Asignatura.setForeground(letra.getColorCombo());
		combo_Asignatura.setLocation(txt_nombreTema.getX(), lbl_asignatura.getY());
		combo_Asignatura.setSize(anchoTxt, 30);
		frame.add(combo_Asignatura);
		
		// TODO Poblar combo con materias
		ArrayList<Materia> materias = controller.obtenerMaterias();
		for (int i = 0; i < materias.size(); i++) {
			combo_Asignatura.addItem(materias.get(i).getNombre());
		}
		combo_Asignatura.setSelectedIndex(-1);
		combo_Asignatura.setVisible(false);
		
	}
	
	/**
	 * metodo que prepara los elementos de los botones
	 */
	private void preparaElementosBotones(){
		int anchoBtn = 100;
		int altoBtn = 30;
		int posicionX = txt_tema.getX()-anchoBtn/2;
		panel_botones = new JPanel();
		panel_botones.setLocation(0, altoPantalla/100*42);
		panel_botones.setSize(anchoPantalla, altoBtn);
		panel_botones.setLayout(null);
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_eliminar = new Boton(0, 0, anchoBtn, altoBtn, "Eliminar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonEliminar();
			}
		};
		btn_eliminar.setLocation(posicionX, 0);
		
		btn_modificar = new Boton(0, 0, anchoBtn, altoBtn, "Modificar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonModificar();
			}
		};
		btn_modificar.setLocation(posicionX+anchoBtn+altoBtn, 0);
		
		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
		btn_regresar.setLocation(posicionX+anchoBtn*2+altoBtn*2, 0);
		
		btn_guardar = new Boton(0, 0, anchoBtn, altoBtn, "Guardar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonGuardar();;
			}
		};
		btn_guardar.setLocation(posicionX, 0);
		btn_guardar.setVisible(false);
		
		btn_cancelar = new Boton(0, 0, anchoBtn, altoBtn, "Cancelar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonCancelar();;
			}
		};
		btn_cancelar.setLocation(posicionX+anchoBtn*2+altoBtn*2, 0);
		btn_cancelar.setVisible(false);
	}
	/**
	 * metodo que prepara la tabla principal
	 */
	private void preparaTablaPrincipal(){
		int anchoTabla = anchoPantalla/100*35;
		int altoTabla  = altoPantalla/100*30;
		int posicionXTabla =anchoPantalla/100*2;
		int posicionYTabla =altoPantalla/100*10;
		
	  	  dtmTablaPrincipal= new TablaNoEditable();
	  	  tablaPrincipal = new JTable(dtmTablaPrincipal);
	  	  dtmTablaPrincipal.addColumn("Id");
	  	  dtmTablaPrincipal.addColumn("Nombre");
	  	  dtmTablaPrincipal.addColumn("Materia");
	  	 

	  	  tablaPrincipal.setPreferredScrollableViewportSize(new Dimension(anchoTabla - 30,altoTabla -30));
		  		
		  	  scrollpanePrincipal = new JScrollPane(tablaPrincipal);
		  	  frame.add(scrollpanePrincipal);
		  	  scrollpanePrincipal.setLocation(posicionXTabla, posicionYTabla);
		  	  scrollpanePrincipal.setSize(anchoTabla,altoTabla);
		  		
		  	  tablaPrincipal.getColumnModel().getColumn(0).setMaxWidth(0);
		  	  tablaPrincipal.getColumnModel().getColumn(0).setMinWidth(0);
		  	  tablaPrincipal.getColumnModel().getColumn(0).setPreferredWidth(0);
		  		
		  	  tablaPrincipal.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		  	  tablaPrincipal.getTableHeader().setReorderingAllowed(false);
		  	  tablaPrincipal.setDefaultRenderer(Object.class, new MiRender());
		  	  tablaPrincipal.setShowHorizontalLines(false);
		  	  tablaPrincipal.setBorder(null);
		  	  tablaPrincipal.setOpaque(false);
		  	  scrollpanePrincipal.setOpaque(false);
		  	  scrollpanePrincipal.getViewport().setOpaque(false);
		  	  scrollpanePrincipal.setBorder(null);
	    
	}
	/**
	 * metodo que carga la tabla principal
	 */
	private void cargarDatosTabla() {
		dtmTablaPrincipal.setRowCount(0);
		ArrayList<String[]> tema = controller.obtenerTodoTema();

		for (int i = 0; i<=tema.size();i++) {
			DatosTablaPrincipal = new String[dtmTablaPrincipal.getColumnCount()];

			try {
				DatosTablaPrincipal[id] = tema.get(i)[0];
				DatosTablaPrincipal[nombre] = tema.get(i)[1];
				DatosTablaPrincipal[asignatura] = tema.get(i)[5];

				dtmTablaPrincipal.addRow(DatosTablaPrincipal);
			} catch (Exception ex) {
			}
		}
	}
	
	/**
	 * metodo que carga los datos en el formulario para poder modificarlos
	 */
	public void cargarDatosModificar(int fila){
		txt_nombreTema.setText(tablaPrincipal.getValueAt(fila, nombre).toString());
		combo_Asignatura.setSelectedItem(tablaPrincipal.getValueAt(fila, asignatura).toString());
		txt_idTema.setText(tablaPrincipal.getValueAt(fila, id).toString());
	}
	
	/**
	 * metodo que contiene la accion del boton eliminar
	 */
	private void accionBotonEliminar(){
		int fila = tablaPrincipal.getSelectedRow();
		if (fila!= -1) {
			String valido = controller.validarSiTemaTieneSubtemas(tablaPrincipal.getValueAt(fila, id).toString());
			if (valido.equals("NOEXISTE")) {
				int resultado = JOptionPane.showOptionDialog(frame, "�Esta seguro que desea eliminar el Tema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+"?", "Advertencia",JOptionPane.OK_OPTION,JOptionPane.QUESTION_MESSAGE,null,null,"aceptar");
				if (resultado ==0) {
					boolean resul = controller.eliminarTema(tablaPrincipal.getValueAt(fila, id).toString());
					if (resul) {
						JOptionPane.showMessageDialog(null, "El tema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+". Fue eliminado correctamente", "Confirmacion",JOptionPane.INFORMATION_MESSAGE);
						cargarDatosTabla();
						deshacerFiltroTablaPrincipal();
						txt_tema.setText("");
						txt_tema.grabFocus();
					}else{
						JOptionPane.showMessageDialog(null, "No fue Posible borrar el tema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+".", "Advertencia",JOptionPane.ERROR_MESSAGE);
					}
				}
			}else if(valido.equals("ERROR")){
				JOptionPane.showMessageDialog(null, "Ha ocurrido un error con la base de datos", "Error",JOptionPane.ERROR_MESSAGE);
			}else if(valido.equals("EXISTE")){
				JOptionPane.showMessageDialog(null, "No es Posible borrar el tema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+". Ya tiene subtemas asociados ", "Advertencia",JOptionPane.WARNING_MESSAGE);
			}
		}else{
			JOptionPane.showMessageDialog(null, "No ha Seleccionado ninguna fila", "Advertencia",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * metodo que contiene la accion del boton modificar
	 */
	private void accionBotonModificar(){
		int fila = tablaPrincipal.getSelectedRow();
		if (fila!= -1) {
			String valido = controller.validarSiTemaTieneSubtemas(tablaPrincipal.getValueAt(fila, id).toString());
			if (valido.equals("NOEXISTE")) {
				ocultarBuscar();
				MostrarModificar();
				cargarDatosModificar(fila);
			}else if(valido.equals("ERROR")){
				JOptionPane.showMessageDialog(null, "Ha ocurrido un error con la base de datos", "Error",JOptionPane.ERROR_MESSAGE);
			}else if(valido.equals("EXISTE")){
				ocultarBuscar();
				MostrarModificar();
				combo_Asignatura.setEnabled(false);
				JOptionPane.showMessageDialog(null, "El tema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+" tiene subtemas asociados, Por lo tanto no es posible modificar la asignatura.", "Advertencia",JOptionPane.WARNING_MESSAGE);
				cargarDatosModificar(fila);
			}
		}else{
			JOptionPane.showMessageDialog(null, "No ha Seleccionado ninguna fila", "Advertencia",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * metodo que contiene la accion del boton regresar
	 */
	private void accionBotonRegresar(){
		cerrarVentana();
	}
	
	/**
	 * metodo que contiene la accion del boton guardar
	 */
	private void accionBotonGuardar(){
		if (!txt_nombreTema.getText().replace(" ", "").isEmpty()) {
			Materia materia = controller.obtenerMateria(combo_Asignatura.getSelectedItem().toString());
				boolean resul = controller.ModificarTema( txt_idTema.getText(), txt_nombreTema.getText(), materia, null);
				if (resul) {
					JOptionPane.showMessageDialog(null, "El tema fue modificado correctamente.", "Confirmacion",JOptionPane.INFORMATION_MESSAGE);
					cargarDatosTabla();
					deshacerFiltroTablaPrincipal();
					txt_tema.setText("");
					txt_tema.grabFocus();
					ocultarModificar();
					MostrarBuscar();
				}else{
					JOptionPane.showMessageDialog(null, "No fue Posible modificar el tema.", "Advertencia",JOptionPane.ERROR_MESSAGE);
				}

		}else{
			JOptionPane.showMessageDialog(null, "EL nombre del tema no puede estar vacio", "Advertencia",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * metodo que contiene la accion del boton Cancelar
	 */
	private void accionBotonCancelar(){
		ocultarModificar();
		MostrarBuscar();
		deshacerFiltroTablaPrincipal();
		txt_tema.setText("");
		txt_tema.grabFocus();
	}
	
	@Override
	protected void actualizarPantalla() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}
	
	/**
	 * metodo que contiene las acciones de los componentes
	 */
	public void accionesComponentes(){
		txt_tema.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				char KeyChar = e.getKeyChar();
				if (Character.isLowerCase(KeyChar)){
					e.setKeyChar(Character.toUpperCase(KeyChar));
				}	
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					if(tablaPrincipal .getRowCount()>=1){
						tablaPrincipal.grabFocus();
						tablaPrincipal.getSelectionModel().setSelectionInterval(0, 0);
					}
					else{
						txt_tema.setText("");
						deshacerFiltroTablaPrincipal();
						JOptionPane.showOptionDialog(frame, "Error, el tipo Tema no existe. Int�ntelo de nuevo", "Error",JOptionPane.DEFAULT_OPTION,JOptionPane.ERROR_MESSAGE,null,null,"aceptar");
						txt_tema.grabFocus();
					}
				}else{
					filtros.filtroDosColumnas(txt_tema.getText().trim().toUpperCase(), trsfiltrotablaPrincipal, 1, 2, dtmTablaPrincipal, tablaPrincipal);
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		txt_nombreTema.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				char KeyChar = e.getKeyChar();
				if (Character.isLowerCase(KeyChar)){
					e.setKeyChar(Character.toUpperCase(KeyChar));
				}	
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
	}

	/**
	 * metodo que deshace el filtro de la tabla documento fuente
	 */
	public void deshacerFiltroTablaPrincipal(){
		filtros.deshacerFiltro(trsfiltrotablaPrincipal, dtmTablaPrincipal, tablaPrincipal);
	}
	/**
	 * mketodo que oculta los compenentes de buscar un tema
	 */
	public void ocultarBuscar(){
		txt_tema.setVisible(false);
		lbl_nombreB.setVisible(false);
		scrollpanePrincipal.setVisible(false);
		btn_regresar.setVisible(false);
		btn_eliminar.setVisible(false);
		btn_modificar.setVisible(false);
	}
	
	/**
	 * metodo que oculta los componentes de modificar tema
	 */
	public void ocultarModificar(){
		btn_cancelar.setVisible(false);
		btn_guardar.setVisible(false);
		txt_nombreTema.setVisible(false);
		combo_Asignatura.setVisible(false);
		combo_Asignatura.setEnabled(true);
		lbl_nombreM.setVisible(false);
		lbl_asignatura.setVisible(false);
	}
	
	/**
	 * metodo que muestra los compenentes de buscar un tema
	 */
	public void MostrarBuscar(){
		txt_tema.setVisible(true);
		lbl_nombreB.setVisible(true);
		scrollpanePrincipal.setVisible(true);
		btn_regresar.setVisible(true);
		btn_eliminar.setVisible(true);
		btn_modificar.setVisible(true);
	}
	
	/**
	 * metodo que muestra los componentes de modificar tema
	 */
	public void MostrarModificar(){
		btn_cancelar.setVisible(true);
		btn_guardar.setVisible(true);
		lbl_nombreM.setVisible(true);
		lbl_asignatura.setVisible(true);
		txt_nombreTema.setVisible(true);
		combo_Asignatura.setVisible(true);
	}
	
}
