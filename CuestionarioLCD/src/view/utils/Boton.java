package view.utils;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public abstract class Boton extends JButton {
	private static final long serialVersionUID = -2239778049395443927L;

	protected static Cursor cursorEspera = new Cursor(Cursor.WAIT_CURSOR);
	protected static Cursor cursorVacio = null;

	/**
	 * Crea un boton con las especificaciones dadas.<br>
	 * <b>NOTA:</b> Las posiciones y los anchos y largos no aplican con un
	 * BorderLayout = new GridLayout().
	 * 
	 * @param x
	 *            posicion en X del boton.
	 * @param y
	 *            posicion en Y del boton.
	 * @param width
	 *            ancho del boton.
	 * @param height
	 *            alto del boton.
	 * @param panel
	 *            Panel al que pertenecera el boton.
	 * @param ventana
	 *            Ventana a la que pertenecera el boton.
	 */
	public Boton(int x, int y, int width, int height, JPanel panel,
			JFrame ventana) {
		panel.add(this);
		setLocation(x, y);
		setSize(width, height);

		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ventana.setCursor(cursorEspera);
				accionBoton();
				ventana.setCursor(cursorVacio);
			}
		});
	}

	/**
	 * Crea un boton con las especificaciones dadas.<br>
	 * <b>NOTA:</b> Las posiciones y los anchos y largos no aplican con un
	 * BorderLayout = new GridLayout().
	 * 
	 * @param x
	 *            posicion en X del boton.
	 * @param y
	 *            posicion en Y del boton.
	 * @param width
	 *            ancho del boton.
	 * @param height
	 *            alto del boton.
	 * @param text
	 *            Contenido del boton.
	 * @param panel
	 *            Panel al que pertenecera el boton.
	 * @param ventana
	 *            Ventana a la que pertenecera el boton.
	 */
	public Boton(int x, int y, int width, int height, String text,
			JPanel panel, JFrame ventana) {
		this(x, y, width, height, panel, ventana);
		super.setText(text);
	}

	/**
	 * Accion a adicionar al boton.
	 */
	public abstract void accionBoton();
}
