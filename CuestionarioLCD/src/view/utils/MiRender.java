package view.utils;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

public class MiRender extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 4829515703881131239L;

	private final Letras letra = new Letras();

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		// Color c2 = new Color(236, 253, 245);
		Color c2 = new Color(227, 244, 253);
		Color c = new Color(255, 255, 255);
		Color colorSelect = new Color(180, 205, 220);

		this.setForeground(letra.getColorFuenteTabla());
		this.setFont(letra.getFuenteTabla());
		this.setBackground(c2);
		// this.setForeground(letra);
		this.setHorizontalAlignment(LEFT);

		boolean oddRow = (row % 2 == 0);
		if (oddRow) {
			setBackground(c);
		}

		if (isSelected) {
			this.setBackground(colorSelect);
		}

		return this;
	}
}