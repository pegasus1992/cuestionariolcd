package view.utils;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import view.*;

public abstract class General extends JFrame {
	private static final long serialVersionUID = -916841688555932268L;

	protected static Cursor cursorEspera = new Cursor(Cursor.WAIT_CURSOR);
	protected static Cursor cursorVacio = null;

	protected static final String logoColegio = "imagenes/escudo.png";
	protected static final String imagenFondo = "imagenes/fondo.png";

	// Dimension de Pantalla
	private static final Dimension dimensionPantalla = Toolkit
			.getDefaultToolkit().getScreenSize();
	public static final int anchoPantalla = (int) dimensionPantalla.getWidth();
	public static final int altoPantalla = (int) dimensionPantalla.getHeight() - 35;

	// Barra de Titulos
	protected JPanel panel_barraTitulos;
	private JLabel lbl_barraTitulos;
	private JButton btn_minimizar, btn_cerrar;

	// Utilitarios
	protected static final String titulo = "Plataforma de Evaluación - ";
	protected static String tituloVentana;
	protected Letras letra;
	private JFrame anterior;
	private int x, y;

	protected General(JFrame anterior, int ancho, int alto) {
		setSize(ancho, alto);

		setUndecorated(true);
		tituloVentana = titulo + tituloPantalla();
		setTitle(tituloVentana);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(logoColegio));
		BackImage.setFondoFrame(imagenFondo, anchoPantalla, altoPantalla, this);

		prepareElementosBarraTitulos(tituloVentana);
		definaAccionesBarraTitulos();

		setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);

		setAnterior(anterior);
		letra = new Letras();
	}

	/**
	 * Prepara la barra de titulos personalizada.
	 * 
	 * @param title
	 *            Nombre de la ventana.
	 */
	private void prepareElementosBarraTitulos(String title) {
		int altoBarra = 22;
		int anchoBoton = 40;

		panel_barraTitulos = new JPanel();
		panel_barraTitulos.setBounds(0, 0, this.getWidth(), altoBarra);
		panel_barraTitulos.setLayout(null);
		panel_barraTitulos.setOpaque(false);
		this.add(panel_barraTitulos);

		lbl_barraTitulos = new JLabel(title, JLabel.LEFT);
		panel_barraTitulos.add(lbl_barraTitulos);
		lbl_barraTitulos.setLocation(panel_barraTitulos.getX(),
				panel_barraTitulos.getY());
		lbl_barraTitulos.setSize(
				panel_barraTitulos.getWidth() - anchoBoton * 2,
				panel_barraTitulos.getHeight());
		lbl_barraTitulos.setBackground(Color.LIGHT_GRAY);
		lbl_barraTitulos.setOpaque(true);
		lbl_barraTitulos.setFocusable(false);

		btn_minimizar = new JButton("_");
		panel_barraTitulos.add(btn_minimizar);
		btn_minimizar.setLocation(lbl_barraTitulos.getWidth(),
				lbl_barraTitulos.getY());
		btn_minimizar.setSize(anchoBoton, lbl_barraTitulos.getHeight());
		btn_minimizar.setFocusable(false);

		btn_cerrar = new JButton("X");
		panel_barraTitulos.add(btn_cerrar);
		btn_cerrar.setLocation(
				lbl_barraTitulos.getWidth() + btn_minimizar.getWidth(),
				btn_minimizar.getY());
		btn_cerrar.setSize(btn_minimizar.getWidth(), btn_minimizar.getHeight());
		btn_cerrar.setFocusable(false);
		btn_cerrar.setEnabled(false);
	}

	/**
	 * Define las acciones de movimiento de la ventana y el boton minimizar.
	 */
	private void definaAccionesBarraTitulos() {
		lbl_barraTitulos.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent evt) {
				x = evt.getX();
				y = evt.getY();
			}
		});
		lbl_barraTitulos.addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				Point point = MouseInfo.getPointerInfo().getLocation();
				setLocation(point.x - x, point.y - y);
			}
		});

		btn_minimizar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setExtendedState(ICONIFIED);
			}
		});
	}

	/**
	 * Refrescar la pantalla actual.
	 */
	protected abstract void actualizarPantalla();

	/**
	 * Titulo especifico de la pantalla.
	 * 
	 * @return
	 */
	protected abstract String tituloPantalla();

	/**
	 * Despliega una nueva ventana, volviendo invisible la actual.
	 * 
	 * @param nuevo
	 */
	protected void nuevaVentana(JFrame nuevo) {
		setVisible(false);
	}

	/**
	 * Reinicia la aplicacion (desde el login).
	 */
	protected void cerrarSesion() {
		new Ingresar();
		setVisible(false);
		dispose();
	}

	/**
	 * Cierra la ventana actual y vuelve a cargar la ventana anterior.
	 */
	protected void cerrarVentana() {
		JFrame anterior = getAnterior();
		if (anterior != null) {
			((General) anterior).actualizarPantalla();
			anterior.setVisible(true);
		}
		setVisible(false);
		dispose();
	}

	/**
	 * Cierra definitivamente el programa.
	 * 
	 * @param ventana
	 *            Ventana a cerrar.
	 */
	protected void cerrarPrograma(General ventana) {
		ventana.dispose();
		System.exit(0);
	}

	private JFrame getAnterior() {
		return anterior;
	}

	private void setAnterior(JFrame anterior) {
		this.anterior = anterior;
	}
}
