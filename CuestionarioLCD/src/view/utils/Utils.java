package view.utils;

import java.io.*;
import java.security.*;
import java.util.logging.*;

import javax.swing.*;

import exception.*;

public class Utils {

	public static void display() {
		System.out.println();
	}

	public static void display(Object msg) {
		System.out.println(msg);
	}

	public static void display(String msg) {
		System.out.println(msg);
	}

	public static void display(boolean msg) {
		System.out.println(msg);
	}

	public static void display(char msg) {
		System.out.println(msg);
	}

	public static void display(char[] msg) {
		System.out.println(msg);
	}

	public static void display(double msg) {
		System.out.println(msg);
	}

	public static void display(float msg) {
		System.out.println(msg);
	}

	public static void display(int msg) {
		System.out.println(msg);
	}

	public static void display(long msg) {
		System.out.println(msg);
	}

	public static void log(String name, Exception ex) {
		Logger.getLogger(name).log(Level.SEVERE, null, ex);
	}

	public static void setLookAndFeel(String lookAndFeel)
			throws LookAndFeelException {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager
					.getInstalledLookAndFeels()) {
				if (lookAndFeel.equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException ex) {
			throw new LookAndFeelException(ex.getMessage());
		}
	}

	public static String formatTime(long miliseconds) {
		int seconds = (int) (miliseconds / 1000000);
		int minutes = seconds / 60;
		seconds %= 60;
		String resp = "";
		if (minutes < 10) {
			resp += "0";
		}
		resp += minutes + ":";
		if (seconds < 10) {
			resp += "0";
		}
		resp += seconds;
		return resp;
	}

	/**
	 * Encripta la cadena ingresada con el metodo de Hash SHA1.
	 * 
	 * @param input
	 * @return cadena encriptada.
	 */
	public static String sha1(String input) {
		try {
			MessageDigest mDigest = MessageDigest.getInstance("SHA1");
			byte[] result = mDigest.digest(input.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16)
						.substring(1));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static boolean stringIsNotNull(String string) {
		return string != null && !string.trim().isEmpty();
	}

	/**
	 * Realiza una copia bit a bit de un archivo, de un fichero a otro.
	 * 
	 * @param ficheroOriginal
	 *            <p>
	 *            Fichero original del archivo.
	 *            </p>
	 * @param ficheroCopia
	 *            <p>
	 *            Fichero de destino del archivo.
	 *            </p>
	 * @throws IOException
	 */
	public static void copyFileBit2Bit(String ficheroOriginal,
			String ficheroCopia) throws IOException {
		File input = new File(ficheroOriginal);
		File output = new File(ficheroCopia);

		if (!input.getAbsolutePath().equals(output.getAbsolutePath())) {
			// Se abre el fichero original para lectura
			FileInputStream fileInput = new FileInputStream(input);
			BufferedInputStream bufferedInput = new BufferedInputStream(
					fileInput);

			// Se abre el fichero donde se har� la copia
			FileOutputStream fileOutput = new FileOutputStream(output);
			BufferedOutputStream bufferedOutput = new BufferedOutputStream(
					fileOutput);

			// Bucle para leer de un fichero y escribir en el otro.
			byte[] array = new byte[50000];
			int leidos = bufferedInput.read(array);
			while (leidos > 0) {
				bufferedOutput.write(array, 0, leidos);
				leidos = bufferedInput.read(array);
			}

			// Cierre de los ficheros
			bufferedInput.close();
			bufferedOutput.close();
		}
	}
}
