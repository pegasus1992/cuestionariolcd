package view.utils;

import javax.swing.table.*;

public class TablaNoEditable extends DefaultTableModel {
	private static final long serialVersionUID = -7470717608639464349L;

	public boolean isCellEditable(int row, int column) {
		return false;
	}
}