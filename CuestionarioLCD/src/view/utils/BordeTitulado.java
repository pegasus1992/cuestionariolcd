package view.utils;

import javax.swing.border.*;

public class BordeTitulado extends TitledBorder {
	private static final long serialVersionUID = 6757520387603583330L;

	// Utilitarios
	private static Letras letra;

	/**
	 * Adiciona un borde con titulo al componente grafico deseado (JLabel,
	 * JPanel, etc).
	 * 
	 * @param title
	 */
	public BordeTitulado(String title) {
		super(title);
		super.setBorder(null);
		super.setTitleJustification(TitledBorder.LEFT);
		super.setTitlePosition(TitledBorder.DEFAULT_POSITION);

		letra = new Letras();
		super.setTitleFont(letra.getFuenteLabel());
		super.setTitleColor(letra.getColorLabel());
	}
}
