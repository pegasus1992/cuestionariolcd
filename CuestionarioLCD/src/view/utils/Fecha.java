package view.utils;

import java.util.*;

import javax.swing.*;

import exception.FechaException;

public class Fecha {

	public void completarFecha(JFormattedTextField cajaFecha) {
		String anioActual = getAnioActual();
		if (cajaFecha.getText().replace("/", "").trim().isEmpty()) {
		} else if (cajaFecha.getText().replace("/", "").trim().length() == 2) {
			Calendar fecha = new GregorianCalendar();
			// int ano = fecha.get(Calendar.YEAR);
			String ano = anioActual.trim();
			int mes = fecha.get(Calendar.MONTH) + 1;
			String anoNuevo = ano;
			String mesNuevo;
			if (mes < 10) {
				mesNuevo = "0" + Integer.toString(mes);
			} else {
				mesNuevo = Integer.toString(mes);
			}
			cajaFecha.setText(cajaFecha.getText().replace("/", "").trim()
					+ mesNuevo + anoNuevo);

		} else if (cajaFecha.getText().replace("/", "").trim().length() == 4) {
			// Calendar fecha = new GregorianCalendar();
			// int ano = fecha.get(Calendar.YEAR);
			String ano = anioActual.trim();
			// String anoNuevo=Integer.toString(ano);
			String anoNuevo = ano;
			cajaFecha.setText(cajaFecha.getText().replace("/", "").trim()
					+ anoNuevo);

		} else if (cajaFecha.getText().replace("/", "").trim().length() == 6) {
			String nuevoano = "20"
					+ cajaFecha.getText().replace("/", "").trim()
							.substring(4, 6);
			cajaFecha.setText(cajaFecha.getText().replace("/", "").trim()
					.substring(0, 4)
					+ nuevoano);
		} else if (cajaFecha.getText().replace("/", "").trim().length() != 8) {

			cajaFecha.setValue("");
			cajaFecha.grabFocus();
		}

	}

	/**
	 * Valida que la fecha ingresada corresponda con el formato < dd/MM/yyyy >.
	 * 
	 * @param fecha
	 * @return
	 */
	public String validarFechaDDMMYYYY(String fecha) {

		if (fecha.replace("/", "").replace(" ", "").trim().length() != 8) {
			return "INVALIDO";
		} else {
			String cadena = fecha.replace("/", "");
			String dia = cadena.substring(0, 2);
			String mes = cadena.substring(2, 4);
			String paso = "";
			if (cadena.trim().length() < 8) {
				return "fecha";
			}

			String anio = cadena.substring(4);
			// System.out.println(anio);
			int numeromes = Integer.parseInt(mes);
			int numerodia = Integer.parseInt(dia);
			int numeroan = Integer.parseInt(anio);
			GregorianCalendar calendar = new GregorianCalendar();
			boolean bis = calendar.isLeapYear(numeroan);

			if (anio.charAt(0) == '0') {
				paso = "anio";
			}
			if (numerodia > 31) {
				paso = "dia";
			}
			if (numeromes > 12) {
				paso = "mes";
			} else {
				if (mes.equals("04") || mes.equals("06") || mes.equals("09")
						|| mes.equals("11")) {
					if (numerodia > 30) {
						paso = "dia";
					}
				} else if (mes.equals("02")) {
					if (bis && numerodia > 29) {
						paso = "dia";
					}
					if (!bis && numerodia > 28) {
						paso = "dia";
					}
				}
			}
			return paso;
		}
	}

	/**
	 * Obtiene la fecha actual, con formato < dd/MM/yyyy >.
	 * 
	 * @return
	 */
	public String fechaActual() {
		Calendar fecha = new GregorianCalendar();
		int anio = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH) + 1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH);

		return String.format("%02d/%02d/%02d", dia, mes, anio);
	}

	/**
	 * Obtiene la fecha actual, sin "/".
	 * 
	 * @return
	 */
	public String getToday() {
		return fechaActual().replace("/", "");
	}

	private String getAnioActual() {
		Calendar fecha = new GregorianCalendar();
		int anio = fecha.get(Calendar.YEAR);
		return String.format("%02d", anio);
	}

	public String fechaInicial() {
		return "01" + "01" + getAnioActual();
	}

	public String fechaFinal() {
		return "31" + "12" + getAnioActual();
	}

	/**
	 * Valida la fecha ingresada.
	 * 
	 * @param fecha
	 * @throws FechaException
	 *             Excepcion si la fecha no corresponde.
	 */
	public void validarFecha(String fecha) throws FechaException {
		if (!fecha.replace("/", "").trim().isEmpty()) {
			String resultado = validarFechaDDMMYYYY(fecha.trim());

			if (fecha.equals(null)) {
				throw new FechaException("Fecha nula.");
			} else if (resultado.equals("mes")) {
				throw new FechaException(
						"Ha ingresado un mes inv�lido. Int�ntelo de nuevo");
			} else if (resultado.equals("dia")) {
				throw new FechaException(
						"Ha ingresado un d�a inv�lido. Int�ntelo de nuevo");
			} else if (resultado.equals("anio")) {
				throw new FechaException(
						"Ha ingresado un a�o inv�lido. Int�ntelo de nuevo");
			} else if (resultado.equals("fecha")) {
				throw new FechaException(
						"Ha ingresado una fecha inv�lida. Int�ntelo de nuevo");
			} else if (resultado.equals("INVALIDO")) {
				throw new FechaException(
						"Ha ingresado una fecha inv�lida. Int�ntelo de nuevo");
			}
		}
	}

	/**
	 * Obtiene la fecha y hora actual, con formato < dd/MM/yyyy - hh:mm:ss (AM | PM) >
	 * .
	 * 
	 * @return
	 */
	public String fechaHoraActual() {
		Calendar fechaHora = new GregorianCalendar();

		int anio = fechaHora.get(Calendar.YEAR);
		int mes = fechaHora.get(Calendar.MONTH) + 1;
		int dia = fechaHora.get(Calendar.DAY_OF_MONTH);
		int hora = fechaHora.get(Calendar.HOUR);
		int minutos = fechaHora.get(Calendar.MINUTE);
		int segundos = fechaHora.get(Calendar.SECOND);
		int ampm = fechaHora.get(Calendar.AM_PM);

		String hour = "", date = "", moment = "";
		String resp = "";

		switch (ampm) {
		case Calendar.AM: {
			moment = "AM";
		}
			break;
		case Calendar.PM: {
			moment = "PM";
		}
			break;
		}
		if (hora == 0) {
			hora = 12;
		}

		hour = String.format("%02d:%02d:%02d", hora, minutos, segundos);
		date = String.format("%02d/%02d/%02d", dia, mes, anio);
		resp = date + " - " + hour + " " + moment;

		return resp;
	}
}
