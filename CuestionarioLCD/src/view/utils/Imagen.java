package view.utils;

import java.awt.*;
import java.awt.image.*;
import java.io.*;

import javax.imageio.*;
import javax.swing.*;

public class Imagen {

	/**
	 * Lee la imagen que esta en la ruta especificada.
	 * 
	 * @param ruta
	 * @param ancho
	 *            ancho de la imagen.
	 * @param alto
	 *            alto de la imagen.
	 * @return
	 */
	public static ImageIcon convert2Image(String ruta, int ancho, int alto) {
		try {
			BufferedImage bufferedImage = ImageIO.read(new File(ruta));
			Image image = bufferedImage.getScaledInstance(ancho, alto,
					Image.SCALE_SMOOTH);
			return new ImageIcon(image);
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
