package view.utils;

import java.awt.*;

public class Letras {
	private Color colorLabel;
	private Color colorLabelVariable;
	private Color colorAvisos;
	private Color colorAvisos2;
	private Color colorFuenteTabla;
	private Color colorTxt;
	private Color colorTxtDisable;
	private Color colorCombo;

	private Font fuenteAvisos2;
	private Font fuenteAvisos;
	private Font fuenteLabelVariable;
	private Font fuenteLabel;
	private Font fuenteTabla;
	private Font fuenteTxt;
	private Font fuenteCombo;

	public Letras() {
		colorLabel = new Color(0, 100, 144);
		colorLabelVariable = new Color(0, 115, 130);
		colorAvisos = new Color(0, 74, 85);
		colorAvisos2 = new Color(0, 74, 85);
		colorFuenteTabla = new Color(51, 51, 51);
		colorTxt = new Color(0, 0, 0);
		colorTxtDisable = new Color(133, 144, 246);
		colorCombo = new Color(0, 0, 0);

		fuenteAvisos2 = new Font("Arial", Font.BOLD, 15);
		fuenteAvisos = new Font("Arial", Font.BOLD, 20);
		fuenteLabelVariable = new Font("Arial", Font.BOLD, 12);
		fuenteLabel = new Font("Arial", Font.BOLD, 12);
		fuenteTabla = new Font("Arial", Font.PLAIN, 12);
		fuenteTxt = new Font("Arial", Font.PLAIN, 12);
		fuenteCombo = new Font("Arial", Font.BOLD, 12);
	}

	public Color getColorLabel() {
		return colorLabel;
	}

	public void setColorLabel(Color colorLabel) {
		this.colorLabel = colorLabel;
	}

	public Color getColorLabelVariable() {
		return colorLabelVariable;
	}

	public void setColorLabelVariable(Color colorLabelVariable) {
		this.colorLabelVariable = colorLabelVariable;
	}

	public Color getColorAvisos() {
		return colorAvisos;
	}

	public void setColorAvisos(Color colorAvisos) {
		this.colorAvisos = colorAvisos;
	}

	public Color getColorAvisos2() {
		return colorAvisos2;
	}

	public void setColorAvisos2(Color colorAvisos2) {
		this.colorAvisos2 = colorAvisos2;
	}

	public Font getFuenteAvisos() {
		return fuenteAvisos;
	}

	public void setFuenteAvisos(Font fuenteAvisos) {
		this.fuenteAvisos = fuenteAvisos;
	}

	public Font getFuenteLabelVariable() {
		return fuenteLabelVariable;
	}

	public void setFuenteLabelVariable(Font fuenteLabelVariable) {
		this.fuenteLabelVariable = fuenteLabelVariable;
	}

	public Font getFuenteLabel() {
		return fuenteLabel;
	}

	public void setFuenteLabel(Font fuenteLabel) {
		this.fuenteLabel = fuenteLabel;
	}

	public Color getColorFuenteTabla() {
		return colorFuenteTabla;
	}

	public void setColorFuenteTabla(Color colorFuenteTabla) {
		this.colorFuenteTabla = colorFuenteTabla;
	}

	public Font getFuenteTabla() {
		return fuenteTabla;
	}

	public void setFuenteTabla(Font fuenteTabla) {
		this.fuenteTabla = fuenteTabla;
	}

	public Color getColorTxt() {
		return colorTxt;
	}

	public void setColorTxt(Color colorTxt) {
		this.colorTxt = colorTxt;
	}

	public Color getColorTxtDisable() {
		return colorTxtDisable;
	}

	public void setColorTxtDisable(Color colorTxtDisable) {
		this.colorTxtDisable = colorTxtDisable;
	}

	public Color getColorCombo() {
		return colorCombo;
	}

	public void setColorCombo(Color colorCombo) {
		this.colorCombo = colorCombo;
	}

	public Font getFuenteTxt() {
		return fuenteTxt;
	}

	public void setFuenteTxt(Font fuenteTxt) {
		this.fuenteTxt = fuenteTxt;
	}

	public Font getFuenteCombo() {
		return fuenteCombo;
	}

	public void setFuenteCombo(Font fuenteCombo) {
		this.fuenteCombo = fuenteCombo;
	}

	public Font getFuenteAvisos2() {
		return fuenteAvisos2;
	}

	public void setFuenteAvisos2(Font fuenteAvisos2) {
		this.fuenteAvisos2 = fuenteAvisos2;
	}
}
