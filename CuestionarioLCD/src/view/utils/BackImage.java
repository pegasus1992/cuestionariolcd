package view.utils;

import java.awt.*;

import javax.swing.*;

public class BackImage extends JComponent {
	private static final long serialVersionUID = -5057401066183625153L;

	Image i;

	public BackImage(Image i) {
		this.i = i;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(i, 0, 0, new Color(187, 240, 210), null);
	}

	/**
	 * Le coloca fondo a la ventana actual.
	 * 
	 * @param rutaImagenFondo
	 * @param anchoPantalla
	 *            ancho de la ventana.
	 * @param altoPantalla
	 *            alto de la ventana.
	 * @param frame
	 *            ventana actual.
	 */
	public static void setFondoFrame(String rutaImagenFondo, int anchoPantalla,
			int altoPantalla, JFrame frame) {
		ImageIcon imagen = Imagen.convert2Image(rutaImagenFondo, anchoPantalla,
				altoPantalla);
		Image fondo = imagen.getImage();
		frame.setContentPane(new BackImage(fondo));
	}

	/**
	 * Le coloca fondo al dialogo actual.
	 * 
	 * @param rutaImagenFondo
	 * @param anchoPantalla
	 *            ancho del dialogo.
	 * @param altoPantalla
	 *            alto del dialogo.
	 * @param dialog
	 *            dialogo actual.
	 */
	public static void setFondoDialog(String rutaImagenFondo,
			int anchoPantalla, int altoPantalla, JDialog dialog) {
		ImageIcon imagen = Imagen.convert2Image(rutaImagenFondo, anchoPantalla,
				altoPantalla);
		Image fondo = imagen.getImage();
		dialog.setContentPane(new BackImage(fondo));
	}
}
