package view;

import java.awt.event.*;

import javax.swing.*;

import model.*;
import controller.*;
import view.utils.*;

public class Ingresar extends General {
	private static final long serialVersionUID = -9023811192106706401L;

	// Panel de Ingreso
	private JPanel panel_ingreso;
	private JLabel lbl_cedula, lbl_contrasena;
	private JTextField txt_cedula;
	private JPasswordField txt_contrasena;
	private JButton btn_ingreso, btn_salir;

	// Utilitarios
	private static final String titulo = "Ingreso de Docente";
	private IngresarController controller;

	/**
	 * Inicializa la ventana de login.
	 */
	public Ingresar() {
		super(null, anchoPantalla / 4, altoPantalla / 4);
		controller = new IngresarController();

		prepareElementosIngreso();
		definaAccionesIngreso();
	}

	/**
	 * Prepara los elementos del login.
	 */
	private void prepareElementosIngreso() {
		panel_ingreso = new JPanel();
		panel_ingreso.setLocation(0, panel_barraTitulos.getHeight());
		panel_ingreso.setSize(this.getWidth(), this.getHeight());
		panel_ingreso.setLayout(null);
		panel_ingreso.setOpaque(false);
		this.add(panel_ingreso);

		int anchoLabel = 175;
		int anchoTxt = 198;
		int altoTxt = 25;
		int multi = 12;

		lbl_cedula = new JLabel("C�dula: ", JLabel.LEFT);
		lbl_cedula.setLocation(panel_ingreso.getWidth() / multi,
				panel_ingreso.getY());
		lbl_cedula.setSize(anchoLabel, altoTxt);
		panel_ingreso.add(lbl_cedula);
		lbl_cedula.setForeground(letra.getColorLabel());
		lbl_cedula.setFont(letra.getFuenteLabel());

		txt_cedula = new JTextField();
		txt_cedula.setLocation(lbl_cedula.getX() * (multi / 3),
				lbl_cedula.getY());
		txt_cedula.setSize(anchoTxt, altoTxt);
		txt_cedula.setColumns(10);
		panel_ingreso.add(txt_cedula);
		txt_cedula.setFont(letra.getFuenteTxt());
		txt_cedula.setForeground(letra.getColorTxt());
		txt_cedula.setDisabledTextColor(letra.getColorTxtDisable());
		txt_cedula.grabFocus();

		lbl_contrasena = new JLabel("Contrase�a: ", JLabel.LEFT);
		lbl_contrasena.setLocation(lbl_cedula.getX(), lbl_cedula.getY()
				+ altoTxt * 2);
		lbl_contrasena.setSize(anchoLabel, altoTxt);
		panel_ingreso.add(lbl_contrasena);
		lbl_contrasena.setForeground(letra.getColorLabel());
		lbl_contrasena.setFont(letra.getFuenteLabel());

		txt_contrasena = new JPasswordField();
		txt_contrasena.setLocation(lbl_contrasena.getX() * (multi / 3),
				lbl_contrasena.getY());
		txt_contrasena.setSize(anchoTxt, altoTxt);
		txt_contrasena.setColumns(10);
		panel_ingreso.add(txt_contrasena);
		txt_contrasena.setFont(letra.getFuenteTxt());
		txt_contrasena.setForeground(letra.getColorTxt());
		txt_contrasena.setDisabledTextColor(letra.getColorTxtDisable());

		int anchoBtn = 110;
		int altoBtn = 30;

		btn_ingreso = new Boton(panel_ingreso.getWidth() / (multi / 2),
				lbl_contrasena.getY() + altoTxt * 2, anchoBtn, altoBtn,
				"Iniciar Sesi�n", panel_ingreso, this) {
			private static final long serialVersionUID = 7302993027107284040L;

			@Override
			public void accionBoton() {
				accionBotonIngreso();
			}
		};

		btn_salir = new Boton(btn_ingreso.getX() + btn_ingreso.getWidth()
				+ altoTxt, btn_ingreso.getY(), anchoBtn, altoBtn, "Salir",
				panel_ingreso, this) {
			private static final long serialVersionUID = 1078157990997062141L;

			@Override
			public void accionBoton() {
				accionBotonSalir();
			}
		};
	}

	/**
	 * Define las acciones en el login.
	 */
	private void definaAccionesIngreso() {
		txt_cedula.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent evt) {
				char KeyChar = evt.getKeyChar();
				if (Character.isLowerCase(KeyChar)) {
					evt.setKeyChar(Character.toUpperCase(KeyChar));
				}
			}

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_ENTER:
				case KeyEvent.VK_DOWN: {
					txt_contrasena.grabFocus();
				}
					break;
				}
			}
		});

		txt_contrasena.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_DOWN: {
					btn_ingreso.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_ingreso.grabFocus();
					accionBotonIngreso();
				}
					break;
				case KeyEvent.VK_UP: {
					txt_cedula.grabFocus();
				}
					break;
				}
			}
		});

		btn_ingreso.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_contrasena.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT: {
					btn_salir.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_ingreso.doClick();
				}
					break;
				}
			}
		});

		btn_salir.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					btn_ingreso.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_salir.doClick();
				}
					break;
				}
			}
		});
	}

	/**
	 * Permite hacer login.
	 */
	private void accionBotonIngreso() {// TODO Boton Iniciar Sesion
		if (!txt_cedula.getText().trim().isEmpty()) {
			String cedula = txt_cedula.getText().trim();
			String contrasena = Utils.sha1(String.valueOf(txt_contrasena
					.getPassword()));

			Docente docente = controller.autenticar(cedula, contrasena);

			if (docente != null) {
				nuevaVentana(new Menu(this, docente));
			} else {
				JOptionPane.showOptionDialog(null,
						"Los datos de ingreso se sesi�n no son correctos.",
						"Datos incorrectos", JOptionPane.DEFAULT_OPTION,
						JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				txt_cedula.grabFocus();
				txt_contrasena.setText("");
			}
		} else if (txt_cedula.getText().trim().isEmpty()) {
			JOptionPane.showOptionDialog(null, "Debe ingresar la c�dula.",
					"Falta c�dula", JOptionPane.DEFAULT_OPTION,
					JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
			txt_cedula.grabFocus();
		}
	}

	/**
	 * Salir del programa.
	 */
	private void accionBotonSalir() {// TODO Boton Salir
		cerrarPrograma(this);
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
	}
}
