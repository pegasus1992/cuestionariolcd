package view;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import model.*;
import controller.*;
import exception.*;
import view.jasper.*;
import view.utils.*;

public class ConsultarPregunta extends General {
	private static final long serialVersionUID = 1978443419975432563L;

	// Panel Formulario
	private JPanel panel_formulario;

	// Contenido Formulario
	private JLabel lbl_id, lbl_grado, lbl_area, lbl_asignatura, lbl_tema,
			lbl_subtema, lbl_competencia, lbl_nivel;
	private JTextField txt_id, txt_grado, txt_area, txt_asignatura, txt_tema,
			txt_subtema, txt_competencia, txt_nivel;

	// Contenido Formulario
	private JLabel lbl_enunciado, lbl_enunciado2, lbl_rutaImagen,
			lbl_respuestas;
	private JTextArea txt_enunciado, txt_enunciado2;
	private JTextField txt_opcion1, txt_opcion2, txt_opcion3, txt_opcion4,
			txt_opcion5;
	private JRadioButton rbtn_opcion1, rbtn_opcion2, rbtn_opcion3,
			rbtn_opcion4, rbtn_opcion5;

	// Panel de Imagen
	private JPanel panel_imagen;
	private JLabel lbl_imagen;

	// Panel Botones
	private JPanel panel_botones;
	private JButton btn_reporte, btn_regresar;

	// Utilitarios
	private static final String titulo = "Consultar Pregunta";
	private Docente docente;
	private Pregunta pregunta;
	private Grado grado;
	private Area area;
	private Materia materia;
	private Tema tema, subtema;
	private ArrayList<Opcion> opciones;
	private String idPregunta;
	private ConsultarPreguntaController controller;

	// private static final String usoConocimiento = "Uso del Conocimiento";
	// private static final String explicativa = "Explicativa";
	// private static final String indagacion = "Indagaci�n";
	// private static final String conceptual = "Conceptual";

	/**
	 * Inicializa la ventana de consultar pregunta.
	 * 
	 * @param anterior
	 *            Ventana anterior.
	 * @param docente
	 * @param idPregunta
	 */
	public ConsultarPregunta(JFrame anterior, Docente docente, String idPregunta) {
		super(anterior, anchoPantalla, 8 * altoPantalla / 9);
		this.docente = docente;
		this.idPregunta = idPregunta;
		controller = new ConsultarPreguntaController();

		prepareElementosPanelFormulario();
		relleneElementosPanelFormulario(idPregunta);
		prepareElementosPanelBotones();
		definaAccionesBotones();
	}

	/**
	 * Rellena los campos con la informacion de la pregunta dada.
	 * 
	 * @param idPregunta
	 */
	private void relleneElementosPanelFormulario(String idPregunta) {

		// Se obtienen los datos de la Pregunta.
		pregunta = controller.obtenerPregunta(idPregunta);
		grado = pregunta.getGrado();
		area = pregunta.getArea();
		materia = pregunta.getMateria();
		subtema = pregunta.getSubtema();
		tema = subtema.getPadre();
		opciones = controller.obtenerOpciones(pregunta);

		txt_id.setText(pregunta.getId());
		txt_grado.setText(grado.getNombre());
		txt_area.setText(area.getNombre());
		txt_asignatura.setText(materia.getNombre());
		txt_tema.setText(tema.getNombre());
		txt_subtema.setText(subtema.getNombre());
		txt_competencia.setText(pregunta.getCompetencia());
		txt_nivel.setText(pregunta.getNivel());

		txt_enunciado.setText(pregunta.getEnunciado());
		String rutaImagen = pregunta.getRutaImagen();
		if (rutaImagen != null && !rutaImagen.trim().isEmpty()
				&& !rutaImagen.equals("null")) {
			lbl_imagen.setIcon(Imagen.convert2Image(rutaImagen,
					panel_imagen.getWidth(), panel_imagen.getHeight() - 10));// TODO
		}
		String enunciadoEspecifico = pregunta.getEnunciadoEspecifico();
		if (enunciadoEspecifico != null
				&& !enunciadoEspecifico.trim().isEmpty()
				&& !enunciadoEspecifico.equals("null")) {
			txt_enunciado2.setText(enunciadoEspecifico);
		}

		Opcion opcion1 = opciones.get(0);
		Opcion opcion2 = opciones.get(1);
		Opcion opcion3 = opciones.get(2);
		Opcion opcion4 = opciones.get(3);
		Opcion opcion5 = null;
		if (opciones.size() > 4) {
			opcion5 = opciones.get(4);
		}

		rbtn_opcion1.setSelected(opcionSi(opcion1));
		rbtn_opcion2.setSelected(opcionSi(opcion2));
		rbtn_opcion3.setSelected(opcionSi(opcion3));
		rbtn_opcion4.setSelected(opcionSi(opcion4));
		if (opcion5 != null) {
			rbtn_opcion5.setSelected(opcionSi(opcion5));
		}

		txt_opcion1.setText(opcion1.getRespuesta());
		txt_opcion2.setText(opcion2.getRespuesta());
		txt_opcion3.setText(opcion3.getRespuesta());
		txt_opcion4.setText(opcion4.getRespuesta());
		if (opcion5 != null) {
			txt_opcion5.setText(opcion5.getRespuesta());
		}
	}

	/**
	 * Responde a la pregunta: <br>
	 * 
	 * <pre>
	 * �La opcion actual es correcta?
	 * </pre>
	 * 
	 * @param opcion
	 * @return
	 */
	private boolean opcionSi(Opcion opcion) {
		return opcion.getCorrecta().equals("S");
	}

	/**
	 * Prepara los elementos del panel de formulario.
	 */
	private void prepareElementosPanelFormulario() {
		int anchoP = anchoPantalla / 10;
		int x = anchoP / 7, alto = anchoP / 5;
		int aumentoX = anchoP / 5, aumentoY = anchoP / 3, reduccion = anchoP / 10;
		int anchoLbl = 3 * anchoP / 4, anchoTxt = 2 * anchoP;
		int comboGrande = anchoTxt, comboPeque�o = anchoTxt / 2, radio = 4 * comboPeque�o / 9;
		int aumentoCombo = 6;

		panel_formulario = new JPanel();
		panel_formulario.setLocation(0, panel_barraTitulos.getHeight());
		panel_formulario.setSize(this.getWidth(), this.getHeight()
				- panel_barraTitulos.getHeight() * 2);
		panel_formulario.setLayout(null);
		panel_formulario.setOpaque(false);
		this.add(panel_formulario);

		lbl_id = new JLabel("ID: ", JLabel.LEFT);
		lbl_id.setLocation(x, panel_formulario.getY() - reduccion);
		lbl_id.setSize(anchoLbl, alto);
		lbl_id.setForeground(letra.getColorLabel());
		lbl_id.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_id);

		txt_id = new JTextField();
		txt_id.setLocation(lbl_id.getWidth() + aumentoX, lbl_id.getY());
		txt_id.setSize(comboPeque�o, alto);
		txt_id.setFont(letra.getFuenteTxt());
		txt_id.setForeground(letra.getColorTxt());
		txt_id.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_id);
		txt_id.setEnabled(false);

		lbl_grado = new JLabel("Grado: ", JLabel.LEFT);
		lbl_grado.setLocation(lbl_id.getX(), lbl_id.getY() + aumentoY);
		lbl_grado.setSize(lbl_id.getWidth(), lbl_id.getHeight());
		lbl_grado.setForeground(letra.getColorLabel());
		lbl_grado.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_grado);

		txt_grado = new JTextField();
		txt_grado.setLocation(txt_id.getX(), lbl_grado.getY());
		txt_grado.setSize(comboGrande, txt_id.getHeight());
		txt_grado.setFont(letra.getFuenteTxt());
		txt_grado.setForeground(letra.getColorTxt());
		txt_grado.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_grado);
		txt_grado.setEnabled(false);

		lbl_area = new JLabel("�rea: ", JLabel.LEFT);
		lbl_area.setLocation(lbl_grado.getX(), lbl_grado.getY() + aumentoY);
		lbl_area.setSize(lbl_grado.getWidth(), lbl_grado.getHeight());
		lbl_area.setForeground(letra.getColorLabel());
		lbl_area.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_area);

		txt_area = new JTextField();
		txt_area.setLocation(txt_grado.getX(), lbl_area.getY());
		txt_area.setSize(comboGrande, txt_grado.getHeight());
		txt_area.setFont(letra.getFuenteTxt());
		txt_area.setForeground(letra.getColorTxt());
		txt_area.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_area);
		txt_area.setEnabled(false);

		lbl_asignatura = new JLabel("Asignatura: ", JLabel.LEFT);
		lbl_asignatura.setLocation(lbl_area.getX(), lbl_area.getY() + aumentoY);
		lbl_asignatura.setSize(lbl_area.getWidth(), lbl_area.getHeight());
		lbl_asignatura.setForeground(letra.getColorLabel());
		lbl_asignatura.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_asignatura);

		txt_asignatura = new JTextField();
		txt_asignatura.setLocation(txt_area.getX(), lbl_asignatura.getY());
		txt_asignatura.setSize(comboGrande, txt_area.getHeight());
		txt_asignatura.setFont(letra.getFuenteTxt());
		txt_asignatura.setForeground(letra.getColorTxt());
		txt_asignatura.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_asignatura);
		txt_asignatura.setEnabled(false);

		lbl_tema = new JLabel("Tema: ", JLabel.LEFT);
		lbl_tema.setLocation(lbl_asignatura.getX(), lbl_asignatura.getY()
				+ aumentoY);
		lbl_tema.setSize(lbl_asignatura.getWidth(), lbl_asignatura.getHeight());
		lbl_tema.setForeground(letra.getColorLabel());
		lbl_tema.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_tema);

		txt_tema = new JTextField();
		txt_tema.setLocation(txt_asignatura.getX(), lbl_tema.getY());
		txt_tema.setSize(comboGrande, txt_asignatura.getHeight());
		txt_tema.setFont(letra.getFuenteTxt());
		txt_tema.setForeground(letra.getColorTxt());
		txt_tema.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_tema);
		txt_tema.setEnabled(false);

		lbl_subtema = new JLabel("Subtema: ", JLabel.LEFT);
		lbl_subtema.setLocation(lbl_tema.getX(), lbl_tema.getY() + aumentoY);
		lbl_subtema.setSize(lbl_tema.getWidth(), lbl_tema.getHeight());
		lbl_subtema.setForeground(letra.getColorLabel());
		lbl_subtema.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_subtema);

		txt_subtema = new JTextField();
		txt_subtema.setLocation(txt_tema.getX(), lbl_subtema.getY());
		txt_subtema.setSize(comboGrande, txt_tema.getHeight());
		txt_subtema.setFont(letra.getFuenteTxt());
		txt_subtema.setForeground(letra.getColorTxt());
		txt_subtema.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_subtema);
		txt_subtema.setEnabled(false);

		lbl_competencia = new JLabel("Competencia: ", JLabel.LEFT);
		lbl_competencia.setLocation(lbl_subtema.getX(), lbl_subtema.getY()
				+ aumentoY);
		lbl_competencia
				.setSize(lbl_subtema.getWidth(), lbl_subtema.getHeight());
		lbl_competencia.setForeground(letra.getColorLabel());
		lbl_competencia.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_competencia);

		txt_competencia = new JTextField();
		txt_competencia.setLocation(txt_subtema.getX(), lbl_competencia.getY());
		txt_competencia.setSize(comboPeque�o, txt_subtema.getHeight());
		txt_competencia.setFont(letra.getFuenteTxt());
		txt_competencia.setForeground(letra.getColorTxt());
		txt_competencia.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_competencia);
		txt_competencia.setEnabled(false);

		lbl_nivel = new JLabel("Nivel: ", JLabel.LEFT);
		lbl_nivel.setLocation(txt_competencia.getWidth() + aumentoX
				* aumentoCombo, txt_competencia.getY());
		lbl_nivel.setSize(lbl_competencia.getWidth(),
				lbl_competencia.getHeight());
		lbl_nivel.setForeground(letra.getColorLabel());
		lbl_nivel.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_nivel);

		txt_nivel = new JTextField();
		txt_nivel.setLocation(lbl_nivel.getWidth() + aumentoX * aumentoCombo
				* 2, lbl_nivel.getY());
		txt_nivel.setSize(comboPeque�o, txt_competencia.getHeight());
		txt_nivel.setFont(letra.getFuenteTxt());
		txt_nivel.setForeground(letra.getColorTxt());
		txt_nivel.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_nivel);
		txt_nivel.setEnabled(false);

		lbl_enunciado = new JLabel("Enunciado: ", JLabel.LEFT);
		lbl_enunciado.setLocation(txt_nivel.getX() + txt_nivel.getWidth()
				+ aumentoX / 3 * aumentoCombo, panel_formulario.getY()
				- reduccion);
		lbl_enunciado.setSize(anchoLbl, alto);
		lbl_enunciado.setForeground(letra.getColorLabel());
		lbl_enunciado.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_enunciado);

		txt_enunciado = new JTextArea();
		txt_enunciado.setLocation(
				lbl_enunciado.getX() + lbl_enunciado.getWidth() + aumentoX,
				lbl_enunciado.getY());
		txt_enunciado.setSize(comboGrande + anchoP * 7 / 3, alto * 3);
		txt_enunciado.setFont(letra.getFuenteTxt());
		txt_enunciado.setForeground(letra.getColorTxt());
		txt_enunciado.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_enunciado);
		// txt_enunciado.setColumns(25);
		// txt_enunciado.setRows(3);
		txt_enunciado.setAutoscrolls(false);
		txt_enunciado.setLineWrap(true);
		txt_enunciado.setEnabled(false);

		lbl_rutaImagen = new JLabel("Imagen: ", JLabel.LEFT);
		lbl_rutaImagen.setLocation(lbl_enunciado.getX(), lbl_enunciado.getY()
				+ lbl_enunciado.getHeight() + aumentoY * 2);
		lbl_rutaImagen.setSize(anchoLbl, alto);
		lbl_rutaImagen.setForeground(letra.getColorLabel());
		lbl_rutaImagen.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_rutaImagen);

		panel_imagen = new JPanel();
		panel_imagen.setLocation(txt_enunciado.getX(), txt_enunciado.getY() + 2
				* aumentoY);
		panel_imagen.setSize(2 * anchoP, anchoP);
		panel_imagen.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel_formulario.add(panel_imagen);

		lbl_imagen = new JLabel();
		panel_imagen.add(lbl_imagen);

		lbl_enunciado2 = new JLabel("Enunciado Esp: ", JLabel.LEFT);
		lbl_enunciado2.setLocation(lbl_rutaImagen.getX(), panel_imagen.getY()
				+ panel_imagen.getHeight() + aumentoY / 2);
		lbl_enunciado2.setSize(anchoLbl, alto);
		lbl_enunciado2.setForeground(letra.getColorLabel());
		lbl_enunciado2.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_enunciado2);

		txt_enunciado2 = new JTextArea();
		txt_enunciado2.setLocation(
				lbl_enunciado2.getX() + lbl_enunciado2.getWidth() + aumentoX,
				lbl_enunciado2.getY());
		txt_enunciado2.setSize(txt_enunciado.getWidth(), alto * 3);
		txt_enunciado2.setFont(letra.getFuenteTxt());
		txt_enunciado2.setForeground(letra.getColorTxt());
		txt_enunciado2.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_enunciado2);
		txt_enunciado2.setAutoscrolls(false);
		txt_enunciado2.setLineWrap(true);
		txt_enunciado2.setEnabled(false);

		lbl_respuestas = new JLabel("Respuestas: ", JLabel.LEFT);
		lbl_respuestas.setLocation(lbl_competencia.getX(),
				txt_enunciado2.getY() + txt_enunciado2.getHeight());
		lbl_respuestas.setSize(anchoLbl, alto);
		lbl_respuestas.setForeground(letra.getColorLabel());
		lbl_respuestas.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_respuestas);

		rbtn_opcion1 = new JRadioButton("A");
		rbtn_opcion1.setLocation(lbl_respuestas.getX() + 2 * x,
				lbl_respuestas.getY() + lbl_respuestas.getHeight());
		rbtn_opcion1.setSize(radio, alto);
		rbtn_opcion1.setOpaque(false);
		rbtn_opcion1.setContentAreaFilled(false);
		rbtn_opcion1.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion1);
		rbtn_opcion1.setEnabled(false);

		txt_opcion1 = new JTextField();
		txt_opcion1.setLocation(txt_competencia.getX(), rbtn_opcion1.getY());
		txt_opcion1.setSize(txt_enunciado.getWidth(), alto);
		txt_opcion1.setFont(letra.getFuenteTxt());
		txt_opcion1.setForeground(letra.getColorTxt());
		txt_opcion1.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion1);
		txt_opcion1.setEnabled(false);

		rbtn_opcion2 = new JRadioButton("B");
		rbtn_opcion2.setLocation(rbtn_opcion1.getX(), rbtn_opcion1.getY()
				+ aumentoY);
		rbtn_opcion2.setSize(radio, alto);
		rbtn_opcion2.setOpaque(false);
		rbtn_opcion2.setContentAreaFilled(false);
		rbtn_opcion2.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion2);
		rbtn_opcion2.setEnabled(false);

		txt_opcion2 = new JTextField();
		txt_opcion2.setLocation(txt_opcion1.getX(), rbtn_opcion2.getY());
		txt_opcion2.setSize(txt_opcion1.getWidth(), txt_opcion1.getHeight());
		txt_opcion2.setFont(letra.getFuenteTxt());
		txt_opcion2.setForeground(letra.getColorTxt());
		txt_opcion2.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion2);
		txt_opcion2.setEnabled(false);

		rbtn_opcion3 = new JRadioButton("C");
		rbtn_opcion3.setLocation(rbtn_opcion2.getX(), rbtn_opcion2.getY()
				+ aumentoY);
		rbtn_opcion3.setSize(radio, alto);
		rbtn_opcion3.setOpaque(false);
		rbtn_opcion3.setContentAreaFilled(false);
		rbtn_opcion3.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion3);
		rbtn_opcion3.setEnabled(false);

		txt_opcion3 = new JTextField();
		txt_opcion3.setLocation(txt_opcion2.getX(), rbtn_opcion3.getY());
		txt_opcion3.setSize(txt_opcion2.getWidth(), txt_opcion2.getHeight());
		txt_opcion3.setFont(letra.getFuenteTxt());
		txt_opcion3.setForeground(letra.getColorTxt());
		txt_opcion3.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion3);
		txt_opcion3.setEnabled(false);

		rbtn_opcion4 = new JRadioButton("D");
		rbtn_opcion4.setLocation(rbtn_opcion3.getX(), rbtn_opcion3.getY()
				+ aumentoY);
		rbtn_opcion4.setSize(radio, alto);
		rbtn_opcion4.setOpaque(false);
		rbtn_opcion4.setContentAreaFilled(false);
		rbtn_opcion4.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion4);
		rbtn_opcion4.setEnabled(false);

		txt_opcion4 = new JTextField();
		txt_opcion4.setLocation(txt_opcion3.getX(), rbtn_opcion4.getY());
		txt_opcion4.setSize(txt_opcion3.getWidth(), txt_opcion3.getHeight());
		txt_opcion4.setFont(letra.getFuenteTxt());
		txt_opcion4.setForeground(letra.getColorTxt());
		txt_opcion4.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion4);
		txt_opcion4.setEnabled(false);

		rbtn_opcion5 = new JRadioButton("E");
		rbtn_opcion5.setLocation(rbtn_opcion4.getX(), rbtn_opcion4.getY()
				+ aumentoY);
		rbtn_opcion5.setSize(radio, alto);
		rbtn_opcion5.setOpaque(false);
		rbtn_opcion5.setContentAreaFilled(false);
		rbtn_opcion5.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion5);
		rbtn_opcion5.setEnabled(false);

		txt_opcion5 = new JTextField();
		txt_opcion5.setLocation(txt_opcion4.getX(), rbtn_opcion5.getY());
		txt_opcion5.setSize(txt_opcion4.getWidth(), txt_opcion4.getHeight());
		txt_opcion5.setFont(letra.getFuenteTxt());
		txt_opcion5.setForeground(letra.getColorTxt());
		txt_opcion5.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion5);
		txt_opcion5.setEnabled(false);
	}

	/**
	 * Prepara los elementos del panel de botones.
	 */
	private void prepareElementosPanelBotones() {
		int anchoBtn = 150;
		int altoBtn = 30;

		panel_botones = new JPanel();
		panel_botones.setLocation(panel_formulario.getWidth() / 3,
				panel_formulario.getHeight());
		panel_botones.setSize(panel_formulario.getWidth() / 3, altoBtn);
		panel_botones.setLayout(new GridLayout(1, 0));
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_reporte = new Boton(0, 0, anchoBtn, altoBtn, "Reporte",
				panel_botones, this) {
			private static final long serialVersionUID = -6742995039980010412L;

			@Override
			public void accionBoton() {
				accionBotonReporte();
			}
		};

		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar",
				panel_botones, this) {
			private static final long serialVersionUID = -4456446890553142037L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
	}

	/**
	 * Define las acciones dentro del panel de botones.
	 */
	private void definaAccionesBotones() {

		btn_reporte.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_ENTER: {
					btn_reporte.doClick();
				}
					break;
				case KeyEvent.VK_RIGHT: {
					btn_regresar.grabFocus();
				}
					break;
				}
			}
		});
		btn_reporte.grabFocus();

		btn_regresar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_ENTER: {
					btn_regresar.doClick();
				}
					break;
				case KeyEvent.VK_LEFT: {
					btn_reporte.grabFocus();
				}
					break;
				}
			}
		});
	}

	/**
	 * Muestra el reporte de la pregunta.
	 */
	private void accionBotonReporte() {// TODO Boton Reporte
		try {
			JasperReportsBuilder report = new JasperReportsBuilder(docente);
			report.verPreguntasFiller(pregunta, grado, area, materia, tema,
					subtema, opciones);
		} catch (JasperException ex) {
			JOptionPane.showOptionDialog(null, ex.getMessage(),
					"Error ocurrido", JOptionPane.DEFAULT_OPTION,
					JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
		}
	}

	/**
	 * Regresar a la ventana anterior.
	 */
	private void accionBotonRegresar() {// TODO Boton Regresar
		cerrarVentana();
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
		relleneElementosPanelFormulario(idPregunta);
	}
}
