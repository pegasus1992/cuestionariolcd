package view;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;

import javax.swing.*;

import model.*;
import controller.*;
import view.utils.*;

public class CambiarContrasenia extends General {
	private static final long serialVersionUID = 4635084672938375869L;

	// Panel Formulario
	private JPanel panel_formulario;

	// Contenido Formulario
	private JLabel lbl_vieja, lbl_nueva, lbl_confirme;
	private JPasswordField txt_vieja, txt_nueva, txt_confirme;

	// Panel Botones
	private JPanel panel_botones;
	private JButton btn_cambiar, btn_regresar;

	// Utilitarios
	private static final String titulo = "Cambiar Contrase�a";
	private Docente docente;
	private CambiarContraseniaController controller;

	/**
	 * Inicializa la ventana de cambiar contrasenia.
	 * 
	 * @param anterior
	 *            Ventana anterior.
	 * @param docente
	 */
	protected CambiarContrasenia(JFrame anterior, Docente docente) {
		super(anterior, 3 * anchoPantalla / 9, 5 * altoPantalla / 17);
		this.docente = docente;
		controller = new CambiarContraseniaController();

		prepareElementosPanelFormulario();
		definaAccionesFormulario();
		prepareElementosPanelBotones();
		definaAccionesBotones();
	}

	/**
	 * Prepara los elementos del panel de formulario.
	 */
	private void prepareElementosPanelFormulario() {
		int anchoP = anchoPantalla / 10;
		int x = anchoP / 7, alto = anchoP / 5;
		int aumentoX = anchoP / 5, aumentoY = anchoP / 3, reduccion = anchoP / 10;
		int anchoLbl = 5 * anchoP / 6, anchoTxt = 2 * anchoP;
		int comboGrande = anchoTxt;

		panel_formulario = new JPanel();
		panel_formulario.setLocation(0, panel_barraTitulos.getHeight());
		panel_formulario.setSize(this.getWidth(), this.getHeight()
				- panel_barraTitulos.getHeight() * 2);
		panel_formulario.setLayout(null);
		panel_formulario.setOpaque(false);
		this.add(panel_formulario);

		lbl_vieja = new JLabel("Contrase�a Vieja: ", JLabel.LEFT);
		lbl_vieja.setLocation(x, panel_formulario.getY() - reduccion);
		lbl_vieja.setSize(anchoLbl, alto);
		lbl_vieja.setForeground(letra.getColorLabel());
		lbl_vieja.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_vieja);

		txt_vieja = new JPasswordField();
		txt_vieja
				.setLocation(lbl_vieja.getWidth() + aumentoX, lbl_vieja.getY());
		txt_vieja.setSize(comboGrande, alto);
		txt_vieja.setFont(letra.getFuenteTxt());
		txt_vieja.setForeground(letra.getColorTxt());
		txt_vieja.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_vieja);

		lbl_nueva = new JLabel("Contrase�a Nueva: ", JLabel.LEFT);
		lbl_nueva.setLocation(lbl_vieja.getX(), lbl_vieja.getY() + aumentoY);
		lbl_nueva.setSize(lbl_vieja.getWidth(), lbl_vieja.getHeight());
		lbl_nueva.setForeground(letra.getColorLabel());
		lbl_nueva.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_nueva);

		txt_nueva = new JPasswordField();
		txt_nueva
				.setLocation(lbl_nueva.getWidth() + aumentoX, lbl_nueva.getY());
		txt_nueva.setSize(comboGrande, alto);
		txt_nueva.setFont(letra.getFuenteTxt());
		txt_nueva.setForeground(letra.getColorTxt());
		txt_nueva.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_nueva);

		lbl_confirme = new JLabel("Confirme Clave: ", JLabel.LEFT);
		lbl_confirme.setLocation(lbl_nueva.getX(), lbl_nueva.getY() + aumentoY);
		lbl_confirme.setSize(lbl_nueva.getWidth(), lbl_nueva.getHeight());
		lbl_confirme.setForeground(letra.getColorLabel());
		lbl_confirme.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_confirme);

		txt_confirme = new JPasswordField();
		txt_confirme.setLocation(lbl_confirme.getWidth() + aumentoX,
				lbl_confirme.getY());
		txt_confirme.setSize(comboGrande, alto);
		txt_confirme.setFont(letra.getFuenteTxt());
		txt_confirme.setForeground(letra.getColorTxt());
		txt_confirme.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_confirme);
	}

	/**
	 * Define las acciones dentro del panel de formulario.
	 */
	private void definaAccionesFormulario() {

		txt_vieja.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_ENTER:
				case KeyEvent.VK_DOWN: {
					txt_nueva.grabFocus();
				}
					break;
				}
			}
		});

		txt_nueva.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_vieja.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN:
				case KeyEvent.VK_ENTER: {
					txt_confirme.grabFocus();
				}
					break;
				}
			}
		});

		txt_confirme.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_nueva.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					btn_cambiar.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_cambiar.grabFocus();
					accionBotonCambiarContrasenia();
				}
					break;
				}
			}
		});
	}

	/**
	 * Prepara el panel de botones.
	 */
	private void prepareElementosPanelBotones() {
		int anchoBtn = 150;
		int altoBtn = 30;

		panel_botones = new JPanel();
		panel_botones.setLocation(panel_formulario.getWidth() / 4,
				panel_formulario.getHeight());
		panel_botones.setSize(panel_formulario.getWidth() / 2, altoBtn);
		panel_botones.setLayout(new GridLayout(1, 0));
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_cambiar = new Boton(0, 0, anchoBtn, altoBtn, "Cambiar",
				panel_botones, this) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonCambiarContrasenia();
			}
		};

		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar",
				panel_botones, this) {
			private static final long serialVersionUID = -4456446890553142037L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
	}

	/**
	 * Define las acciones dentro del panel de botones.
	 */
	private void definaAccionesBotones() {

		btn_cambiar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_confirme.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT: {
					btn_regresar.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_cambiar.doClick();
				}
					break;
				}
			}
		});

		btn_regresar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					btn_cambiar.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_regresar.doClick();
				}
					break;
				}
			}
		});
	}

	/**
	 * Realizar el cambio de contrasenia.
	 */
	private void accionBotonCambiarContrasenia() {
		// TODO Boton Cambiar Contrasenia

		String vieja = String.valueOf(txt_vieja.getPassword());
		String nueva = String.valueOf(txt_nueva.getPassword());
		String confirme = String.valueOf(txt_confirme.getPassword());

		String claveVieja = Utils.sha1(vieja);
		if (claveVieja != null) {
			if (docente.getContrasena().equals(claveVieja)) {
				if (nueva.equals(confirme)) {
					if (!nueva.equals(vieja)) {
						String claveNueva = Utils.sha1(nueva);
						if (claveNueva != null) {
							try {
								controller.cambiarContrasenia(docente,
										claveNueva);
								JOptionPane
										.showMessageDialog(
												null,
												"Contrase�a cambiada con �xito.\nSe cerrar� sesi�n para guardar los cambios",
												"Informaci�n",
												JOptionPane.INFORMATION_MESSAGE);
								cerrarSesion();
							} catch (SQLException e) {
								JOptionPane.showMessageDialog(null,
										e.getMessage(), "Error",
										JOptionPane.INFORMATION_MESSAGE);
								e.printStackTrace();
							}
						}
					} else {
						JOptionPane
								.showOptionDialog(
										null,
										"Por seguridad, la contrase�a nueva debe ser diferente a la vieja.",
										"Contrase�as no iguales",
										JOptionPane.DEFAULT_OPTION,
										JOptionPane.WARNING_MESSAGE, null,
										null, "aceptar");
						txt_nueva.grabFocus();
					}
				} else {
					JOptionPane.showOptionDialog(null,
							"Las contrase�as nuevas no coinciden.",
							"Contrase�as no coinciden",
							JOptionPane.DEFAULT_OPTION,
							JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_confirme.grabFocus();
				}
			} else {
				JOptionPane.showOptionDialog(null,
						"La contrase�a vieja no es correcta",
						"Contrase�a incorrecta", JOptionPane.DEFAULT_OPTION,
						JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				txt_vieja.grabFocus();
			}
		}
	}

	/**
	 * Regresar a la ventana anterior.
	 */
	private void accionBotonRegresar() {// TODO Boton Regresar
		cerrarVentana();
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
	}
}
