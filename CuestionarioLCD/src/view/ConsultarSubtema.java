package view;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import view.utils.Boton;
import view.utils.General;
import view.utils.MiRender;
import view.utils.TablaNoEditable;
import model.Docente;
import model.Materia;
import model.Tema;
import controller.CrearTemaController;
import controller.Filtro;

public class ConsultarSubtema extends General {

	private JFrame frame;
	private JTextField txt_Subtema, txt_idSubTema, txt_nombreSubTema;
	private JLabel lbl_nombreB, lbl_nombreM , lbl_asignatura, lbl_tema;
	private JComboBox<String> combo_Asignatura, combo_Tema;
	private JPanel panel_botones;
	
	private static final String titulo = "Consultar SubTema";
	private CrearTemaController controller;
	///////////////////PANEL PREPARA TABLA PRINCIPAL///////////////////////
	public DefaultTableModel dtmTablaPrincipal;
	public JTable tablaPrincipal;
	public JScrollPane scrollpanePrincipal;
	public String DatosTablaPrincipal[];
	public TableRowSorter<DefaultTableModel> trsfiltrotablaPrincipal;
	
	private JButton btn_cancelar, btn_guardar, btn_eliminar, btn_modificar, btn_regresar;
	
	public Filtro filtros;
		
	int id =0;
	int nombre =1;
	int asignatura =2;
	int tema =3;
	
	private static final long serialVersionUID = -4054532557621116059L;
	
	public ConsultarSubtema(JFrame anterior, Docente docente) {
		super(anterior, 2 * anchoPantalla / 5, altoPantalla * 11 / 25);
		frame = this;
		controller = new CrearTemaController();
		filtros = new Filtro();
		prepararElementos();
		preparaElementosBotones();
		preparaTablaPrincipal();
		cargarDatosTabla();	
		accionesComponentes();
		deshacerFiltroTablaPrincipal();
	}


	/**
	 * metodo que prepara los elementos
	 */
	private void prepararElementos(){
		int posicionX = anchoPantalla/100*6;
		int posicionY = altoPantalla/100*5;
		int anchoTxt = anchoPantalla/100*20;
		
		lbl_nombreB = new JLabel("Nombre: ", JLabel.LEFT);
		lbl_nombreB.setLocation(posicionX , posicionY);
		lbl_nombreB.setSize(80, 25);
		lbl_nombreB.setForeground(letra.getColorLabel());
		lbl_nombreB.setFont(letra.getFuenteLabel());
		frame.add(lbl_nombreB);
		
		txt_idSubTema = new JTextField();
		
		txt_Subtema = new JTextField();
		txt_Subtema.setLocation(lbl_nombreB.getX()+lbl_nombreB.getWidth()/4*5,lbl_nombreB.getY());
		txt_Subtema.setSize(anchoTxt, 25);
		txt_Subtema.setFont(letra.getFuenteTxt());
		txt_Subtema.setForeground(letra.getColorTxt());
		txt_Subtema.setDisabledTextColor(letra.getColorTxtDisable());
		frame.add(txt_Subtema);
		
		// txt para la modiciacion del tema
		lbl_nombreM = new JLabel("Nombre: ", JLabel.LEFT);
		lbl_nombreM.setLocation(posicionX , posicionY*3);
		lbl_nombreM.setSize(80, 25);
		lbl_nombreM.setForeground(letra.getColorLabel());
		lbl_nombreM.setFont(letra.getFuenteLabel());
		frame.add(lbl_nombreM);
		lbl_nombreM.setVisible(false);
		
		txt_nombreSubTema = new JTextField();
		txt_nombreSubTema.setLocation(lbl_nombreM.getX()+lbl_nombreM.getWidth()/4*5,posicionY*3);
		txt_nombreSubTema.setSize(anchoTxt, 25);
		txt_nombreSubTema.setFont(letra.getFuenteTxt());
		txt_nombreSubTema.setForeground(letra.getColorTxt());
		txt_nombreSubTema.setDisabledTextColor(letra.getColorTxtDisable());
		frame.add(txt_nombreSubTema);
		txt_nombreSubTema.setVisible(false);
		
		lbl_asignatura = new JLabel("Asignatura: ", JLabel.LEFT);
		lbl_asignatura.setLocation(posicionX , posicionY*3+25*2);
		lbl_asignatura.setSize(80, 25);
		lbl_asignatura.setForeground(letra.getColorLabel());
		lbl_asignatura.setFont(letra.getFuenteLabel());
		frame.add(lbl_asignatura);
		lbl_asignatura.setVisible(false);
		
		//combo para modificar la el tema
		combo_Asignatura = new JComboBox<String>();
		combo_Asignatura.setFont(letra.getFuenteCombo());
		combo_Asignatura.setForeground(letra.getColorCombo());
		combo_Asignatura.setLocation(txt_nombreSubTema.getX(), lbl_asignatura.getY());
		combo_Asignatura.setSize(anchoTxt, 30);
		frame.add(combo_Asignatura);
		
		// TODO Poblar combo con materias
		ArrayList<Materia> materias = controller.obtenerMaterias();
		for (int i = 0; i < materias.size(); i++) {
			combo_Asignatura.addItem(materias.get(i).getNombre());
		}
		combo_Asignatura.setSelectedIndex(-1);
		combo_Asignatura.setVisible(false);
		
		lbl_tema = new JLabel("Tema: ", JLabel.LEFT);
		lbl_tema.setLocation(posicionX , posicionY*4+25*2);
		lbl_tema.setSize(80, 25);
		lbl_tema.setForeground(letra.getColorLabel());
		lbl_tema.setFont(letra.getFuenteLabel());
		frame.add(lbl_tema);
		lbl_tema.setVisible(false);
		
		//combo para modificar la el tema
		combo_Tema = new JComboBox<String>();
		combo_Tema.setFont(letra.getFuenteCombo());
		combo_Tema.setForeground(letra.getColorCombo());
		combo_Tema.setLocation(txt_nombreSubTema.getX(), lbl_tema.getY());
		combo_Tema.setSize(anchoTxt, 30);
		frame.add(combo_Tema);
		
		combo_Tema.setVisible(false);
		
		
	}
	
	/**
	 * metodo que prepara los elementos de los botones
	 */
	private void preparaElementosBotones(){
		int anchoBtn = 100;
		int altoBtn = 30;
		int posicionX = txt_Subtema.getX()-anchoBtn/2;
		panel_botones = new JPanel();
		panel_botones.setLocation(0, altoPantalla/100*42);
		panel_botones.setSize(anchoPantalla, altoBtn);
		panel_botones.setLayout(null);
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_eliminar = new Boton(0, 0, anchoBtn, altoBtn, "Eliminar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonEliminar();
			}
		};
		btn_eliminar.setLocation(posicionX, 0);
		
		btn_modificar = new Boton(0, 0, anchoBtn, altoBtn, "Modificar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonModificar();
			}
		};
		btn_modificar.setLocation(posicionX+anchoBtn+altoBtn, 0);
		
		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
		btn_regresar.setLocation(posicionX+anchoBtn*2+altoBtn*2, 0);
		
		btn_guardar = new Boton(0, 0, anchoBtn, altoBtn, "Guardar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonGuardar();;
			}
		};
		btn_guardar.setLocation(posicionX, 0);
		btn_guardar.setVisible(false);
		
		btn_cancelar = new Boton(0, 0, anchoBtn, altoBtn, "Cancelar", panel_botones, frame) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonCancelar();;
			}
		};
		btn_cancelar.setLocation(posicionX+anchoBtn*2+altoBtn*2, 0);
		btn_cancelar.setVisible(false);
	}
	/**
	 * metodo que prepara la tabla principal
	 */
	private void preparaTablaPrincipal(){
		int anchoTabla = anchoPantalla/100*35;
		int altoTabla  = altoPantalla/100*30;
		int posicionXTabla =anchoPantalla/100*2;
		int posicionYTabla =altoPantalla/100*10;
		
	  	  dtmTablaPrincipal= new TablaNoEditable();
	  	  tablaPrincipal = new JTable(dtmTablaPrincipal);
	  	  dtmTablaPrincipal.addColumn("Id");
	  	  dtmTablaPrincipal.addColumn("Nombre");
	  	  dtmTablaPrincipal.addColumn("Materia");
	  	  dtmTablaPrincipal.addColumn("Tema");

	  	  tablaPrincipal.setPreferredScrollableViewportSize(new Dimension(anchoTabla - 30,altoTabla -30));
		  		
		  	  scrollpanePrincipal = new JScrollPane(tablaPrincipal);
		  	  frame.add(scrollpanePrincipal);
		  	  scrollpanePrincipal.setLocation(posicionXTabla, posicionYTabla);
		  	  scrollpanePrincipal.setSize(anchoTabla,altoTabla);
		  		
		  	  tablaPrincipal.getColumnModel().getColumn(0).setMaxWidth(0);
		  	  tablaPrincipal.getColumnModel().getColumn(0).setMinWidth(0);
		  	  tablaPrincipal.getColumnModel().getColumn(0).setPreferredWidth(0);
		  		
		  	  tablaPrincipal.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		  	  tablaPrincipal.getTableHeader().setReorderingAllowed(false);
		  	  tablaPrincipal.setDefaultRenderer(Object.class, new MiRender());
		  	  tablaPrincipal.setShowHorizontalLines(false);
		  	  tablaPrincipal.setBorder(null);
		  	  tablaPrincipal.setOpaque(false);
		  	  scrollpanePrincipal.setOpaque(false);
		  	  scrollpanePrincipal.getViewport().setOpaque(false);
		  	  scrollpanePrincipal.setBorder(null);
	    
	}
	/**
	 * metodo que carga la tabla principal
	 */
	private void cargarDatosTabla() {
		dtmTablaPrincipal.setRowCount(0);
		ArrayList<String[]> tema = controller.obtenerTodoSubtema();

		for (int i = 0; i<=tema.size();i++) {
			DatosTablaPrincipal = new String[dtmTablaPrincipal.getColumnCount()];

			try {
				DatosTablaPrincipal[id] = tema.get(i)[0];
				DatosTablaPrincipal[nombre] = tema.get(i)[1];
				DatosTablaPrincipal[asignatura] = tema.get(i)[5];
				DatosTablaPrincipal[this.tema] = tema.get(i)[11];
				dtmTablaPrincipal.addRow(DatosTablaPrincipal);
			} catch (Exception ex) {
			}
		}
	}
	
	/**
	 * metodo que carga los datos en el formulario para poder modificarlos
	 */
	public void cargarDatosModificar(int fila){
		txt_nombreSubTema.setText(tablaPrincipal.getValueAt(fila, nombre).toString());
		combo_Asignatura.setSelectedItem(tablaPrincipal.getValueAt(fila, asignatura).toString());
		llenarComboTema(combo_Asignatura);
		combo_Tema.setSelectedItem(tablaPrincipal.getValueAt(fila, tema).toString());
		txt_idSubTema.setText(tablaPrincipal.getValueAt(fila, id).toString());
	}
	
	/**
	 * metodo que contiene la accion del boton eliminar
	 */
	private void accionBotonEliminar(){
		int fila = tablaPrincipal.getSelectedRow();
		if (fila!= -1) {
			String valido = controller.validarSiSubTemaTienePregunta(tablaPrincipal.getValueAt(fila, id).toString());
			if (valido.equals("NOEXISTE")) {
				int resultado = JOptionPane.showOptionDialog(frame, "�Esta seguro que desea eliminar el SubTema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+"?", "Advertencia",JOptionPane.OK_OPTION,JOptionPane.QUESTION_MESSAGE,null,null,"aceptar");
				if (resultado ==0) {
					boolean resul = controller.eliminarTema(tablaPrincipal.getValueAt(fila, id).toString());
					if (resul) {
						JOptionPane.showMessageDialog(null, "El Subtema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+". Fue eliminado correctamente", "Confirmacion",JOptionPane.INFORMATION_MESSAGE);
						cargarDatosTabla();
						deshacerFiltroTablaPrincipal();
						txt_Subtema.setText("");
						txt_Subtema.grabFocus();
					}else{
						JOptionPane.showMessageDialog(null, "No fue Posible borrar el subtema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+".", "Advertencia",JOptionPane.ERROR_MESSAGE);
					}
				}
			}else if(valido.equals("ERROR")){
				JOptionPane.showMessageDialog(null, "Ha ocurrido un error con la base de datos", "Error",JOptionPane.ERROR_MESSAGE);
			}else if(valido.equals("EXISTE")){
				JOptionPane.showMessageDialog(null, "No es Posible borrar el subtema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+". Ya tiene preguntas asociadas ", "Advertencia",JOptionPane.WARNING_MESSAGE);
			}
		}else{
			JOptionPane.showMessageDialog(null, "No ha Seleccionado ninguna fila", "Advertencia",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * metodo que contiene la accion del boton modificar
	 */
	private void accionBotonModificar(){
		int fila = tablaPrincipal.getSelectedRow();
		if (fila!= -1) {
			String valido = controller.validarSiSubTemaTienePregunta(tablaPrincipal.getValueAt(fila, id).toString());
			if (valido.equals("NOEXISTE")) {
				ocultarBuscar();
				MostrarModificar();
				cargarDatosModificar(fila);
			}else if(valido.equals("ERROR")){
				JOptionPane.showMessageDialog(null, "Ha ocurrido un error con la base de datos", "Error",JOptionPane.ERROR_MESSAGE);
			}else if(valido.equals("EXISTE")){
				ocultarBuscar();
				MostrarModificar();
				combo_Asignatura.setEnabled(false);
				combo_Tema.setEnabled(false);
				cargarDatosModificar(fila);
				JOptionPane.showMessageDialog(null, "El subtema: "+tablaPrincipal.getValueAt(fila, nombre).toString()+" tiene Preguntas asociadas, Por lo tanto no es posible modificar la asignatura ni el tema.", "Advertencia",JOptionPane.WARNING_MESSAGE);
				
			}
		}else{
			JOptionPane.showMessageDialog(null, "No ha Seleccionado ninguna fila", "Advertencia",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * metodo que contiene la accion del boton regresar
	 */
	private void accionBotonRegresar(){
		cerrarVentana();
	}
	
	/**
	 * metodo que contiene la accion del boton guardar
	 */
	private void accionBotonGuardar(){
		if (!txt_nombreSubTema.getText().replace(" ", "").isEmpty()) {
			Materia materia = controller.obtenerMateria(combo_Asignatura.getSelectedItem().toString());
			Tema tema = controller.obtenerTema(combo_Tema.getSelectedItem().toString());

				boolean resul = controller.ModificarTema( txt_idSubTema.getText(), txt_nombreSubTema.getText(), materia, tema);
				if (resul) {
					JOptionPane.showMessageDialog(null, "El subtema fue modificado correctamente.", "Confirmacion",JOptionPane.INFORMATION_MESSAGE);
					cargarDatosTabla();
					deshacerFiltroTablaPrincipal();
					txt_Subtema.setText("");
					txt_Subtema.grabFocus();
					ocultarModificar();
					MostrarBuscar();
				}else{
					JOptionPane.showMessageDialog(null, "No fue Posible modificar el subtema.", "Advertencia",JOptionPane.ERROR_MESSAGE);
				}

		}else{
			JOptionPane.showMessageDialog(null, "EL nombre del subtema no puede estar vacio", "Advertencia",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * metodo que contiene la accion del boton Cancelar
	 */
	private void accionBotonCancelar(){
		ocultarModificar();
		MostrarBuscar();
		deshacerFiltroTablaPrincipal();
		txt_Subtema.setText("");
		txt_Subtema.grabFocus();
	}
	
	@Override
	protected void actualizarPantalla() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}
	
	/**
	 * metodo que contiene las acciones de los componentes
	 */
	public void accionesComponentes(){
		txt_Subtema.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				char KeyChar = e.getKeyChar();
				if (Character.isLowerCase(KeyChar)){
					e.setKeyChar(Character.toUpperCase(KeyChar));
				}	
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					if(tablaPrincipal .getRowCount()>=1){
						tablaPrincipal.grabFocus();
						tablaPrincipal.getSelectionModel().setSelectionInterval(0, 0);
					}
					else{
						txt_Subtema.setText("");
						deshacerFiltroTablaPrincipal();
						JOptionPane.showOptionDialog(frame, "Error, el tipo subTema no existe. Int�ntelo de nuevo", "Error",JOptionPane.DEFAULT_OPTION,JOptionPane.ERROR_MESSAGE,null,null,"aceptar");
						txt_Subtema.grabFocus();
					}
				}else{
					filtros.filtroTresColumnas(txt_Subtema.getText().trim().toUpperCase(), trsfiltrotablaPrincipal, 1, 2,3, dtmTablaPrincipal, tablaPrincipal);
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		txt_nombreSubTema.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				char KeyChar = e.getKeyChar();
				if (Character.isLowerCase(KeyChar)){
					e.setKeyChar(Character.toUpperCase(KeyChar));
				}	
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		
		combo_Asignatura.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				llenarComboTema(combo_Asignatura);
			}
		});
	}

	/**
	 * metodo que deshace el filtro de la tabla documento fuente
	 */
	public void deshacerFiltroTablaPrincipal(){
		filtros.deshacerFiltro(trsfiltrotablaPrincipal, dtmTablaPrincipal, tablaPrincipal);
	}
	/**
	 * mketodo que oculta los compenentes de buscar un tema
	 */
	public void ocultarBuscar(){
		txt_Subtema.setVisible(false);
		lbl_nombreB.setVisible(false);
		scrollpanePrincipal.setVisible(false);
		btn_regresar.setVisible(false);
		btn_eliminar.setVisible(false);
		btn_modificar.setVisible(false);
	}
	
	/**
	 * metodo que oculta los componentes de modificar tema
	 */
	public void ocultarModificar(){
		btn_cancelar.setVisible(false);
		btn_guardar.setVisible(false);
		txt_nombreSubTema.setVisible(false);
		combo_Asignatura.setVisible(false);
		combo_Asignatura.setEnabled(true);
		lbl_nombreM.setVisible(false);
		lbl_asignatura.setVisible(false);
		lbl_tema.setVisible(false);
		combo_Tema.setVisible(false);
		combo_Tema.setEnabled(true);
	}
	
	/**
	 * metodo que muestra los compenentes de buscar un tema
	 */
	public void MostrarBuscar(){
		txt_Subtema.setVisible(true);
		lbl_nombreB.setVisible(true);
		scrollpanePrincipal.setVisible(true);
		btn_regresar.setVisible(true);
		btn_eliminar.setVisible(true);
		btn_modificar.setVisible(true);
	}
	
	/**
	 * metodo que muestra los componentes de modificar tema
	 */
	public void MostrarModificar(){
		btn_cancelar.setVisible(true);
		btn_guardar.setVisible(true);
		lbl_nombreM.setVisible(true);
		lbl_asignatura.setVisible(true);
		txt_nombreSubTema.setVisible(true);
		combo_Asignatura.setVisible(true);
		lbl_tema.setVisible(true);
		combo_Tema.setVisible(true);
	}
	
		public void llenarComboTema(JComboBox<String> combo_materias){
			// TODO Poblar combo con temas
			combo_Tema.removeAllItems();
			int materiaSeleccionada = combo_materias.getSelectedIndex();
			if (materiaSeleccionada != -1) {
				ArrayList<Tema> temas = controller.obtenerTemas(combo_materias.getItemAt(materiaSeleccionada));
				for (int i = 0; i < temas.size(); i++) {
					combo_Tema.addItem(temas.get(i).getNombre());
				}
				combo_Tema.setSelectedIndex(-1);
		}
	}
}
