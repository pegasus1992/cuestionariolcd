package view;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import model.*;
import controller.*;
import view.utils.*;

public class ConsultarPreguntas extends General {
	private static final long serialVersionUID = -3768397593307103816L;

	// Panel tabla preguntas
	private JPanel panel_preguntas;
	private DefaultTableModel dtmTablaDatosPreguntas;
	private JTable tablaDatosPreguntas;
	private JScrollPane scrollPanelTablaDatosPreguntas;
	private String datosTablaPreguntas[];

	// Panel Botones
	private JPanel panel_botones;
	private JButton btn_consultar, btn_regresar;

	// Utilitarios
	private static final String titulo = "Consultar Preguntas";
	private Docente docente;
	private ConsultarPreguntaController controller;

	// Indices de la tabla.
	private static final int columnaId = 0;
	private static final int columnaGrado = 1;
	private static final int columnaArea = 2;
	private static final int columnaAsignatura = 3;
	private static final int columnaTema = 4;
	private static final int columnaSubtema = 5;

	/**
	 * Inicializa la ventana de consultar preguntas.
	 * @param anterior Ventana anterior.
	 * @param docente
	 */
	public ConsultarPreguntas(JFrame anterior, Docente docente) {
		super(anterior, 2 * anchoPantalla / 3, 2 * altoPantalla / 3);
		this.docente = docente;
		controller = new ConsultarPreguntaController();

		prepareElementosPanelTabla();
		cargarDatosTabla();
		definaAccionesTabla();
		prepareElementosPanelBotones();
		definaAccionesBotones();
	}

	/**
	 * Prepara los elementos de la tabla.
	 */
	private void prepareElementosPanelTabla() {
		panel_preguntas = new JPanel();
		panel_preguntas.setLocation(0, panel_barraTitulos.getHeight() + 5);
		panel_preguntas.setSize(this.getWidth(), this.getHeight()
				- panel_barraTitulos.getHeight() * 3);
		panel_preguntas.setLayout(null);
		panel_preguntas.setOpaque(false);
		this.add(panel_preguntas);

		dtmTablaDatosPreguntas = new TablaNoEditable();
		tablaDatosPreguntas = new JTable(dtmTablaDatosPreguntas);

		dtmTablaDatosPreguntas.addColumn("ID");
		dtmTablaDatosPreguntas.addColumn("Grado");
		dtmTablaDatosPreguntas.addColumn("�rea");
		dtmTablaDatosPreguntas.addColumn("Asignatura");
		dtmTablaDatosPreguntas.addColumn("Tema");
		dtmTablaDatosPreguntas.addColumn("Subtema");

		tablaDatosPreguntas.setPreferredScrollableViewportSize(new Dimension(
				panel_preguntas.getWidth(), panel_preguntas.getHeight()));
		scrollPanelTablaDatosPreguntas = new JScrollPane(tablaDatosPreguntas);
		tablaDatosPreguntas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		panel_preguntas.add(scrollPanelTablaDatosPreguntas);
		scrollPanelTablaDatosPreguntas.setSize(panel_preguntas.getWidth(),
				panel_preguntas.getHeight());
		scrollPanelTablaDatosPreguntas.setLocation(0, 0);

		tablaDatosPreguntas.setDefaultRenderer(Object.class, new MiRender());
		tablaDatosPreguntas.setShowHorizontalLines(false);
		tablaDatosPreguntas.setBorder(null);
		tablaDatosPreguntas.setOpaque(false);
		scrollPanelTablaDatosPreguntas.setOpaque(false);
		scrollPanelTablaDatosPreguntas.getViewport().setOpaque(false);
		scrollPanelTablaDatosPreguntas.setBorder(null);

		tablaDatosPreguntas
				.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaDatosPreguntas.getTableHeader().setReorderingAllowed(false);

		// Auto-ordenar columnas.
		tablaDatosPreguntas.setAutoCreateRowSorter(true);

		int columna = (anchoPantalla / 2 - 20) / 2;
		tablaDatosPreguntas.getColumnModel().getColumn(columnaId)
				.setPreferredWidth(columna / 4);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaGrado)
				.setPreferredWidth(columna / 2);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaArea)
				.setPreferredWidth(columna);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaAsignatura)
				.setPreferredWidth(columna);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaTema)
				.setPreferredWidth(columna);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaSubtema)
				.setPreferredWidth(columna);
	}

	/**
	 * Carga los datos a la tabla.
	 */
	private void cargarDatosTabla() {
		dtmTablaDatosPreguntas.setRowCount(0);
		ArrayList<Pregunta> preguntas = controller.obtenerPreguntas();

		for (Pregunta pregunta : preguntas) {
			datosTablaPreguntas = new String[dtmTablaDatosPreguntas
					.getColumnCount()];

			try {
				Grado grado = pregunta.getGrado();
				Area area = pregunta.getArea();
				Materia materia = pregunta.getMateria();
				Tema subtema = pregunta.getSubtema();
				Tema tema = subtema.getPadre();

				datosTablaPreguntas[columnaId] = pregunta.getId();
				datosTablaPreguntas[columnaGrado] = grado.getNombre();
				datosTablaPreguntas[columnaArea] = area.getNombre();
				datosTablaPreguntas[columnaAsignatura] = materia.getNombre();
				datosTablaPreguntas[columnaTema] = tema.getNombre();
				datosTablaPreguntas[columnaSubtema] = subtema.getNombre();

				dtmTablaDatosPreguntas.addRow(datosTablaPreguntas);
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * Prepara los elementos del panel de botones.
	 */
	private void prepareElementosPanelBotones() {
		int anchoBtn = 150;
		int altoBtn = 30;

		panel_botones = new JPanel();
		panel_botones.setLocation(panel_preguntas.getWidth() / 4,
				panel_preguntas.getHeight() + 30);
		panel_botones.setSize(panel_preguntas.getWidth() / 2, altoBtn);
		panel_botones.setLayout(new GridLayout(1, 0));
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_consultar = new Boton(0, 0, anchoBtn, altoBtn, "Ver Pregunta",
				panel_botones, this) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonConsultarPregunta();
			}
		};
		btn_consultar.setEnabled(false);

		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar",
				panel_botones, this) {
			private static final long serialVersionUID = -4456446890553142037L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
	}

	/**
	 * Define las acciones dentro de la tabla.
	 */
	private void definaAccionesTabla() {

		tablaDatosPreguntas.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				habilitarBotonConsultar();
			}
		});
		tablaDatosPreguntas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_SPACE: {
					habilitarBotonConsultar();
				}
					break;
				}
			}
		});
	}

	/**
	 * Habilita el boton de consultar <b>siempre y cuando</b> haya una fila
	 * seleccionada en la tabla.
	 */
	private void habilitarBotonConsultar() {
		int fila = tablaDatosPreguntas.getSelectedRow();
		if (fila != -1) {
			btn_consultar.setEnabled(true);
			btn_consultar.grabFocus();
		}
	}

	/**
	 * Define las acciones dentro del panel de botones.
	 */
	private void definaAccionesBotones() {
		btn_consultar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_RIGHT: {
					btn_regresar.grabFocus();
				}
					break;
				case KeyEvent.VK_UP: {
					if (tablaDatosPreguntas.getRowCount() >= 1) {
						tablaDatosPreguntas.grabFocus();
						btn_consultar.setEnabled(false);
					}
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_consultar.doClick();
				}
					break;
				}
			}
		});

		btn_regresar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					btn_consultar.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_regresar.doClick();
				}
					break;
				}
			}
		});
	}

	/**
	 * Si hay una pregunta seleccionada de la tabla, muestra su detalle.
	 */
	private void accionBotonConsultarPregunta() {
		// TODO Boton Consultar Pregunta
		int fila = tablaDatosPreguntas.getSelectedRow();
		if (fila != -1) {
			String idPregunta = (String) tablaDatosPreguntas
					.getValueAt(fila, 0);
			nuevaVentana(new ConsultarPregunta(this, docente, idPregunta));
		}
	}

	/**
	 * Regresar a la ventana anterior.
	 */
	private void accionBotonRegresar() {// TODO Boton Regresar
		cerrarVentana();
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
		cargarDatosTabla();
		if (tablaDatosPreguntas.getRowCount() >= 1) {
			tablaDatosPreguntas.grabFocus();
			btn_consultar.setEnabled(false);
		}
	}
}
