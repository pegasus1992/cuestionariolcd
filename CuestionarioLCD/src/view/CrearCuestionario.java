package view;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import model.*;
import controller.*;
import exception.*;
import view.jasper.*;
import view.utils.*;

public class CrearCuestionario extends General {
	private static final long serialVersionUID = 4004027546208771362L;

	private static final String usoConocimiento = "Uso del Conocimiento";
	private static final String explicativa = "Explicativa";
	private static final String indagacion = "Indagaci�n";
	private static final String conceptual = "Conceptual";
	
	// Panel Informacion
	private JPanel panel_informacion;
	private JLabel lbl_grado, lbl_area, lbl_asignatura, lbl_tema, lbl_subtema, lbl_competencia, lbl_nivel;
	private JComboBox<String> combo_grados, combo_areas, combo_asignaturas ,combo_competencia, combo_nivel,	combo_temas, combo_subtemas;

	// Panel Tabla de Preguntas
	private JPanel panel_preguntas;
	private DefaultTableModel dtmTablaDatosPreguntas;
	private JTable tablaDatosPreguntas;
	private JScrollPane scrollPanelTablaDatosPreguntas;
	private String datosTablaPreguntas[];

	// Panel Botones
	private JPanel panel_botones;
	private JButton btn_crear, btn_regresar, btn_consultar;

	// Utilitarios
	private static final String titulo = "Crear Cuestionario";
	private Docente docente;
	private CrearCuestionarioController controller;
	private boolean esCrear;

	// Indices de la tabla.
	private static final int columnaId = 0;
	private static final int columnaTema = 1;
	private static final int columnaSubtema = 2;
	private static final int columnaCompetencia = 3;
	private static final int columnaNivel = 4;
	private static final int columnaEnunciadoGeneral = 5;
	private static final int columnaEnunciadoEspecifico = 6;
	private static final int columnaRespuestaCorrecta = 7;
	private static final int columnaImagen = 8;
	private static final int columnaRespuestaA = 9;
	private static final int columnaRespuestaB = 10;
	private static final int columnaRespuestaC = 11;
	private static final int columnaRespuestaD = 12;
	private static final int columnaRespuestaE = 13;
	
	int idCuestionario;

	/**
	 * Inicializa la ventana de crear cuestionario.
	 * 
	 * @param anterior
	 *            Ventana anterior.
	 * @param docente
	 * @param esCrear
	 *            es <b>true</b> si en esta ventana se creara un cuestionario
	 *            con las preguntas seleccionadas.<br>
	 *            De lo contrario, muestra el detalle de la pregunta
	 *            seleccionada.
	 */
	public CrearCuestionario(JFrame anterior, Docente docente, boolean esCrear) {
		super(anterior, 2 * anchoPantalla / 3, 2 * altoPantalla / 9*4);
		this.docente = docente;
		controller = new CrearCuestionarioController();
		this.esCrear = esCrear;

		prepareElementosPanelInformacion();
		definaAccionesInformacion();
		prepareElementosTabla();
		prepareElementosPanelBotones();
		definaAccionesBotones();


	}
	
	/**
	 * metodo que inserta un cuentionario nuevo
	 * @return
	 */
	public boolean insertarCuestionarioYCuestionarioPregunta(String[] idPreguntas){
		idCuestionario = controller.obtenerIdMaximoCuestionario();
		return controller.agregarCuestionario(idCuestionario, idPreguntas);
	}

	/**
	 * Prepara los elementos del panel de informacion.
	 */
	private void prepareElementosPanelInformacion() {
		int anchoP = anchoPantalla / 10;
		int x = anchoP / 7, alto = anchoP / 5;
		int aumentoX = anchoP / 5, aumentoY = anchoP / 3, reduccion = anchoP / 10;
		int anchoLbl = 3 * anchoP / 4, anchoTxt = 2 * anchoP;
		int comboGrande = anchoTxt, comboPeque�o = anchoTxt / 2;

		panel_informacion = new JPanel();
		panel_informacion.setLocation(0, panel_barraTitulos.getHeight() + 5);
		panel_informacion.setSize(this.getWidth(), this.getHeight()	- panel_barraTitulos.getHeight() * 3);
		panel_informacion.setLayout(null);
		panel_informacion.setOpaque(false);
		this.add(panel_informacion);

		lbl_area = new JLabel("�rea: ", JLabel.LEFT);
		lbl_area.setLocation(x, panel_informacion.getY() - reduccion);
		lbl_area.setSize(anchoLbl, alto);
		lbl_area.setForeground(letra.getColorLabel());
		lbl_area.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_area);

		combo_areas = new JComboBox<String>();
		combo_areas.setLocation(lbl_area.getWidth() + aumentoX, lbl_area.getY());		combo_areas.setSize(comboGrande, alto);
		combo_areas.setForeground(letra.getColorCombo());
		combo_areas.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_areas);

		// TODO Poblar combo con areas
		if (esCrear) {
			ArrayList<Area> areas = controller.obtenerAreas();
			for (int i = 0; i < areas.size(); i++) {
				combo_areas.addItem(areas.get(i).getNombre());
			}
		} else {
			ArrayList<Area> areas = controller.obtenerAreasDocente(docente);
			for (int i = 0; i < areas.size(); i++) {
				combo_areas.addItem(areas.get(i).getNombre());
			}
		}
		combo_areas.setSelectedIndex(-1);

		lbl_asignatura = new JLabel("Asignatura: ", JLabel.LEFT);
		lbl_asignatura.setLocation(lbl_area.getX(), lbl_area.getY() + aumentoY);
		lbl_asignatura.setSize(lbl_area.getWidth(), lbl_area.getHeight());
		lbl_asignatura.setForeground(letra.getColorLabel());
		lbl_asignatura.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_asignatura);

		combo_asignaturas = new JComboBox<String>();
		combo_asignaturas
				.setLocation(combo_areas.getX(), lbl_asignatura.getY());
		combo_asignaturas.setSize(comboGrande, combo_areas.getHeight());
		combo_asignaturas.setForeground(letra.getColorCombo());
		combo_asignaturas.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_asignaturas);

		lbl_grado = new JLabel("Grado: ", JLabel.LEFT);
		lbl_grado.setLocation(lbl_asignatura.getX(), lbl_asignatura.getY()+ aumentoY);
		lbl_grado.setSize(lbl_asignatura.getWidth(), lbl_asignatura.getHeight());
		lbl_grado.setForeground(letra.getColorLabel());
		lbl_grado.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_grado);

		combo_grados = new JComboBox<String>();
		combo_grados.setLocation(combo_asignaturas.getX(), lbl_grado.getY());
		combo_grados.setSize(comboPeque�o, combo_asignaturas.getHeight());
		combo_grados.setForeground(letra.getColorCombo());
		combo_grados.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_grados);

		// TODO Poblar combo con grados
		ArrayList<Grado> grados = controller.obtenerGrados();
		for (int i = 0; i < grados.size(); i++) {
			combo_grados.addItem(grados.get(i).getNombre());
		}
		combo_grados.setSelectedIndex(-1);

		lbl_competencia = new JLabel("Competencia: ", JLabel.LEFT);
		lbl_competencia.setLocation(lbl_grado.getX(), lbl_grado.getY()+ aumentoY);
		lbl_competencia.setSize(lbl_grado.getWidth(), lbl_grado.getHeight());
		lbl_competencia.setForeground(letra.getColorLabel());
		lbl_competencia.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_competencia);
		
		combo_competencia = new JComboBox<String>();
		combo_competencia.setLocation(combo_grados.getX(), lbl_competencia.getY());
		combo_competencia.setSize(comboPeque�o, combo_asignaturas.getHeight());
		combo_competencia.setForeground(letra.getColorCombo());
		combo_competencia.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_competencia);
		
		lbl_tema = new JLabel("Tema: ", JLabel.LEFT);
		lbl_tema.setLocation(combo_areas.getX() + combo_areas.getWidth() + 5* aumentoX, combo_areas.getY());
		lbl_tema.setSize(lbl_area.getWidth(), lbl_area.getHeight());
		lbl_tema.setForeground(letra.getColorLabel());
		lbl_tema.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_tema);

		combo_temas = new JComboBox<String>();
		combo_temas.setLocation(lbl_tema.getX() + lbl_tema.getWidth()+ aumentoX, lbl_tema.getY());
		combo_temas.setSize(comboPeque�o, combo_asignaturas.getHeight());
		combo_temas.setForeground(letra.getColorCombo());
		combo_temas.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_temas);

		lbl_subtema = new JLabel("Subtema: ", JLabel.LEFT);
		lbl_subtema.setLocation(lbl_tema.getX(), lbl_tema.getY() + aumentoY);
		lbl_subtema.setSize(lbl_tema.getWidth(), lbl_tema.getHeight());
		lbl_subtema.setForeground(letra.getColorLabel());
		lbl_subtema.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_subtema);

		combo_subtemas = new JComboBox<String>();
		combo_subtemas.setLocation(combo_temas.getX(), lbl_subtema.getY());
		combo_subtemas.setSize(combo_temas.getWidth(), combo_temas.getHeight());
		combo_subtemas.setForeground(letra.getColorCombo());
		combo_subtemas.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_subtemas);
		
		lbl_nivel = new JLabel("Nivel: ", JLabel.LEFT);
		lbl_nivel.setLocation(lbl_tema.getX(), lbl_subtema.getY() + aumentoY);
		lbl_nivel.setSize(lbl_tema.getWidth(), lbl_tema.getHeight());
		lbl_nivel.setForeground(letra.getColorLabel());
		lbl_nivel.setFont(letra.getFuenteLabel());
		panel_informacion.add(lbl_nivel);

		combo_nivel = new JComboBox<String>();
		combo_nivel.setLocation(combo_temas.getX(), lbl_nivel.getY());
		combo_nivel.setSize(combo_subtemas.getWidth(), combo_subtemas.getHeight());
		combo_nivel.setForeground(letra.getColorCombo());
		combo_nivel.setFont(letra.getFuenteCombo());
		panel_informacion.add(combo_nivel);
		
		// Adicionando Campos
		combo_competencia.addItem(usoConocimiento);
		combo_competencia.addItem(explicativa);
		combo_competencia.addItem(indagacion);
		combo_competencia.addItem(conceptual);
		combo_competencia.setSelectedIndex(-1);

		combo_nivel.addItem("Alto");
		combo_nivel.addItem("Medio");
		combo_nivel.addItem("Bajo");
		combo_nivel.setSelectedIndex(-1);
		
		int anchoBtn = 150;
		int altoBtn = 30;
		
		btn_consultar = new Boton(0, 0, anchoBtn, altoBtn,"Consultar", panel_informacion, this) {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonConsultar();
			}
		};
		btn_consultar.setLocation(combo_temas.getX(), lbl_nivel.getY() + aumentoY);
	}

	/**
	 * Prepara los elementos de la tabla.
	 */
	private void prepareElementosTabla() {

		panel_preguntas = new JPanel();
		panel_preguntas.setLocation(0, lbl_grado.getY() + lbl_grado.getHeight()*2+ 25);
		panel_preguntas.setSize(panel_informacion.getWidth(),panel_informacion.getHeight() - lbl_grado.getY()*2);
		panel_preguntas.setLayout(null);
		panel_preguntas.setOpaque(false);
		panel_informacion.add(panel_preguntas);
		// panel_preguntas.setVisible(false);

		dtmTablaDatosPreguntas = new TablaNoEditable();
		tablaDatosPreguntas = new JTable(dtmTablaDatosPreguntas);

		dtmTablaDatosPreguntas.addColumn("ID");
		dtmTablaDatosPreguntas.addColumn("Tema");
		dtmTablaDatosPreguntas.addColumn("Subtema");
		dtmTablaDatosPreguntas.addColumn("Competencia");
		dtmTablaDatosPreguntas.addColumn("Nivel");
		dtmTablaDatosPreguntas.addColumn("Enunciado General");
		dtmTablaDatosPreguntas.addColumn("Enunciado Espec�fico");
		dtmTablaDatosPreguntas.addColumn("Respuesta Correcta");
		dtmTablaDatosPreguntas.addColumn("Imagen");
		dtmTablaDatosPreguntas.addColumn("Respuesta A");
		dtmTablaDatosPreguntas.addColumn("Respuesta B");
		dtmTablaDatosPreguntas.addColumn("Respuesta C");
		dtmTablaDatosPreguntas.addColumn("Respuesta D");
		dtmTablaDatosPreguntas.addColumn("Respuesta E");

		tablaDatosPreguntas.setPreferredScrollableViewportSize(new Dimension(panel_preguntas.getWidth(), panel_preguntas.getHeight()));
		scrollPanelTablaDatosPreguntas = new JScrollPane(tablaDatosPreguntas);
		tablaDatosPreguntas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		panel_preguntas.add(scrollPanelTablaDatosPreguntas);
		scrollPanelTablaDatosPreguntas.setSize(panel_preguntas.getWidth(),panel_preguntas.getHeight());
		scrollPanelTablaDatosPreguntas.setLocation(0, 0);

		tablaDatosPreguntas.setDefaultRenderer(Object.class, new MiRender());
		tablaDatosPreguntas.setShowHorizontalLines(false);
		tablaDatosPreguntas.setBorder(null);
		tablaDatosPreguntas.setOpaque(false);
		scrollPanelTablaDatosPreguntas.setOpaque(false);
		scrollPanelTablaDatosPreguntas.getViewport().setOpaque(false);
		scrollPanelTablaDatosPreguntas.setBorder(null);

		if (esCrear) {
			// Se crea el cuestionario con las preguntas seleccionadas.
			tablaDatosPreguntas.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			// Muestra el detalle de la pregunta seleccionada.
			tablaDatosPreguntas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		tablaDatosPreguntas.getTableHeader().setReorderingAllowed(false);

		// Auto-ordenar columnas.
		tablaDatosPreguntas.setAutoCreateRowSorter(true);

		int columna = (anchoPantalla / 2 - 20) / 4;
		tablaDatosPreguntas.getColumnModel().getColumn(columnaId).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaId).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaId).setPreferredWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaTema).setPreferredWidth(columna);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaSubtema).setPreferredWidth(columna);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaCompetencia).setPreferredWidth(columna / 2);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaNivel).setPreferredWidth(columna / 3);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaEnunciadoGeneral).setPreferredWidth(columna * 2);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaEnunciadoEspecifico).setPreferredWidth(columna * 2);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaCorrecta).setPreferredWidth(columna);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaImagen).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaImagen).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaImagen).setPreferredWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaA).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaA).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaA).setPreferredWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaB).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaB).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaB).setPreferredWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaC).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaC).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaC).setPreferredWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaD).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaD).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaD).setPreferredWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaE).setMaxWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaE).setMinWidth(0);
		tablaDatosPreguntas.getColumnModel().getColumn(columnaRespuestaE).setPreferredWidth(0);
	}

	/**
	 * Define las acciones dentro del panel de informacion.
	 */
	private void definaAccionesInformacion() {

		combo_areas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_asignaturas.grabFocus();
				}
					break;
				}
			}
		});
		combo_areas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// Reseteo general
				dtmTablaDatosPreguntas.setRowCount(0);
				combo_grados.setSelectedIndex(-1);
				combo_temas.removeAllItems();
				combo_subtemas.removeAllItems();

				combo_asignaturas.removeAllItems();
				// TODO Poblar combo con asignaturas
				int areaSeleccionada = combo_areas.getSelectedIndex();
				if (areaSeleccionada != -1) {
					ArrayList<Materia> materias;
					if (esCrear) {
						materias = controller.obtenerMaterias(combo_areas
								.getItemAt(areaSeleccionada));
					} else {
						materias = controller.obtenerMateriasDocente(
								combo_areas.getItemAt(areaSeleccionada),
								docente);
					}
					for (int i = 0; i < materias.size(); i++) {
						combo_asignaturas.addItem(materias.get(i).getNombre());
					}
					combo_asignaturas.setSelectedIndex(-1);
				}
			}
		});

		combo_asignaturas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_areas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_grados.grabFocus();
				}
					break;
				}
			}
		});
		combo_asignaturas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// Reseteo general
				dtmTablaDatosPreguntas.setRowCount(0);
				combo_grados.setSelectedIndex(-1);
				combo_subtemas.removeAllItems();

				combo_temas.removeAllItems();
				// TODO Poblar combo con temas
				int materiaSeleccionada = combo_asignaturas.getSelectedIndex();
				if (materiaSeleccionada != -1) {
					ArrayList<Tema> temas = controller
							.obtenerTemas(combo_asignaturas
									.getItemAt(materiaSeleccionada));
					for (int i = 0; i < temas.size(); i++) {
						combo_temas.addItem(temas.get(i).getNombre());
					}
					combo_temas.setSelectedIndex(-1);
				}
			}
		});

		combo_temas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// Reseteo general
				dtmTablaDatosPreguntas.setRowCount(0);
				combo_grados.setSelectedIndex(-1);

				combo_subtemas.removeAllItems();
				// TODO Poblar combo con subtemas
				int temaSeleccionado = combo_temas.getSelectedIndex();
				if (temaSeleccionado != -1) {
					ArrayList<Tema> subtemas = controller
							.obtenerSubtemas(combo_temas
									.getItemAt(temaSeleccionado));
					for (int i = 0; i < subtemas.size(); i++) {
						combo_subtemas.addItem(subtemas.get(i).getNombre());
					}
					combo_subtemas.setSelectedIndex(-1);
				}
			}
		});

		combo_subtemas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// Reseteo general
				dtmTablaDatosPreguntas.setRowCount(0);
				combo_grados.setSelectedIndex(-1);
			}
		});

		combo_grados.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_asignaturas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					if (btn_crear != null) {
						btn_crear.grabFocus();
					} else {
						btn_regresar.grabFocus();
					}
				}
					break;
				}
			}
		});
	}

	/**
	 * Prepara el panel de botones.
	 */
	private void prepareElementosPanelBotones() {
		int anchoBtn = 150;
		int altoBtn = 30;

		panel_botones = new JPanel();
		panel_botones.setLocation(panel_informacion.getWidth() / 4,
				panel_informacion.getHeight() + 30);
		panel_botones.setSize(panel_informacion.getWidth() / 2, altoBtn);
		panel_botones.setLayout(new GridLayout(1, 0));
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		if (esCrear) {
			btn_crear = new Boton(0, 0, anchoBtn, altoBtn,"Crear Cuestionario", panel_botones, this) {
				private static final long serialVersionUID = -5785655676854998075L;

				@Override
				public void accionBoton() {
					if (esCrear) {
						int areaSeleccionada = combo_areas.getSelectedIndex();
						int asignaturaSeleccionada = combo_asignaturas.getSelectedIndex();
						int gradoSeleccionado = combo_grados.getSelectedIndex();
						if (areaSeleccionada != -1&& asignaturaSeleccionada != -1 && gradoSeleccionado != -1) {
							accionBotonCrearCuestionario();
						} else {
							if (areaSeleccionada == -1) {
								JOptionPane.showOptionDialog(null, "Debe seleccionar el �rea.", "Falta �rea", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
								combo_areas.grabFocus();
							} else if (asignaturaSeleccionada == -1) {
								JOptionPane.showOptionDialog(null, "Debe seleccionar la asignatura.", "Falta asignatura", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
								combo_asignaturas.grabFocus();
							} else if (gradoSeleccionado == -1) {
								JOptionPane.showOptionDialog(null, "Debe seleccionar el grado.", "Falta grado", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
								combo_grados.grabFocus();
							}
						}
					}
				}
			};
		} else {
			btn_crear = new Boton(0, 0, anchoBtn, altoBtn,"Consultar Pregunta", panel_botones, this) {
				private static final long serialVersionUID = 2921273826496999236L;

				@Override
				public void accionBoton() {
					if (!esCrear) {
						accionBotonConsultarPregunta();
					}
				}
			};
		}

		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar",
				panel_botones, this) {
			private static final long serialVersionUID = -4456446890553142037L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
	}

	/**
	 * Define las acciones dentro del panel de botones.
	 */
	private void definaAccionesBotones() {
		if (btn_crear != null) {
			btn_crear.addKeyListener(new KeyAdapter() {

				@Override
				public void keyPressed(KeyEvent evt) {
					switch (evt.getKeyCode()) {
					case KeyEvent.VK_LEFT: {
						combo_grados.grabFocus();
					}
						break;
					case KeyEvent.VK_RIGHT: {
						btn_regresar.grabFocus();
					}
						break;
					case KeyEvent.VK_ENTER: {
						btn_crear.doClick();
					}
						break;
					}
				}
			});
		}

		btn_regresar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					if (btn_crear != null) {
						btn_crear.grabFocus();
					} else {
						combo_grados.grabFocus();
					}
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_regresar.doClick();
				}
					break;
				}
			}
		});
	}

	/**
	 * Rellenar la tabla.
	 */
	private void cargarTablaPrincipal() {
		// TODO Poblar Tablas

		// Limpiar Tabla
		dtmTablaDatosPreguntas.setRowCount(0);

		int areaSeleccionada = combo_areas.getSelectedIndex();
		String nombreArea = combo_areas.getItemAt(areaSeleccionada);
		System.out.println("Nombre Area:   "+nombreArea);
		Area area = controller.obtenerArea(nombreArea);
		System.out.println("Area: "+area);
		
		int asignaturaSeleccionada = combo_asignaturas.getSelectedIndex();
		String nombreAsignatura = combo_asignaturas.getItemAt(asignaturaSeleccionada);
		System.out.println("Nombre Asignatura:   "+nombreAsignatura);
		Materia asignatura = controller.obtenerMateria(nombreAsignatura);
		System.out.println("Asignatura: "+asignatura);
		
		int gradoSeleccionado = combo_grados.getSelectedIndex();
		String nombreGrado = combo_grados.getItemAt(gradoSeleccionado);
		System.out.println("Nombre Grado:   "+nombreGrado);
		Grado grado = controller.obtenerGrado(nombreGrado);
		System.out.println("grado:  "+grado);
		String competencia = "";
		if (combo_competencia.getSelectedItem() != null) {
			competencia = combo_competencia.getSelectedItem().toString();
		}
		System.out.println("Nombre competencia: "+competencia);
		
		String nombreTema = "";
		if (combo_temas.getSelectedItem() != null) {
			nombreTema = combo_temas.getSelectedItem().toString();
		}
		System.out.println("Nombre tema: "+nombreTema);
		
		String nombreSubtema = "";
		if (combo_subtemas.getSelectedItem() != null) {
			nombreSubtema = combo_subtemas.getSelectedItem().toString();
		}
		System.out.println("Nombre Subtema: "+nombreSubtema);
		
		String nivel = "";
		if (combo_nivel.getSelectedItem() != null) {
			nivel = combo_nivel.getSelectedItem().toString();
		}
		System.out.println("Nombre nivel: "+nivel);
		
		String docente ="";
		if (!permisos()) {
			docente = this.docente.getId();
		}
		
		ArrayList<String[]> preguntas = controller.obtenerPreguntas(area, asignatura, grado,  competencia , nombreTema, nombreSubtema,  nivel, docente);

		for (int i=0; i< preguntas.size();i++) {

			datosTablaPreguntas = new String[dtmTablaDatosPreguntas.getColumnCount()];

			datosTablaPreguntas[columnaId] = preguntas.get(i)[0];
			datosTablaPreguntas[columnaTema] = preguntas.get(i)[30];
			datosTablaPreguntas[columnaSubtema] = preguntas.get(i)[26];
			datosTablaPreguntas[columnaCompetencia] = preguntas.get(i)[1];
			datosTablaPreguntas[columnaNivel] = preguntas.get(i)[2];
			datosTablaPreguntas[columnaEnunciadoGeneral] = preguntas.get(i)[8];
			datosTablaPreguntas[columnaEnunciadoEspecifico] = preguntas.get(i)[9];
			datosTablaPreguntas[columnaImagen] = preguntas.get(i)[31];
			
			
			String opcionCorrecta = "";
			ArrayList<String[]> opciones = controller.obtenerOpciones(preguntas.get(i)[0]);
			for (int h=0; h< opciones.size();h++) {
				if (opciones.get(h)[2].equals("S")) {
					opcionCorrecta = opciones.get(h)[2];
					break;
				}
			}			
			datosTablaPreguntas[columnaRespuestaCorrecta] = opcionCorrecta;
			datosTablaPreguntas[columnaRespuestaA] = opciones.get(0)[1];
			datosTablaPreguntas[columnaRespuestaB] = opciones.get(1)[1];
			datosTablaPreguntas[columnaRespuestaC] = opciones.get(2)[1];
			datosTablaPreguntas[columnaRespuestaD] = opciones.get(3)[1];
			if (opciones.size()>4) {
				datosTablaPreguntas[columnaRespuestaE] = opciones.get(4)[1];
			}else{
				datosTablaPreguntas[columnaRespuestaE] = "";
			}

			dtmTablaDatosPreguntas.addRow(datosTablaPreguntas);
		}
	}

	/**
	 * Validado el caso de SOLO para crear cuestionarios (esCrear = true).
	 */
	private void accionBotonCrearCuestionario() {
		// TODO Boton Consultar Pregunta
		
		int[] seleccionadasA = tablaDatosPreguntas.getSelectedRows();
		String[] idsPreguntas = new String[seleccionadasA.length];
		for (int i = 0; i < seleccionadasA.length; i++) {
			idsPreguntas[i] = dtmTablaDatosPreguntas.getValueAt(seleccionadasA[i], columnaId).toString();
		}
		
        boolean resultado = insertarCuestionarioYCuestionarioPregunta(idsPreguntas);
        if (resultado) {
			ArrayList<Map<String, String>> preguntasSeleccionadas = null;
			if (dtmTablaDatosPreguntas.getRowCount() > 0) {
				preguntasSeleccionadas = new ArrayList<Map<String, String>>();
				int[] seleccionadas = tablaDatosPreguntas.getSelectedRows();
	
				for (int i = 0; i < seleccionadas.length; i++) {
					Map<String, String> temp = new HashMap<String, String>();
	
					int selected = seleccionadas[i];
					String tema = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaTema);
					String subtema = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaSubtema);
					String competencia = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaCompetencia);
					String nivel = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaNivel);
					String enunciadoGeneral = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaEnunciadoGeneral);
					String enunciadoEspecifico = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaEnunciadoEspecifico);
					String rutaImagen = (String) dtmTablaDatosPreguntas.getValueAt(	selected, columnaImagen);
					String opcionA = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaRespuestaA);
					String opcionB = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaRespuestaB);
					String opcionC = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaRespuestaC);
					String opcionD = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaRespuestaD);
					String opcionE = (String) dtmTablaDatosPreguntas.getValueAt(selected, columnaRespuestaE);
	
					temp.put("numeroPregunta", (i + 1) + ".");
					temp.put("tema", tema);
					temp.put("subtema", subtema);
					temp.put("competencia", competencia);
					temp.put("nivel", nivel);
					temp.put("enunciadoGeneral", enunciadoGeneral);
					temp.put("A", "A.");
					temp.put("opcionA", opcionA);
					temp.put("B", "B.");
					temp.put("opcionB", opcionB);
					temp.put("C", "C.");
					temp.put("opcionC", opcionC);
					temp.put("D", "D.");
					temp.put("opcionD", opcionD);
	
					if (rutaImagen != null && !rutaImagen.trim().isEmpty()) {
						temp.put("rutaImagen", rutaImagen);
					}
	
					if (enunciadoEspecifico != null
							&& !enunciadoEspecifico.trim().isEmpty()) {
						temp.put("enunciadoEspecifico", enunciadoEspecifico);
					}
	
					if (opcionE != null && !opcionE.trim().isEmpty()) {
						temp.put("E", "E.");
						temp.put("opcionE", opcionE);
					}
	
					preguntasSeleccionadas.add(temp);
				}
			}
	
			if (preguntasSeleccionadas != null) {
				int areaSeleccionada = combo_areas.getSelectedIndex();
				String nombreArea = combo_areas.getItemAt(areaSeleccionada);
	
				int asignaturaSeleccionada = combo_asignaturas.getSelectedIndex();
				String nombreAsignatura = combo_asignaturas.getItemAt(asignaturaSeleccionada);
	
				int gradoSeleccionado = combo_grados.getSelectedIndex();
				String nombreGrado = combo_grados.getItemAt(gradoSeleccionado);
	
				try {
					JasperReportsBuilder report = new JasperReportsBuilder(docente);
					report.verCuestionarioFiller(nombreArea, nombreAsignatura,nombreGrado, preguntasSeleccionadas);
				} catch (JasperException ex) {
					JOptionPane.showOptionDialog(null, ex.getMessage(),	"Error ocurrido", JOptionPane.DEFAULT_OPTION,JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				}
			}
			
			Map<String, Object> arreglo = new HashMap<String, Object>();
			String idTransaccion = idCuestionario+"";
			System.out.println("ESTE ES EL ID QUE ME ENVIA"+idTransaccion);
			arreglo.put("idCuestionario", idTransaccion);
			AbstractJasperReports.createReportMec(arreglo);
			AbstractJasperReports.showViewerMEC();
			
			
			
        }else{
			JOptionPane.showOptionDialog(null, "�Error! Intentelo de nuevo",	"Error ocurrido", JOptionPane.DEFAULT_OPTION,JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
        }
	}

	/**
	 * Validado el caso de SOLO consultar preguntas (esCrear = false).
	 */
	private void accionBotonConsultarPregunta() {
		// TODO Boton Consultar Pregunta
		int fila = tablaDatosPreguntas.getSelectedRow();
		if (fila != -1) {
			String idPregunta = (String) tablaDatosPreguntas
					.getValueAt(fila, 0);
			nuevaVentana(new ConsultarPregunta(this, docente, idPregunta));
		}
	}

	/**
	 * metodo que contiene la accion del boton consultar
	 */
	private void accionBotonConsultar(){
//		String valida = validarConsultar();
//		if (valida.equals("SI")) {
			cargarTablaPrincipal();
//		}else{
//			JOptionPane.showOptionDialog(null, "Debe Seleccionar como minimo un grado",	"Advertencia", JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE, null, null, "aceptar");
//		}
	}
	
	/**
	 * regresar a la ventana anterior.
	 */
	private void accionBotonRegresar() {// TODO Boton Regresar
		cerrarVentana();
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
	}
	
	/**
	 * Permisos (por ahora quemados en codigo <b>:(</b> ).
	 * 
	 * @return
	 */
	private boolean permisos() {
		return docente.getId().equalsIgnoreCase("Avuuna")
				|| docente.getId().equalsIgnoreCase("Root");
	}

	
}
