package view;

import view.utils.*;
import exception.*;

/**
 * Inicia la aplicacion.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Main {

	public static void main(String[] args) {
		try {
			Utils.setLookAndFeel("Nimbus");
			new Ingresar();
		} catch (LookAndFeelException ex) {
			Utils.display(ex.getClass().getName() + ": " + ex.getMessage());
			Utils.log(Main.class.getName(), ex);
			ex.printStackTrace();
		}
	}
}
