package view;

import java.awt.*;

import javax.swing.*;

import model.*;
import view.jasper.*;
import view.utils.*;

public class Menu extends General {

	/**
	 * Permisos (por ahora quemados en codigo <b>:(</b> ).
	 * 
	 * @return
	 */
	private boolean permisos() {
		return docente.getId().equalsIgnoreCase("Avuuna")
				|| docente.getId().equalsIgnoreCase("Root");
	}

	private static final long serialVersionUID = -8327866065465185193L;

	// Panel Preguntas
	private JPanel panel_preguntas;

	// Panel Temas y Cuestionarios
	private JPanel panel_temas;

	// Panel Control Usuario
	private JPanel panel_control;

	// Panel Logo
	private JPanel panel_logo;
	private JLabel lbl_logo;

	// Utilitarios
	private static final String titulo = "Men� Principal";
	private Docente docente;

	/**
	 * Inicializa el menu principal.
	 * 
	 * @param anterior
	 *            Ventana anterior.
	 * @param docente
	 */
	public Menu(JFrame anterior, Docente docente) {
		super(anterior, 2 * anchoPantalla / 5, altoPantalla * 11 / 25);
		this.docente = docente;

		prepareElementos();
		prepareInformacionUsuario();
		{
			int anchoBtn = 150;
			int altoBtn = 30;

			prepareElementosPanelLogo();
			prepareElementosPanelPreguntas(anchoBtn, altoBtn);
			prepareElementosPanelTemas(anchoBtn, altoBtn);
			prepareElementosPanelControlUsuario(anchoBtn, altoBtn);
		}
	}

	/**
	 * Prepara el panel desplegable con la informacion del usuario.
	 */
	private void prepareInformacionUsuario() {
		int anchoP = anchoPantalla / 10;
		int x = this.getWidth() - 100;
		int y = panel_barraTitulos.getY() + panel_barraTitulos.getHeight() + 10;
		int y2 = y + anchoP / 10;
		int width = this.getWidth() / 2;
		int height = this.getHeight() - y2 - anchoP / 3;
		new InformacionUsuario(x, y, width, height, docente, this);
	}

	/**
	 * Prepara el panel donde va el logo del colegio.
	 */
	private void prepareElementosPanelLogo() {
		lbl_logo = new JLabel();
		lbl_logo.setIcon(Imagen.convert2Image(logoColegio,
				panel_logo.getWidth(), panel_logo.getHeight() - 10));// TODO
		panel_logo.add(lbl_logo);
	}

	/**
	 * Prepara el panel de preguntas.
	 * 
	 * @param anchoBtn
	 * @param altoBtn
	 */
	private void prepareElementosPanelPreguntas(int anchoBtn, int altoBtn) {

		new Boton(0, 0, anchoBtn, altoBtn, "Crear Pregunta", panel_preguntas,
				this) {
			private static final long serialVersionUID = 2243168745572439656L;

			@Override
			public void accionBoton() {
				accionBotonCrearPregunta();
			}
		};

		addEmptyLabel(panel_preguntas);

		new Boton(0, 0, anchoBtn, altoBtn, "Consultar Preguntas",
				panel_preguntas, this) {
			private static final long serialVersionUID = 2243168745572439656L;

			@Override
			public void accionBoton() {
				accionBotonConsultarPreguntas();
			}
		};
	}

	/**
	 * Prepara el panel de temas y subtemas.
	 * 
	 * @param anchoBtn
	 * @param altoBtn
	 */
	private void prepareElementosPanelTemas(int anchoBtn, int altoBtn) {

		if (permisos()) {
			new Boton(0, 0, anchoBtn, altoBtn, "Crear Tema", panel_temas, this) {
				private static final long serialVersionUID = 7856914380008168217L;

				@Override
				public void accionBoton() {
					accionBotonCrearTema();
				}
			};
			new Boton(0, 0, anchoBtn, altoBtn, "Consultar Tema", panel_temas,
					this) {
				private static final long serialVersionUID = 7856914380008168217L;

				@Override
				public void accionBoton() {
					accionBotonConsultarTema();
				}
			};
			new Boton(0, 0, anchoBtn, altoBtn, "Crear Subtema", panel_temas,
					this) {
				private static final long serialVersionUID = 7856914380008168217L;

				@Override
				public void accionBoton() {
					accionBotonCrearSubtema();
				}
			};
			new Boton(0, 0, anchoBtn, altoBtn, "Consultar Subtema",
					panel_temas, this) {
				private static final long serialVersionUID = 7856914380008168217L;

				@Override
				public void accionBoton() {
					accionBotonConsultarSubtema();
				}
			};

			new Boton(0, 0, anchoBtn, altoBtn, "Crear Cuestionario",
					panel_temas, this) {
				private static final long serialVersionUID = -564039830422134827L;

				@Override
				public void accionBoton() {
					accionBotonCrearCuestionario();
				}
			};
		} else {
			panel_temas.add(panel_logo);
		}
	}

	/**
	 * Prepara el panel de control del usuario.
	 * 
	 * @param anchoBtn
	 * @param altoBtn
	 */
	private void prepareElementosPanelControlUsuario(int anchoBtn, int altoBtn) {

		new Boton(0, 0, anchoBtn, altoBtn, "Cambiar Contrase�a", panel_control,
				this) {
			private static final long serialVersionUID = -564039830422134827L;

			@Override
			public void accionBoton() {
				accionBotonCambiarContrasenia();
			}
		};

		if (permisos()) {
			new Boton(0, 0, anchoBtn, altoBtn, "Formato Cuestionario",
					panel_control, this) {
				private static final long serialVersionUID = 4495597894310350003L;

				@Override
				public void accionBoton() {
					showDesign(AbstractJasperReports.verCuestionario);
				}
			};
		} else {
			addEmptyLabel(panel_control);
		}

		new Boton(0, 0, anchoBtn, altoBtn, "Cerrar Sesi�n", panel_control, this) {
			private static final long serialVersionUID = 2243168745572439656L;

			@Override
			public void accionBoton() {
				accionBotonCerrar();
			}
		};
	}

	/**
	 * Muestra el disenio del cuestionario.
	 * 
	 * @param fileName
	 */
	private void showDesign(String fileName) {
		String filePath = AbstractJasperReports.addExtensionJrxml(fileName);
		AbstractJasperReports.openEditor(filePath);
	}

	/**
	 * Prepara los elementos del menu.
	 */
	private void prepareElementos() {
		int anchoP = anchoPantalla / 10;
		int barraTitulosY = panel_barraTitulos.getY()
				+ panel_barraTitulos.getHeight();

		/******************* PANEL LOGO *******************/

		panel_logo = new JPanel();
		panel_logo.setLocation(this.getWidth() / 3 + 20, barraTitulosY);
		panel_logo.setSize(anchoP * 2 / 3, anchoP * 2 / 3);
		// panel_logo.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel_logo.setOpaque(false);
		if (permisos()) {
			this.add(panel_logo);
		} else {
			Font font1 = new Font("Verdana", Font.BOLD, 15);
			Font font2 = new Font("Verdana", Font.BOLD, 12);

			JLabel labelTitulo = new JLabel(General.titulo.replace(" -", ""),
					JLabel.CENTER);
			labelTitulo.setLocation(panel_logo.getX() / 2,
					panel_logo.getY() * 3 / 2);
			labelTitulo.setSize(panel_logo.getWidth() * 3, 30);
			labelTitulo.setFont(font1);
			this.add(labelTitulo);

			JLabel labelSubtitulo = new JLabel("Liceo Campo David",
					JLabel.CENTER);
			labelSubtitulo.setLocation(labelTitulo.getX(), labelTitulo.getY()
					+ anchoP / 4);
			labelSubtitulo.setSize(labelTitulo.getWidth(),
					labelTitulo.getHeight());
			labelSubtitulo.setFont(font2);
			this.add(labelSubtitulo);
		}

		/******************* PANEL PREGUNTAS *******************/

		int x = 10;
		int y = panel_logo.getY() + panel_logo.getWidth() + 10;

		panel_preguntas = new JPanel();
		this.add(panel_preguntas);
		panel_preguntas.setLocation(x, y);
		panel_preguntas.setSize(anchoPantalla / 8, y * 7 / 5);
		panel_preguntas.setLayout(new GridLayout(0, 1));
		panel_preguntas.setOpaque(false);
		panel_preguntas.setBorder(new BordeTitulado(""));

		/******************* PANEL TEMAS *******************/

		panel_temas = new JPanel();
		this.add(panel_temas);
		panel_temas.setLocation(
				panel_preguntas.getX() + panel_preguntas.getWidth() + x,
				panel_preguntas.getY());
		panel_temas.setSize(panel_preguntas.getWidth(),
				panel_preguntas.getHeight());
		panel_temas.setLayout(new GridLayout(0, 1));
		panel_temas.setOpaque(false);
		if (permisos()) {
			panel_temas.setBorder(new BordeTitulado(""));
		}

		/******************* PANEL CONTROL USUARIO *******************/

		panel_control = new JPanel();
		this.add(panel_control);
		panel_control.setLocation(panel_temas.getX() + panel_temas.getWidth()
				+ x, panel_temas.getY());
		panel_control.setSize(panel_temas.getWidth(), panel_temas.getHeight());
		panel_control.setLayout(new GridLayout(0, 1));
		panel_control.setOpaque(false);
		panel_control.setBorder(new BordeTitulado(""));

	}

	/**
	 * Crear tema.
	 */
	private void accionBotonCrearTema() {
		// TODO Boton Crear Tema
		nuevaVentana(new CrearTema(this, docente));
	}

	/**
	 * Consultar tema.
	 */
	private void accionBotonConsultarTema() {
		// TODO Boton Consultar Tema
		nuevaVentana(new ConsultarTema(this, docente));
	}

	/**
	 * Crear subtema.
	 */
	private void accionBotonCrearSubtema() {
		// TODO Boton Crear Subtema
		nuevaVentana(new CrearSubtema(this, docente));
	}

	/**
	 * Consultar subtema.
	 */
	private void accionBotonConsultarSubtema() {
		// TODO Boton Consultar Subtema
		nuevaVentana(new ConsultarSubtema(this, docente));
	}

	/**
	 * Crear cuestionario.
	 */
	private void accionBotonCrearCuestionario() {
		// TODO Boton Crear Cuestionario
		nuevaVentana(new CrearCuestionario(this, docente, true));
	}

	/**
	 * Crear pregunta.
	 */
	private void accionBotonCrearPregunta() {
		// TODO Boton Crear Pregunta
		nuevaVentana(new CrearPregunta(this, docente));
	}

	/**
	 * Consultar preguntas.
	 */
	private void accionBotonConsultarPreguntas() {
		// TODO Boton Consultar Preguntas
		if (permisos()) {
			//Entra aqui cuando es super usuario
			nuevaVentana(new ConsultarPreguntas(this, docente));
		} else {
			nuevaVentana(new CrearCuestionario(this, docente, false));
		}
	}

	/**
	 * Cambiar contrasenia.
	 */
	private void accionBotonCambiarContrasenia() {
		// TODO Boton Cambiar Contrasenia
		nuevaVentana(new CambiarContrasenia(this, docente));
	}

	/**
	 * Cerrar sesion.
	 */
	private void accionBotonCerrar() {
		// TODO Boton Cerrar Sesion
		int option = JOptionPane.showConfirmDialog(this,
				"�Est� seguro de que desea cerrar sesi�n?",
				"Confirmar cierre de sesi�n", JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);
		if (option == JOptionPane.YES_OPTION) {
			cerrarSesion();
		}
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
	}

	/**
	 * Adiciona un label vacio al panel especificado.
	 * 
	 * @param panel
	 */
	private void addEmptyLabel(JPanel panel) {
		JLabel label = new JLabel();
		panel.add(label);
	}
}
