package view;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.*;

import javax.swing.*;
import javax.swing.filechooser.*;

import model.*;
import controller.*;
import view.utils.*;

public class CrearPregunta extends General {
	private static final long serialVersionUID = 480066173137555224L;

	// Panel Formulario
	private JPanel panel_formulario;

	// Contenido Formulario
	private JLabel lbl_grado, lbl_area, lbl_asignatura, lbl_tema, lbl_subtema,
			lbl_competencia, lbl_nivel;
	private JComboBox<String> combo_grados, combo_areas, combo_asignaturas,
			combo_temas, combo_subtemas, combo_competencias, combo_niveles;

	// Contenido Formulario
	private JLabel lbl_enunciado, lbl_enunciado2, lbl_rutaImagen,
			lbl_respuestas;
	private JTextArea txt_enunciado, txt_enunciado2;
	private JTextField txt_rutaImagen, txt_opcion1, txt_opcion2, txt_opcion3,
			txt_opcion4, txt_opcion5;
	private ButtonGroup btng_opciones;
	private JRadioButton rbtn_opcion1, rbtn_opcion2, rbtn_opcion3,
			rbtn_opcion4, rbtn_opcion5;
	private JButton btn_rutaImagen;

	// Panel de Imagen
	private JPanel panel_imagen;
	private JLabel lbl_imagen;
	private JFileChooser fc_imagen;

	// Panel Botones
	private JPanel panel_botones;
	private JButton btn_crear, btn_regresar;

	// Utilitarios
	private static final String titulo = "Adicionar Pregunta";
	private Docente docente;
	private CrearPreguntaController controller;
	
	private static final String usoConocimiento = "Uso del Conocimiento";
	private static final String explicativa = "Explicativa";
	private static final String indagacion = "Indagaci�n";
	private static final String conceptual = "Conceptual";

	/**
	 * Inicializa la ventana de crear pregunta.
	 * 
	 * @param anterior
	 *            Ventana anterior.
	 * @param docente
	 */
	public CrearPregunta(JFrame anterior, Docente docente) {
		super(anterior, anchoPantalla, 8 * altoPantalla / 9);
		this.docente = docente;
		controller = new CrearPreguntaController();

		prepareElementosPanelFormulario();
		definaAccionesFormulario();
		prepareElementosPanelBotones();
		definaAccionesBotones();
	}

	/**
	 * Prepara los elementos del panel de formulario.
	 */
	private void prepareElementosPanelFormulario() {
		int anchoP = anchoPantalla / 10;
		int x = anchoP / 7, alto = anchoP / 5;
		int aumentoX = anchoP / 5, aumentoY = anchoP / 3, reduccion = anchoP / 10;
		int anchoLbl = 3 * anchoP / 4, anchoTxt = 2 * anchoP;
		int comboGrande = anchoTxt, comboPeque�o = anchoTxt / 2, radio = 4 * comboPeque�o / 9;
		int aumentoCombo = 6;

		panel_formulario = new JPanel();
		panel_formulario.setLocation(0, panel_barraTitulos.getHeight());
		panel_formulario.setSize(this.getWidth(), this.getHeight()
				- panel_barraTitulos.getHeight() * 2);
		panel_formulario.setLayout(null);
		panel_formulario.setOpaque(false);
		this.add(panel_formulario);

		lbl_grado = new JLabel("(*) Grado: ", JLabel.LEFT);
		lbl_grado.setLocation(x, panel_formulario.getY() - reduccion);
		lbl_grado.setSize(anchoLbl, alto);
		lbl_grado.setForeground(letra.getColorLabel());
		lbl_grado.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_grado);

		combo_grados = new JComboBox<String>();
		combo_grados.setLocation(lbl_grado.getWidth() + aumentoX,
				lbl_grado.getY());
		combo_grados.setSize(comboPeque�o, alto);
		combo_grados.setForeground(letra.getColorCombo());
		combo_grados.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_grados);

		// TODO Poblar combo con grados
		ArrayList<Grado> grados = controller.obtenerGrados();
		for (int i = 0; i < grados.size(); i++) {
			combo_grados.addItem(grados.get(i).getNombre());
		}
		combo_grados.setSelectedIndex(-1);

		lbl_area = new JLabel("(*) �rea: ", JLabel.LEFT);
		lbl_area.setLocation(lbl_grado.getX(), lbl_grado.getY() + aumentoY);
		lbl_area.setSize(lbl_grado.getWidth(), lbl_grado.getHeight());
		lbl_area.setForeground(letra.getColorLabel());
		lbl_area.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_area);

		combo_areas = new JComboBox<String>();
		combo_areas.setLocation(combo_grados.getX(), lbl_area.getY());
		combo_areas.setSize(comboGrande, combo_grados.getHeight());
		combo_areas.setForeground(letra.getColorCombo());
		combo_areas.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_areas);

		// TODO Poblar combo con areas
		ArrayList<Area> areas = controller.obtenerAreas(docente);
		for (int i = 0; i < areas.size(); i++) {
			combo_areas.addItem(areas.get(i).getNombre());
		}
		combo_areas.setSelectedIndex(-1);

		lbl_asignatura = new JLabel("(*) Asignatura: ", JLabel.LEFT);
		lbl_asignatura.setLocation(lbl_area.getX(), lbl_area.getY() + aumentoY);
		lbl_asignatura.setSize(lbl_area.getWidth(), lbl_area.getHeight());
		lbl_asignatura.setForeground(letra.getColorLabel());
		lbl_asignatura.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_asignatura);

		combo_asignaturas = new JComboBox<String>();
		combo_asignaturas.setLocation(combo_areas.getX(), lbl_asignatura.getY());
		combo_asignaturas.setSize(comboGrande, combo_areas.getHeight());
		combo_asignaturas.setForeground(letra.getColorCombo());
		combo_asignaturas.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_asignaturas);

		lbl_tema = new JLabel("(*) Tema: ", JLabel.LEFT);
		lbl_tema.setLocation(lbl_asignatura.getX(), lbl_asignatura.getY()
				+ aumentoY);
		lbl_tema.setSize(lbl_asignatura.getWidth(), lbl_asignatura.getHeight());
		lbl_tema.setForeground(letra.getColorLabel());
		lbl_tema.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_tema);

		combo_temas = new JComboBox<String>();
		combo_temas.setLocation(combo_asignaturas.getX(), lbl_tema.getY());
		combo_temas.setSize(comboPeque�o, combo_asignaturas.getHeight());
		combo_temas.setForeground(letra.getColorCombo());
		combo_temas.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_temas);

		lbl_subtema = new JLabel("(*) Subtema: ", JLabel.LEFT);
		lbl_subtema.setLocation(combo_temas.getWidth() + aumentoX
				* aumentoCombo, combo_temas.getY());
		lbl_subtema.setSize(lbl_tema.getWidth(), lbl_tema.getHeight());
		lbl_subtema.setForeground(letra.getColorLabel());
		lbl_subtema.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_subtema);

		combo_subtemas = new JComboBox<String>();
		combo_subtemas.setLocation(lbl_subtema.getWidth() + aumentoX
				* aumentoCombo * 2, lbl_subtema.getY());
		combo_subtemas.setSize(combo_temas.getWidth(), combo_temas.getHeight());
		combo_subtemas.setForeground(letra.getColorCombo());
		combo_subtemas.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_subtemas);

		lbl_competencia = new JLabel("(*) Competencia: ", JLabel.LEFT);
		lbl_competencia
				.setLocation(lbl_tema.getX(), lbl_tema.getY() + aumentoY);
		lbl_competencia.setSize(lbl_tema.getWidth(), lbl_tema.getHeight());
		lbl_competencia.setForeground(letra.getColorLabel());
		lbl_competencia.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_competencia);

		combo_competencias = new JComboBox<String>();
		combo_competencias.setLocation(combo_temas.getX(),
				lbl_competencia.getY());
		combo_competencias.setSize(comboPeque�o, combo_temas.getHeight());
		combo_competencias.setForeground(letra.getColorCombo());
		combo_competencias.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_competencias);

		lbl_nivel = new JLabel("(*) Nivel: ", JLabel.LEFT);
		lbl_nivel.setLocation(combo_competencias.getWidth() + aumentoX
				* aumentoCombo, combo_competencias.getY());
		lbl_nivel.setSize(lbl_competencia.getWidth(),
				lbl_competencia.getHeight());
		lbl_nivel.setForeground(letra.getColorLabel());
		lbl_nivel.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_nivel);

		combo_niveles = new JComboBox<String>();
		combo_niveles.setLocation(lbl_nivel.getWidth() + aumentoX* aumentoCombo * 2, lbl_nivel.getY());
		combo_niveles.setSize(combo_competencias.getWidth(),combo_competencias.getHeight());
		combo_niveles.setForeground(letra.getColorCombo());
		combo_niveles.setFont(letra.getFuenteCombo());
		panel_formulario.add(combo_niveles);

		// Adicionando Campos
		combo_competencias.addItem(usoConocimiento);
		combo_competencias.addItem(explicativa);
		combo_competencias.addItem(indagacion);
		combo_competencias.addItem(conceptual);
		combo_competencias.setSelectedIndex(-1);

		combo_niveles.addItem("Alto");
		combo_niveles.addItem("Medio");
		combo_niveles.addItem("Bajo");
		combo_niveles.setSelectedIndex(-1);

		lbl_enunciado = new JLabel("(*) Enunciado: ", JLabel.LEFT);
		lbl_enunciado.setLocation(combo_niveles.getX() + combo_niveles.getWidth() + aumentoX / 3* aumentoCombo, panel_formulario.getY() - reduccion);
		lbl_enunciado.setSize(anchoLbl, alto);
		lbl_enunciado.setForeground(letra.getColorLabel());
		lbl_enunciado.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_enunciado);

		txt_enunciado = new JTextArea();
		txt_enunciado.setLocation(
				lbl_enunciado.getX() + lbl_enunciado.getWidth() + aumentoX,
				lbl_enunciado.getY());
		txt_enunciado.setSize(comboGrande + anchoP * 7 / 3, alto * 3);
		txt_enunciado.setFont(letra.getFuenteTxt());
		txt_enunciado.setForeground(letra.getColorTxt());
		txt_enunciado.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_enunciado);
		// txt_enunciado.setColumns(25);
		// txt_enunciado.setRows(3);
		txt_enunciado.setAutoscrolls(false);
		txt_enunciado.setLineWrap(true);

		lbl_rutaImagen = new JLabel("Imagen: ", JLabel.LEFT);
		lbl_rutaImagen.setLocation(lbl_enunciado.getX(), lbl_enunciado.getY()
				+ lbl_enunciado.getHeight() + aumentoY * 2);
		lbl_rutaImagen.setSize(anchoLbl, alto);
		lbl_rutaImagen.setForeground(letra.getColorLabel());
		lbl_rutaImagen.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_rutaImagen);

		txt_rutaImagen = new JTextField();

		panel_imagen = new JPanel();
		panel_imagen.setLocation(txt_enunciado.getX(), txt_enunciado.getY() + 2
				* aumentoY);
		panel_imagen.setSize(2 * anchoP, anchoP);
		panel_imagen.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		panel_formulario.add(panel_imagen);

		lbl_imagen = new JLabel();
		panel_imagen.add(lbl_imagen);

		fc_imagen = new JFileChooser();
		fc_imagen.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filtro = new FileNameExtensionFilter(
				"Im�genes (jpg, jpeg, png)", "jpg", "jpeg", "png");
		fc_imagen.setFileFilter(filtro);

		btn_rutaImagen = new Boton(txt_enunciado.getX() + 7
				* txt_enunciado.getWidth() / 16 + anchoP / 3,
				lbl_rutaImagen.getY(), anchoLbl + aumentoX, alto,
				"A�adir Imagen", panel_formulario, this) {
			private static final long serialVersionUID = 2146416023455808870L;

			@Override
			public void accionBoton() {
				accionBotonRutaImagen();
			}
		};
		btn_rutaImagen.setVisible(true);

		lbl_enunciado2 = new JLabel("Enunciado Esp: ", JLabel.LEFT);
		lbl_enunciado2.setLocation(lbl_competencia.getX(), panel_imagen.getY()
				+ panel_imagen.getHeight() + aumentoY / 2);
		lbl_enunciado2.setSize(anchoLbl, alto);
		lbl_enunciado2.setForeground(letra.getColorLabel());
		lbl_enunciado2.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_enunciado2);

		txt_enunciado2 = new JTextArea();
		txt_enunciado2.setLocation(lbl_enunciado2.getWidth() + aumentoX,
				lbl_enunciado2.getY());
		txt_enunciado2.setSize(txt_enunciado.getWidth(), alto * 3);
		txt_enunciado2.setFont(letra.getFuenteTxt());
		txt_enunciado2.setForeground(letra.getColorTxt());
		txt_enunciado2.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_enunciado2);
		txt_enunciado2.setAutoscrolls(false);
		txt_enunciado2.setLineWrap(true);

		lbl_respuestas = new JLabel("(*) Respuestas: ", JLabel.LEFT);
		lbl_respuestas.setLocation(lbl_enunciado2.getX(), txt_enunciado2.getY()
				+ txt_enunciado2.getHeight());
		lbl_respuestas.setSize(anchoLbl, alto);
		lbl_respuestas.setForeground(letra.getColorLabel());
		lbl_respuestas.setFont(letra.getFuenteLabel());
		panel_formulario.add(lbl_respuestas);

		rbtn_opcion1 = new JRadioButton("A");
		rbtn_opcion1.setLocation(lbl_respuestas.getX() + 2 * x,
				lbl_respuestas.getY() + lbl_respuestas.getHeight());
		rbtn_opcion1.setSize(radio, alto);
		rbtn_opcion1.setOpaque(false);
		rbtn_opcion1.setContentAreaFilled(false);
		rbtn_opcion1.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion1);

		txt_opcion1 = new JTextField();
		txt_opcion1.setLocation(txt_enunciado2.getX(), rbtn_opcion1.getY());
		txt_opcion1.setSize(txt_enunciado.getWidth(), alto);
		txt_opcion1.setFont(letra.getFuenteTxt());
		txt_opcion1.setForeground(letra.getColorTxt());
		txt_opcion1.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion1);

		rbtn_opcion2 = new JRadioButton("B");
		rbtn_opcion2.setLocation(rbtn_opcion1.getX(), rbtn_opcion1.getY()
				+ aumentoY);
		rbtn_opcion2.setSize(radio, alto);
		rbtn_opcion2.setOpaque(false);
		rbtn_opcion2.setContentAreaFilled(false);
		rbtn_opcion2.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion2);

		txt_opcion2 = new JTextField();
		txt_opcion2.setLocation(txt_opcion1.getX(), rbtn_opcion2.getY());
		txt_opcion2.setSize(txt_opcion1.getWidth(), txt_opcion1.getHeight());
		txt_opcion2.setFont(letra.getFuenteTxt());
		txt_opcion2.setForeground(letra.getColorTxt());
		txt_opcion2.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion2);

		rbtn_opcion3 = new JRadioButton("C");
		rbtn_opcion3.setLocation(rbtn_opcion2.getX(), rbtn_opcion2.getY()
				+ aumentoY);
		rbtn_opcion3.setSize(radio, alto);
		rbtn_opcion3.setOpaque(false);
		rbtn_opcion3.setContentAreaFilled(false);
		rbtn_opcion3.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion3);

		txt_opcion3 = new JTextField();
		txt_opcion3.setLocation(txt_opcion2.getX(), rbtn_opcion3.getY());
		txt_opcion3.setSize(txt_opcion2.getWidth(), txt_opcion2.getHeight());
		txt_opcion3.setFont(letra.getFuenteTxt());
		txt_opcion3.setForeground(letra.getColorTxt());
		txt_opcion3.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion3);

		rbtn_opcion4 = new JRadioButton("D");
		rbtn_opcion4.setLocation(rbtn_opcion3.getX(), rbtn_opcion3.getY()
				+ aumentoY);
		rbtn_opcion4.setSize(radio, alto);
		rbtn_opcion4.setOpaque(false);
		rbtn_opcion4.setContentAreaFilled(false);
		rbtn_opcion4.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion4);

		txt_opcion4 = new JTextField();
		txt_opcion4.setLocation(txt_opcion3.getX(), rbtn_opcion4.getY());
		txt_opcion4.setSize(txt_opcion3.getWidth(), txt_opcion3.getHeight());
		txt_opcion4.setFont(letra.getFuenteTxt());
		txt_opcion4.setForeground(letra.getColorTxt());
		txt_opcion4.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion4);

		rbtn_opcion5 = new JRadioButton("E");
		rbtn_opcion5.setLocation(rbtn_opcion4.getX(), rbtn_opcion4.getY()
				+ aumentoY);
		rbtn_opcion5.setSize(radio, alto);
		rbtn_opcion5.setOpaque(false);
		rbtn_opcion5.setContentAreaFilled(false);
		rbtn_opcion5.setBorderPainted(false);
		panel_formulario.add(rbtn_opcion5);
		rbtn_opcion5.setEnabled(false);

		txt_opcion5 = new JTextField();
		txt_opcion5.setLocation(txt_opcion4.getX(), rbtn_opcion5.getY());
		txt_opcion5.setSize(txt_opcion4.getWidth(), txt_opcion4.getHeight());
		txt_opcion5.setFont(letra.getFuenteTxt());
		txt_opcion5.setForeground(letra.getColorTxt());
		txt_opcion5.setDisabledTextColor(letra.getColorTxtDisable());
		panel_formulario.add(txt_opcion5);
		txt_opcion5.setEnabled(false);

		btng_opciones = new ButtonGroup();
		btng_opciones.add(rbtn_opcion1);
		btng_opciones.add(rbtn_opcion2);
		btng_opciones.add(rbtn_opcion3);
		btng_opciones.add(rbtn_opcion4);
		btng_opciones.add(rbtn_opcion5);
	}

	/**
	 * Define las acciones dentro del panel de formulario.
	 */
	private void definaAccionesFormulario() {
		combo_grados.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_areas.grabFocus();
				}
					break;
				}
			}
		});

		combo_areas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_grados.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_asignaturas.grabFocus();
				}
					break;
				}
			}
		});
		combo_areas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Poblar combo con asignaturas
				combo_asignaturas.removeAllItems();
				int areaSeleccionada = combo_areas.getSelectedIndex();
				if (areaSeleccionada != -1) {
					ArrayList<Materia> materias = controller.obtenerMaterias(
							combo_areas.getItemAt(areaSeleccionada), docente);
					for (int i = 0; i < materias.size(); i++) {
						combo_asignaturas.addItem(materias.get(i).getNombre());
					}
					combo_asignaturas.setSelectedIndex(-1);
				}
			}
		});

		combo_asignaturas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_areas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_temas.grabFocus();
				}
					break;
				}
			}
		});
		combo_asignaturas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Poblar combo con temas
				combo_temas.removeAllItems();
				int materiaSeleccionada = combo_asignaturas.getSelectedIndex();
				if (materiaSeleccionada != -1) {
					ArrayList<Tema> temas = controller.obtenerTemas(combo_asignaturas
									.getItemAt(materiaSeleccionada));
					for (int i = 0; i < temas.size(); i++) {
						combo_temas.addItem(temas.get(i).getNombre());
					}
					combo_temas.setSelectedIndex(-1);
				}
			}
		});

		combo_temas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_asignaturas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_subtemas.grabFocus();
				}
					break;
				}
			}
		});
		combo_temas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Poblar combo con subtemas
				combo_subtemas.removeAllItems();
				int temaSeleccionado = combo_temas.getSelectedIndex();
				if (temaSeleccionado != -1) {
					ArrayList<Tema> subtemas = controller.obtenerSubtemas(combo_temas
									.getItemAt(temaSeleccionado));
					for (int i = 0; i < subtemas.size(); i++) {
						combo_subtemas.addItem(subtemas.get(i).getNombre());
						System.out.println("id Subtema:  "+subtemas.get(i).getId());
					}
					combo_subtemas.setSelectedIndex(-1);
				}
			}
		});

		combo_subtemas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_temas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_competencias.grabFocus();
				}
					break;
				}
			}
		});

		combo_competencias.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				int selected = combo_competencias.getSelectedIndex();
				if (selected != -1) {
					String competencia = combo_competencias.getItemAt(selected);
					if (competencia.equals(conceptual)) {
						rbtn_opcion5.setEnabled(true);
						txt_opcion5.setEnabled(true);
					} else {
						if (rbtn_opcion5.isSelected()) {
							btng_opciones.clearSelection();
						}
						rbtn_opcion5.setEnabled(false);
						txt_opcion5.setEnabled(false);
					}
				} else {
					if (rbtn_opcion5.isSelected()) {
						btng_opciones.clearSelection();
					}
					rbtn_opcion5.setEnabled(false);
					txt_opcion5.setEnabled(false);
				}
			}
		});
		combo_competencias.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_subtemas.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					combo_niveles.grabFocus();
				}
					break;
				}
			}
		});

		combo_niveles.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					combo_competencias.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					txt_enunciado.grabFocus();
				}
					break;
				}
			}
		});
		rbtn_opcion1.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_DOWN: {
					rbtn_opcion2.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					txt_opcion1.grabFocus();
				}
					break;
				}
			}
		});

		txt_opcion1.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_DOWN: {
					txt_opcion2.grabFocus();
				}
					break;
				}
			}
		});

		rbtn_opcion2.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					rbtn_opcion1.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					rbtn_opcion3.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					txt_opcion2.grabFocus();
				}
					break;
				}
			}
		});

		txt_opcion2.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_opcion1.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					txt_opcion3.grabFocus();
				}
					break;
				}
			}
		});

		rbtn_opcion3.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					rbtn_opcion2.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					rbtn_opcion4.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					txt_opcion3.grabFocus();
				}
					break;
				}
			}
		});

		txt_opcion3.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_opcion2.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					txt_opcion4.grabFocus();
				}
					break;
				}
			}
		});

		rbtn_opcion4.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					rbtn_opcion3.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					boolean enabled = rbtn_opcion5.isEnabled();
					if (enabled) {
						rbtn_opcion5.grabFocus();
					} else {
						btn_crear.grabFocus();
					}
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					txt_opcion4.grabFocus();
				}
					break;
				}
			}
		});

		txt_opcion4.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_opcion3.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					boolean enabled = txt_opcion5.isEnabled();
					if (enabled) {
						txt_opcion5.grabFocus();
					} else {
						btn_crear.grabFocus();
					}
				}
					break;
				}
			}
		});

		rbtn_opcion5.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					rbtn_opcion4.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					btn_crear.grabFocus();
				}
					break;
				case KeyEvent.VK_RIGHT:
				case KeyEvent.VK_ENTER: {
					txt_opcion5.grabFocus();
				}
					break;
				}
			}
		});

		txt_opcion5.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					txt_opcion4.grabFocus();
				}
					break;
				case KeyEvent.VK_DOWN: {
					btn_crear.grabFocus();
				}
					break;
				}
			}
		});
	}

	/**
	 * Prepara los elementos del panel de botones.
	 */
	private void prepareElementosPanelBotones() {
		int anchoBtn = 150;
		int altoBtn = 30;

		panel_botones = new JPanel();
		panel_botones.setLocation(panel_formulario.getWidth() / 4,
				panel_formulario.getHeight());
		panel_botones.setSize(panel_formulario.getWidth() / 2, altoBtn);
		panel_botones.setLayout(new GridLayout(1, 0));
		panel_botones.setOpaque(false);
		this.add(panel_botones);

		btn_crear = new Boton(0, 0, anchoBtn, altoBtn, "Crear Pregunta",
				panel_botones, this) {
			private static final long serialVersionUID = -5785655676854998075L;

			@Override
			public void accionBoton() {
				accionBotonCrearPregunta();
			}
		};

		btn_regresar = new Boton(0, 0, anchoBtn, altoBtn, "Regresar",
				panel_botones, this) {
			private static final long serialVersionUID = -4456446890553142037L;

			@Override
			public void accionBoton() {
				accionBotonRegresar();
			}
		};
	}

	/**
	 * Define las acciones dentro del panel de botones.
	 */
	private void definaAccionesBotones() {
		btn_crear.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_UP: {
					boolean enabled = txt_opcion5.isEnabled();
					if (enabled) {
						txt_opcion5.grabFocus();
					} else {
						txt_opcion4.grabFocus();
					}
				}
					break;
				case KeyEvent.VK_RIGHT: {
					btn_regresar.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_crear.doClick();
				}
					break;
				}
			}
		});

		btn_regresar.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent evt) {
				switch (evt.getKeyCode()) {
				case KeyEvent.VK_LEFT: {
					btn_crear.grabFocus();
				}
					break;
				case KeyEvent.VK_ENTER: {
					btn_regresar.doClick();
				}
					break;
				}
			}
		});
	}

	/**
	 * Lee el archivo seleccionado y carga su imagen.
	 */
	private void accionBotonRutaImagen() {// TODO Boton Ruta Imagen
		int seleccion = fc_imagen.showOpenDialog(this);
		if (seleccion == JFileChooser.APPROVE_OPTION) 
		{	String fichero = fc_imagen.getSelectedFile().getAbsolutePath() .replace("\\", "/");
		System.out.println("ruta img:  "+fichero);
			txt_rutaImagen.setText(fichero);
			lbl_imagen.setIcon(Imagen.convert2Image(txt_rutaImagen.getText(), panel_imagen.getWidth(), panel_imagen.getHeight()));
		}
	}

	/**
	 * Permite crear la pregunta.
	 */
	private void accionBotonCrearPregunta() {
		// TODO Boton Crear Pregunta

		int gradoSeleccionado = combo_grados.getSelectedIndex();
		int areaSeleccionada = combo_areas.getSelectedIndex();
		int materiaSeleccionada = combo_asignaturas.getSelectedIndex();
		int temaSeleccionado = combo_temas.getSelectedIndex();
		int subtemaSeleccionado = combo_subtemas.getSelectedIndex();
		int competenciaSeleccionada = combo_competencias.getSelectedIndex();
		int nivelSeleccionado = combo_niveles.getSelectedIndex();

		String enunciado = txt_enunciado.getText();
		String rutaImagen = txt_rutaImagen.getText();
		String enunciado2 = txt_enunciado2.getText();
		System.out.println("ruta final"+rutaImagen);
		char b_opcion1 = rbtn_opcion1.isSelected() ? 'S' : 'N';
		String t_opcion1 = txt_opcion1.getText();

		char b_opcion2 = rbtn_opcion2.isSelected() ? 'S' : 'N';
		String t_opcion2 = txt_opcion2.getText();

		char b_opcion3 = rbtn_opcion3.isSelected() ? 'S' : 'N';
		String t_opcion3 = txt_opcion3.getText();

		char b_opcion4 = rbtn_opcion4.isSelected() ? 'S' : 'N';
		String t_opcion4 = txt_opcion4.getText();

		char b_opcion5 = rbtn_opcion5.isSelected() ? 'S' : 'N';
		String t_opcion5 = txt_opcion5.getText();
		boolean enabled_b_opcion5 = rbtn_opcion5.isEnabled();
		boolean enabled_t_opcion5 = txt_opcion5.isEnabled();

		if (gradoSeleccionado != -1 && areaSeleccionada != -1 && materiaSeleccionada != -1 && temaSeleccionado != -1 && subtemaSeleccionado != -1 && competenciaSeleccionada != -1 && nivelSeleccionado != -1) {

			if (!enunciado.isEmpty()
					&& !t_opcion1.isEmpty()
					&& !t_opcion2.isEmpty()
					&& !t_opcion3.isEmpty()
					&& !t_opcion4.isEmpty()
					&& (b_opcion1 == 'S' || b_opcion2 == 'S'
							|| b_opcion3 == 'S' || b_opcion4 == 'S' || (enabled_b_opcion5 && b_opcion5 == 'S'))
					&& (!enabled_t_opcion5 || (enabled_t_opcion5 && !t_opcion5
							.isEmpty()))) {

				Grado grado = controller.obtenerGrado(combo_grados.getItemAt(gradoSeleccionado));
				Area area = controller.obtenerArea(combo_areas.getItemAt(areaSeleccionada));
				Materia materia = controller.obtenerMateria(combo_asignaturas.getItemAt(materiaSeleccionada));
				Tema subtema = controller.obtenerSubtema(combo_subtemas.getItemAt(subtemaSeleccionado));
				String competencia = combo_competencias.getItemAt(competenciaSeleccionada);
				String nivel = combo_niveles.getItemAt(nivelSeleccionado);
				int idPregunta = controller.obtenerIdMaximoPregunta();
				try {
					controller.agregarPregunta(idPregunta, competencia, nivel,
							docente, grado, area, materia, subtema, enunciado,
							rutaImagen, enunciado2);
					controller.agregarOpcion(t_opcion1, b_opcion1, idPregunta, "A");
					controller.agregarOpcion(t_opcion2, b_opcion2, idPregunta, "B");
					controller.agregarOpcion(t_opcion3, b_opcion3, idPregunta, "C");
					controller.agregarOpcion(t_opcion4, b_opcion4, idPregunta, "D");
					if (enabled_b_opcion5 && enabled_t_opcion5) {
						controller.agregarOpcion(t_opcion5, b_opcion5,	idPregunta, "E");
					}

					JOptionPane.showMessageDialog(null,"Pregunta guardada exitosamente.\n"+ "El identificador de la pregunta es "+ idPregunta, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
					cerrarVentana();
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
					e.printStackTrace();
				}
			} else {
				if (enunciado.isEmpty()) {
					JOptionPane.showOptionDialog(null,
							"Debe ingresar el enunciado.", "Falta enunciado", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_enunciado.grabFocus();
				} else if (t_opcion1.isEmpty()) {
					JOptionPane.showOptionDialog(null,
							"Debe ingresar la opci�n A.", "Falta opci�n A", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_opcion1.grabFocus();
				} else if (t_opcion2.isEmpty()) {
					JOptionPane.showOptionDialog(null,
							"Debe ingresar la opci�n B.", "Falta opci�n B",  JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_opcion2.grabFocus();
				} else if (t_opcion3.isEmpty()) {
					JOptionPane.showOptionDialog(null,
							"Debe ingresar la opci�n C.", "Falta opci�n C", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_opcion3.grabFocus();
				} else if (t_opcion4.isEmpty()) {
					JOptionPane.showOptionDialog(null,
							"Debe ingresar la opci�n D.", "Falta opci�n D", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_opcion4.grabFocus();
				} else if (enabled_t_opcion5 && t_opcion5.isEmpty()) {
					JOptionPane.showOptionDialog(null,
							"Debe ingresar la opci�n E.", "Falta opci�n E", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					txt_opcion5.grabFocus();
				} else if (!(b_opcion1 == 'S' || b_opcion2 == 'S' || b_opcion3 == 'S' || b_opcion4 == 'S' || (enabled_b_opcion5 && b_opcion5 == 'S'))) {
					JOptionPane.showOptionDialog(null,
							"Debe seleccionar la respuesta correcta.", "Falta respuesta", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
					rbtn_opcion1.grabFocus();
				}
			}
		} else {
			if (gradoSeleccionado == -1) {
				JOptionPane.showOptionDialog(null,
						"Debe seleccionar el grado de la pregunta.", "Falta grado", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_grados.grabFocus();
			} else if (areaSeleccionada == -1) {
				JOptionPane.showOptionDialog(null,
						"Debe seleccionar el area de la pregunta.", "Falta area", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_areas.grabFocus();
			} else if (materiaSeleccionada == -1) {
				JOptionPane.showOptionDialog(null, "Debe seleccionar la asignatura de la pregunta.", "Falta asignatura", JOptionPane.DEFAULT_OPTION,
						JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_asignaturas.grabFocus();
			} else if (temaSeleccionado == -1) {
				JOptionPane.showOptionDialog(null, "Debe seleccionar el tema de la pregunta.", "Falta tema", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_temas.grabFocus();
			} else if (subtemaSeleccionado == -1) {
				JOptionPane.showOptionDialog(null,
						"Debe seleccionar el subtema de la pregunta.", "Falta subtema", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_subtemas.grabFocus();
			} else if (competenciaSeleccionada == -1) {
				JOptionPane.showOptionDialog(null,
						"Debe seleccionar la competencia de la pregunta.", "Falta competencia", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_competencias.grabFocus();
			} else if (nivelSeleccionado == -1) {
				JOptionPane.showOptionDialog(null,
						"Debe seleccionar el nivel de la pregunta.", "Falta nivel", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, "aceptar");
				combo_niveles.grabFocus();
			}
		}
	}

	/**
	 * Regresar a la ventana anterior.
	 */
	private void accionBotonRegresar() {// TODO Boton Regresar
		cerrarVentana();
	}

	@Override
	protected String tituloPantalla() {
		return titulo;
	}

	@Override
	protected void actualizarPantalla() {
	}
}
