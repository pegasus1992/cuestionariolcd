package controller;

import java.io.*;
import java.sql.*;
import java.util.*;

import model.*;
import persistance.*;
import view.utils.*;

public class CrearPreguntaController {


	private Oracle oracle;

	public CrearPreguntaController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Obtiene los grados registrados.
	 * 
	 * @return
	 */
	public ArrayList<Grado> obtenerGrados() {
		ArrayList<Grado> grados;

		oracle.conectar();

		String sql = "SELECT * FROM Grados";
		try {
			grados = new ArrayList<Grado>();
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Grado grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
				grados.add(grado);
			}
			rs.close();
		} catch (Exception e) {
			grados = null;
			e.printStackTrace();
		}
		oracle.desconectar();

		return grados;
	}

	/**
	 * Obtiene las areas a las que tiene acceso el docente dado.
	 * 
	 * @param docente
	 * @return
	 */
	public ArrayList<Area> obtenerAreas(Docente docente) {
		ArrayList<Area> areas = new ArrayList<Area>();

		oracle.conectar();
		String condicion ="";
		if (!docente.getId().equals("ROOT")) {
			condicion = " WHERE ASG.id_docente = '" + docente.getId() + "'";
		}
		
		
		
		String sql = "SELECT DISTINCT A.id_area, A.orden orden_area, A.nombre_area "
				+ "FROM Areas A LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Asignaturas ASG ON (ASG.id_area = A.id_area OR ASG.id_docente = D.id_docente) "
				+ condicion;
		System.out.println(sql);
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				areas.add(area);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return areas;
	}

	/**
	 * Obtiene las materias que corresponden al area dada, a las que tiene
	 * acceso el docente dado.
	 * 
	 * @param nombreArea
	 * @param docente
	 * @return
	 */
	public ArrayList<Materia> obtenerMaterias(String nombreArea, Docente docente) {
		ArrayList<Materia> materias = new ArrayList<Materia>();

		oracle.conectar();
		String condicion ="";
		if (!docente.getId().equals("ROOT")) {
			condicion = " WHERE ASG.id_docente = '" + docente.getId() + "' ";
		}else{
			condicion = " WHERE A.nombre_area = '" + nombreArea + "' ";
		}
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Asignaturas ASG ON (ASG.id_area = A.id_area OR ASG.id_docente = D.id_docente OR ASG.id_materia = M.codigo_materia) "
				+ condicion ;
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				materias.add(materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materias;
	}

	/**
	 * Obtiene el area dado su nombre.
	 * 
	 * @param nombreArea
	 * @return
	 */
	public Area obtenerArea(String nombreArea) {
		Area area = null;

		oracle.conectar();

		String sql = "SELECT DISTINCT A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Areas A LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE A.nombre_area = '" + nombreArea + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return area;
	}

	/**
	 * Obtiene los temas que corresponden al nombre de materia dado.
	 * 
	 * @param nombreMateria
	 * @return El tema cuyo padre [3] sea <b>null</b>.
	 */
	public ArrayList<Tema> obtenerTemas(String nombreMateria) {
		ArrayList<Tema> temas;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "WHERE T.temaPadre IS NULL AND M.nombre_materia = '"
				+ nombreMateria + "'";
		try {
			temas = new ArrayList<Tema>();
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				temas.add(tema);
			}
			rs.close();
		} catch (Exception e) {
			temas = null;
			e.printStackTrace();
		}

		oracle.desconectar();

		return temas;
	}

	/**
	 * Obtiene la materia dado su nombre.
	 * 
	 * @param nombre
	 * @return
	 */
	public Materia obtenerMateria(String nombre) {
		Materia materia = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE M.nombre_materia = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materia;
	}

	/**
	 * Obtiene los subtemas que corresponden al nombre de tema dado.
	 * 
	 * @param nombreTema
	 * @return
	 */
	public ArrayList<Tema> obtenerSubtemas(String nombreTema) {
		ArrayList<Tema> subtemas = new ArrayList<Tema>();

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "WHERE T.temaPadre IS NOT NULL AND T2.nombre = '"
				+ nombreTema + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema subtema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				subtemas.add(subtema);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return subtemas;
	}

	/**
	 * Obtiene el tema dado su nombre.
	 * 
	 * @param nombre
	 * @return El tema cuyo padre [3] sea <b>null</b>.
	 */
	public Tema obtenerTema(String nombre) {
		Tema tema = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "WHERE T.temaPadre IS NULL AND T.nombre = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return tema;
	}

	/**
	 * Obtiene el tema dado el nombre del Subtema.
	 * 
	 * @param nombre
	 * @return El tema cuyo padre [3] <b>NO</b> sea <b>null</b>.
	 */
	public Tema obtenerSubtema(String nombre){
		Tema tema = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "WHERE T.temaPadre IS NOT NULL AND T.nombre = '"
				+ nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			System.out.println("obtener subtema: "+ sql);
			
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return tema;
	}

	/**
	 * Obtiene el grado dado su nombre
	 * 
	 * @param nombre
	 * @return
	 */
	public Grado obtenerGrado(String nombre) {
		Grado grado = null;

		oracle.conectar();

		String sql = "SELECT * FROM Grados WHERE nombre = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return grado;
	}

	/**
	 * Obtiene el id maximo de la tabla de preguntas.
	 * 
	 * @return El id maximo. Si no hay nada (solo la cabecera del archivo [==
	 *         1]) => retorna 1.
	 */
	public int obtenerIdMaximoPregunta() {
		int id = -1;

		oracle.conectar();

		String sql = "SELECT MAX(idPregunta) maxId FROM Preguntas";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String maxId = rs.getString("maxId");
				if (maxId != null) {
					id = Integer.parseInt(maxId) + 1;
				} else {
					id = 1;
				}
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return id;
	}

	/**
	 * Obtiene el id maximo de la tabla de opciones.
	 * 
	 * @return El id maximo. Si no hay nada (solo la cabecera del archivo [==
	 *         1]) => retorna 1.
	 */
	private int obtenerIdMaximoOpcion() {
		int id = -1;

		oracle.conectar();

		String sql = "SELECT MAX(idOpcion) maxId FROM Opciones";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String maxId = rs.getString("maxId");
				if (maxId != null) {
					id = Integer.parseInt(maxId) + 1;
				} else {
					id = 1;
				}
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return id;
	}

	/**
	 * Agrega una pregunta.
	 * 
	 * @param id
	 *            Id de la pregunta.
	 * @param competencia
	 *            Nombre de la competencia.
	 * @param nivel
	 *            Nivel de la pregunta.
	 * @param docente
	 *            Docente que ingresa la pregunta.
	 * @param grado
	 *            Grado al que aplica la pregunta.
	 * @param area
	 *            Area a la que aplica la pregunta.
	 * @param materia
	 *            Materia a la que aplica la pregunta.
	 * @param subtema
	 *            Subtema (tema padre == <b>null</b>) al que aplica la pregunta.
	 * @param enunciado
	 *            Enunciado de la pregunta.
	 * @param rutaImagen
	 *            Ruta de imagen del enunciado (si lo hay).
	 * @param enunciadoEspecifico
	 *            Enunciado secundario de la pregunta (si lo hay).
	 * @throws SQLException
	 */
	public void agregarPregunta(int id, String competencia, String nivel,Docente docente, Grado grado, Area area, Materia materia,Tema subtema, String enunciado, String rutaImagen,String enunciadoEspecifico) throws SQLException {
		System.out.println("RI"+rutaImagen);
		oracle.conectar();
		File imagenOrigen = rutaImagen != null && !rutaImagen.trim().isEmpty() ? new File(rutaImagen) : null;
		String imagen = null;
		if (imagenOrigen != null) {
			String nombre = imagenOrigen.getName();
			String extencion ="";
			String[] datos  = nombre.replace(".", ",").split(",");
			nombre = datos[0];
			extencion = datos[1];
			try {
				String rutaDestino = Oracle.rutaImagenes + nombre+String.valueOf(new java.util.Date()).replace(" ", "").replace(":", "")+"."+extencion;
				Utils.copyFileBit2Bit(rutaImagen, rutaDestino);
				imagen = rutaDestino;
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		String enunciado2 = enunciadoEspecifico != null
				&& !enunciadoEspecifico.trim().isEmpty() ? enunciadoEspecifico
				: null;

		String sql = "INSERT INTO Preguntas (idPregunta, competencia, nivel, idDocente, idGrado, idArea, idMateria, idSubtema, enunciado, imagen, enunciado2, FECHACREACION) "
				+ "VALUES ('"
				+ id
				+ "', '"
				+ competencia
				+ "', '"
				+ nivel
				+ "', '"
				+ docente.getId()
				+ "', "
				+ "'"
				+ grado.getId()
				+ "', '"
				+ area.getId()
				+ "', '"
				+ materia.getCodigo()
				+ "', '"
				+ subtema.getId()
				+ "', '"
				+ enunciado
				+ "', '"
				+ imagen
				+ "', '" + enunciado2 + "' , sysdate)";
		oracle.escribir(sql);

		oracle.desconectar();
	}

	/**
	 * Agrega una opcion a la pregunta dada.
	 * 
	 * @param opcion
	 *            Carretazo de la opci�n.
	 * @param correcta
	 *            �La respuesta es correcta? S o N.
	 * @param idPregunta
	 *            Id de la pregunta.
	 * @throws SQLException
	 */
	public void agregarOpcion(String opcion, char correcta, int idPregunta, String letra)
			throws SQLException {
		int idOpcion = obtenerIdMaximoOpcion();
		oracle.conectar();

		String sql = "INSERT INTO Opciones (idOpcion, respuesta, correcta, idPregunta, posicion)" + "VALUES ('" + idOpcion + "', '" + opcion + "', '" + correcta + "', '" + idPregunta + "', '"+letra+"')";
		oracle.escribir(sql);

		oracle.desconectar();
	}
}
