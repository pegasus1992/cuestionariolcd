package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import model.*;
import persistance.*;

public class CrearTemaController {

	private Oracle oracle;

	public CrearTemaController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Obtiene las materias registradas.
	 * 
	 * @return
	 */
	public ArrayList<Materia> obtenerMaterias() {
		ArrayList<Materia> materias;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente";
		try {
			materias = new ArrayList<Materia>();
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				materias.add(materia);
			}
			rs.close();
		} catch (Exception e) {
			materias = null;
			e.printStackTrace();
		}

		oracle.desconectar();

		return materias;
	}

	/**
	 * Obtiene el tema dado el nombre de la materia.
	 * 
	 * @param nombreMateria
	 * @return El tema cuyo padre [3] sea <b>null</b>.
	 */
	public ArrayList<Tema> obtenerTemas(String nombreMateria) {
		ArrayList<Tema> temas;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "WHERE T.temaPadre IS NULL AND M.nombre_materia = '"
				+ nombreMateria + "'";
		try {
			temas = new ArrayList<Tema>();
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				temas.add(tema);
			}
			rs.close();
		} catch (Exception e) {
			temas = null;
			e.printStackTrace();
		}

		oracle.desconectar();

		return temas;
	}

	/**
	 * Obtiene todos los  tema 
 
	 * @return retorna todos los temas
	 */
	public ArrayList<String[]>	 obtenerTodoTema() {
		ArrayList<String[]> temas = new ArrayList<String[]>();

		oracle.conectar();

		String sql = "select T.IDTEMA, T.NOMBRE, T.IDMATERIA, T.TEMAPADRE , M.CODIGO_MATERIA, M.NOMBRE_MATERIA , M.ORDEN, M.ID_AREA, M.TIENECOMPETENCIAS , M.TIENEPARCIALES, M.TIENEPRUEBAPI from MATERIAS M RIGHT JOIN TEMAS T ON(M.CODIGO_MATERIA  = T.IDMATERIA) where T.TEMAPADRE is null order by T.IDTEMA asc";
		try {
			System.out.println(sql);
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String[] datos = new String[12];	
	//			IDTEMA
				if (rs.getString(1) == null ) {
					datos[0] = "";
				}else{
					datos[0] = rs.getString(1);
				}
	//			NOMBRE
				if (rs.getString(2) == null ) {
					datos[1] = "";
				}else{
					datos[1] = rs.getString(2);
				}
	//			IDMATERIA
				if (rs.getString(3) == null ) {
					datos[2] = "";
				}else{
					datos[2] = rs.getString(3);
				}
	//			TEMAPADRE
				if (rs.getString(4) == null ) {
					datos[3] = "";
				}else{
					datos[3] = rs.getString(4);
				}
	//			CODIGO_MATERIA
				if (rs.getString(5) == null ) {
					datos[4] = "";
				}else{
					datos[4] = rs.getString(5);
				}
	//			NOMBRE_MATERIA
				if (rs.getString(6) == null ) {
					datos[5] = "";
				}else{
					datos[5] = rs.getString(6);
				}
	//			ORDEN
				if (rs.getString(7) == null ) {
					datos[6] = "";
				}else{
					datos[6] = rs.getString(7);
				}
	//			ID_AREA
				if (rs.getString(8) == null ) {
					datos[7] = "";
				}else{
					datos[7] = rs.getString(8);
				}
	//			TIENECOMPETENCIAS
				if (rs.getString(9) == null ) {
					datos[8] = "";
				}else{
					datos[8] = rs.getString(9);
				}
	//			TIENEPARCIALES
				if (rs.getString(10) == null ) {
					datos[9] = "";
				}else{
					datos[9] = rs.getString(10);
				}
	//			TIENEPRUEBAPI
				if (rs.getString(11) == null ) {
					datos[10] = "";
				}else{
					datos[10] = rs.getString(11);
				}
				
				temas.add(datos);	
			}
			rs.close();
		} catch (Exception e) {
			temas = null;
			e.printStackTrace();
		}
		oracle.desconectar();
		return temas;
	}

	/**
	 * Obtiene todos los subtemas
	 * @return retorna todos los temas
	 */
	public ArrayList<String[]>	obtenerTodoSubtema() {
		ArrayList<String[]> temas = new ArrayList<String[]>();

		oracle.conectar();

		String sql = "select S.IDTEMA, S.NOMBRE, S.IDMATERIA, S.TEMAPADRE , M.CODIGO_MATERIA, M.NOMBRE_MATERIA , M.ORDEN, M.ID_AREA, M.TIENECOMPETENCIAS , M.TIENEPARCIALES, M.TIENEPRUEBAPI, T.NOMBRE from MATERIAS M RIGHT JOIN TEMAS T ON(M.CODIGO_MATERIA  = T.IDMATERIA) left join TEMAS S ON(T.IDTEMA = S.TEMAPADRE ) where S.TEMAPADRE is not null order by S.IDTEMA asc";
		try {
			System.out.println(sql);
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String[] datos = new String[13];	
	//			IDTEMA
				if (rs.getString(1) == null ) {
					datos[0] = "";
				}else{
					datos[0] = rs.getString(1);
				}
	//			NOMBRE/nombre subtema
				if (rs.getString(2) == null ) {
					datos[1] = "";
				}else{
					datos[1] = rs.getString(2);
				}
	//			IDMATERIA
				if (rs.getString(3) == null ) {
					datos[2] = "";
				}else{
					datos[2] = rs.getString(3);
				}
	//			TEMAPADRE
				if (rs.getString(4) == null ) {
					datos[3] = "";
				}else{
					datos[3] = rs.getString(4);
				}
	//			CODIGO_MATERIA
				if (rs.getString(5) == null ) {
					datos[4] = "";
				}else{
					datos[4] = rs.getString(5);
				}
	//			NOMBRE_MATERIA
				if (rs.getString(6) == null ) {
					datos[5] = "";
				}else{
					datos[5] = rs.getString(6);
				}
	//			ORDEN
				if (rs.getString(7) == null ) {
					datos[6] = "";
				}else{
					datos[6] = rs.getString(7);
				}
	//			ID_AREA
				if (rs.getString(8) == null ) {
					datos[7] = "";
				}else{
					datos[7] = rs.getString(8);
				}
	//			TIENECOMPETENCIAS
				if (rs.getString(9) == null ) {
					datos[8] = "";
				}else{
					datos[8] = rs.getString(9);
				}
	//			TIENEPARCIALES
				if (rs.getString(10) == null ) {
					datos[9] = "";
				}else{
					datos[9] = rs.getString(10);
				}
	//			TIENEPRUEBAPI
				if (rs.getString(11) == null ) {
					datos[10] = "";
				}else{
					datos[10] = rs.getString(11);
				}
				//TEMA/nombre tema
				if (rs.getString(12) == null ) {
					datos[11] = "";
				}else{
					datos[11] = rs.getString(12);
				}
				
				temas.add(datos);	
			}
			rs.close();
		} catch (Exception e) {
			temas = null;
			e.printStackTrace();
		}
		oracle.desconectar();
		return temas;
	}
	
	/**
	 * metodo que valida si el tema tiene o no subtemas asociados
	 * @param idTema
	 * @return EXISTE , NO EXISTE, ERROR
	 */
	public String validarSiTemaTieneSubtemas(String idTema){
		oracle.conectar();		
		String sqlVT="select T.IDTEMA, T.NOMBRE, T.IDMATERIA, T.TEMAPADRE FROM TEMAS T WHERE T.TEMAPADRE = '"+idTema+"'";
		ResultSet resultadoVT = null;
		try {
			resultadoVT = oracle.consultar(sqlVT);
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
		try {
			if(resultadoVT.next()){
				oracle.desconectar();
				return "EXISTE";
			}else{
				oracle.desconectar();
				return "NOEXISTE";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "ERROR";
		}

	}
	
	/**
	 * metodo que valida si el tema tiene o no subtemas asociados
	 * @param idTema
	 * @return EXISTE , NO EXISTE, ERROR
	 */
	public String validarSiSubTemaTienePregunta(String idSubtema){
		oracle.conectar();		
		String sqlVT="select IDPREGUNTA, COMPETENCIA, NIVEL, IDDOCENTE, IDGRADO, IDAREA, IDMATERIA, IDSUBTEMA, ENUNCIADO, IMAGEN, ENUNCIADO2 from PREGUNTAS where IDSUBTEMA ='"+idSubtema+"'";
		ResultSet resultadoVT = null;
		try {
			resultadoVT = oracle.consultar(sqlVT);
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
		try {
			if(resultadoVT.next()){
				oracle.desconectar();
				return "EXISTE";
			}else{
				oracle.desconectar();
				return "NOEXISTE";
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "ERROR";
		}

	}
	
	/**
	 * metodo que elimina un tema dando su id interno
	 * @param idTema
	 * @return
	 */
	public boolean eliminarTema(String idTema){
		oracle.conectar();

		String sql = "DELETE FROM TEMAS WHERE IDTEMA = '"+idTema+"' ";
		boolean resultado;
		try {
			resultado = oracle.escribir(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			oracle.desconectar();
			return false;
		}
		oracle.desconectar();
		return resultado;
	}
	
	/**
	 * Obtiene la materia dado su nombre.
	 * 
	 * @param nombre
	 * @return
	 */
	public Materia obtenerMateria(String nombre) {
		Materia materia = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE M.nombre_materia = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materia;
	}

	/**
	 * Obtiene el tema dado su nombre.
	 * 
	 * @param nombre
	 * @return El tema cuyo padre sea <b>null</b>.
	 */
	public Tema obtenerTema(String nombre) {
		Tema tema = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "WHERE T.temaPadre IS NULL AND T.nombre = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_materia"), materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return tema;
	}

	/**
	 * Obtiene el id maximo de la tabla de preguntas.
	 * 
	 * @return El id maximo. Si no hay nada (solo la cabecera del archivo [==
	 *         1]) => retorna 1.
	 */
	public int obtenerIdMaximoTema() {
		int id = -1;

		oracle.conectar();

		String sql = "SELECT MAX(idTema) maxId FROM Temas";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String maxId = rs.getString("maxId");
				if (maxId != null) {
					id = Integer.parseInt(maxId) + 1;
				} else {
					id = 1;
				}
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return id;
	}

	/**
	 * Agrega el tema padre.
	 * 
	 * @param id
	 *            Id del tema padre.
	 * @param nombre
	 *            Nombre del tema.
	 * @param materia
	 *            Materia a la que es asignado.
	 * @throws SQLException
	 */
	public void agregarTemaPadre(int id, String nombre, Materia materia)throws SQLException {
		oracle.conectar();
		String sql = "INSERT INTO Temas (idTema, nombre, idMateria) "+ "VALUES ('" + id + "', '" + nombre + "', '"+ materia.getCodigo() + "')";
		oracle.escribir(sql);

		oracle.desconectar();
	}
	
	
	/**
	 * Agrega el tema hijo.
	 * 
	 * @param id
	 *            Id del tema hijo.
	 * @param nombre
	 *            Nombre del tema.
	 * @param materia
	 *            Materia a la que es asignado.
	 * @param padre
	 *            Id del tema padre.
	 * @throws SQLException
	 */
	public void agregarTemaHijo(int id, String nombre, Materia materia,	Tema padre) throws SQLException {
		oracle.conectar();

		String sql = "INSERT INTO Temas (idTema, nombre, idMateria, temaPadre) "+ "VALUES ('"+ id+ "', '"+ nombre+ "', '"+ materia.getCodigo() + "', '" + padre.getId() + "')";
		oracle.escribir(sql);

		oracle.desconectar();
	}
	
	/**
	 * modificar tema
	 * @param id
	 * @param nombre
	 * @param materia
	 * @throws SQLException
	 */
	public boolean ModificarTema(String id, String nombre, Materia materia, Tema tema){
		oracle.conectar();	
		String temaPadre ="";
		if (tema != null) {
			temaPadre = tema.getId();
		}
		String sql = "update TEMAS set NOMBRE = '"+nombre+"' , IDMATERIA = '"+materia.getCodigo()+"', TEMAPADRE = '"+temaPadre+"' where IDTEMA ='"+id+"'";
		try {
		 	boolean resultado = oracle.escribir(sql);
		 	return resultado;
		} catch (SQLException e) {
			e.printStackTrace();
			oracle.desconectar();
			return false;
		}
		
	}
	
}
