package controller;

import java.sql.ResultSet;

import model.*;
import persistance.*;

public class InformacionUsuarioController {

	private Oracle oracle;

	public InformacionUsuarioController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Obtiene los datos basicos del docente.
	 * 
	 * @param docente
	 * @return
	 */
	public Persona obtenerPersona(Docente docente) {
		Persona persona = null;

		oracle.conectar();

		String sql = "SELECT * FROM Personas " + "WHERE nid = '"
				+ docente.getId() + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				persona = new Persona(rs.getString("tid"), rs.getString("nid"),
						rs.getString("nombres"), rs.getString("apellidos"),
						rs.getString("lugar_nacimiento"),
						rs.getString("direccion"), rs.getString("barrio"),
						rs.getString("telefono"), rs.getString("sexo"),
						rs.getString("correo"), rs.getString("celular"),
						rs.getString("rol"), rs.getString("estado"),
						rs.getString("fecha_registro"),
						rs.getString("fecha_inactivo"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return persona;
	}
}
