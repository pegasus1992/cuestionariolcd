package controller;

import java.util.LinkedList;

import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Filtro {

	public Filtro() {
	
	}
	
	/**
	 * metodo que filtra una tabla por un item de busqueda
	 * @param filtro
	 * @param trsfiltro
	 */
	public void filtro(String filtro,TableRowSorter<DefaultTableModel> trsfiltro) {
	    trsfiltro.setRowFilter(RowFilter.regexFilter(filtro));
	}

	
	/**
	 * metodo que filtra una tabla por una columna
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 */
	public void filtroUnaColumnaMoneda(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , DefaultTableModel dtm , JTable tabla) {
		
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);		
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter(filtro, opcion1));	
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	
	
	/**
	 * metodo que filtra una tabla por una columna
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 */
	public void filtroUnaColumna(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , DefaultTableModel dtm , JTable tabla) {
		
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);		
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter("^"+filtro, opcion1));	
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}	 
	/**
	 * metodo que filtra una tabla por dos item de busqueda 
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 * @param opcion2
	 */
	public void filtroDosColumnas(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , int opcion2, DefaultTableModel dtm  , JTable tabla) {
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);	
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter("^"+filtro, opcion1));
		lista.add(RowFilter.regexFilter("^"+filtro, opcion2));
		
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	
	/**
	 * metodo que filtra una tabla por tres item de busqueda 
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 * @param opcion2
	 * @param opcion3
	 */
	public void filtroTresColumnas(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , int opcion2 ,int opcion3, DefaultTableModel dtm , JTable tabla) {
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);	
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter("^"+filtro, opcion1));
		lista.add(RowFilter.regexFilter("^"+filtro, opcion2));
		lista.add(RowFilter.regexFilter("^"+filtro, opcion3));
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	
	
	/**
	 * metodo que deshace el filtro de la tabla 
	 */
	public void deshacerFiltro(TableRowSorter<DefaultTableModel> trsfiltroTabla, DefaultTableModel dtm_Tabla , JTable tabla){
		
		trsfiltroTabla = new TableRowSorter<DefaultTableModel>(dtm_Tabla);
        // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltroTabla);
		String textoFiltro="";
		filtro(textoFiltro,trsfiltroTabla);
	}
	
	
	/**
	 * metodo que filtra una tabla por dos item de busqueda sin necesariamente empezar con lo escrito
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 * @param opcion2
	 */
	public void filtroDosColumnasQueContenga(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , int opcion2, DefaultTableModel dtm  , JTable tabla) {
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);	
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter(filtro, opcion1));
		lista.add(RowFilter.regexFilter(filtro, opcion2));
		
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	
	/**
	 * metodo que filtra una tabla por tres item de busqueda sin necesariamente empezar con lo escrito
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 * @param opcion2
	 * @param opcion3
	 */
	public void filtroTresColumnasQueContenga(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , int opcion2 ,int opcion3, DefaultTableModel dtm , JTable tabla) {
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);	
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter(filtro, opcion1));
		lista.add(RowFilter.regexFilter(filtro, opcion2));
		lista.add(RowFilter.regexFilter(filtro, opcion3));
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	
	/**
	 * metodo que filtra una tabla por tres item de busqueda sin necesariamente empezar con lo escrito
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 * @param opcion2
	 * @param opcion3
	 * @param opcion4
	 */
	public void filtroCuatroColumnasQueContenga(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , int opcion2 ,int opcion3, int opcion4, DefaultTableModel dtm , JTable tabla) {
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);	
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter(filtro, opcion1));
		lista.add(RowFilter.regexFilter(filtro, opcion2));
		lista.add(RowFilter.regexFilter(filtro, opcion3));
		lista.add(RowFilter.regexFilter(filtro, opcion4));
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	
	/**
	 * metodo que filtra una tabla por una columna sin necesariamente empezar con lo escrito
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 */
	public void filtroUnaColumnaQueContenga(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , DefaultTableModel dtm , JTable tabla) {
		
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);		
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter(filtro, opcion1));	
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}	
	/**
	 * metodo que filtra una tabla por dos item de busqueda el primer campo debe empezar, el segundo que contenga
	 * @param filtro
	 * @param trsfiltro
	 * @param opcion1
	 * @param opcion2
	 */
	public void filtroDosColumnasQueEmpiezeYContenga(String filtro , TableRowSorter<DefaultTableModel> trsfiltro , int opcion1 , int opcion2, DefaultTableModel dtm  , JTable tabla) {
		trsfiltro = new TableRowSorter<DefaultTableModel>(dtm);
	    // A�adimos al Jtable el filtro trsfiltro
		tabla.setRowSorter(trsfiltro);	
		LinkedList<RowFilter<Object,Object>> lista = new LinkedList<RowFilter<Object,Object>>();
		lista.add(RowFilter.regexFilter(filtro, opcion2));
		lista.add(RowFilter.regexFilter("^"+filtro, opcion1));
		
		RowFilter<Object, Object> filtroOr = RowFilter.orFilter(lista);  // or de ambos filtros.
		trsfiltro.setRowFilter(filtroOr);
	}
	

}
