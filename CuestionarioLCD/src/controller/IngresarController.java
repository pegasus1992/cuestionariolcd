package controller;

import java.sql.*;

import model.*;
import persistance.*;

public class IngresarController {

	private Oracle oracle;

	public IngresarController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Autentica al docente ingresado.
	 * 
	 * @param cedula
	 *            Cedula del docente.
	 * @param contrasena
	 *            Contrasena del docente.
	 * @return
	 */
	public Docente autenticar(String cedula, String contrasena) {
		Docente docente = null;

		oracle.conectar();

		String sql = "SELECT * FROM Docentes " + "WHERE nid_docente = '"
				+ cedula + "' AND contrasena = '" + contrasena + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return docente;
	}
}
