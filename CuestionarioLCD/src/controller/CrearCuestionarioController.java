package controller;

import java.sql.*;
import java.util.*;

import model.*;
import persistance.*;

public class CrearCuestionarioController {

	private Oracle oracle;

	public CrearCuestionarioController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Obtiene los grados registrados.
	 * 
	 * @return
	 */
	public ArrayList<Grado> obtenerGrados() {
		ArrayList<Grado> grados;

		oracle.conectar();

		String sql = "SELECT * FROM Grados";
		try {
			grados = new ArrayList<Grado>();
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Grado grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
				grados.add(grado);
			}
			rs.close();
		} catch (Exception e) {
			grados = null;
			e.printStackTrace();
		}
		oracle.desconectar();

		return grados;
	}

	/**
	 * Obtiene las areas registradas.
	 * 
	 * @return
	 */
	public ArrayList<Area> obtenerAreas() {
		ArrayList<Area> areas = new ArrayList<Area>();

		oracle.conectar();

		String sql = "SELECT DISTINCT A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Areas A LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Asignaturas ASG ON (ASG.id_area = A.id_area)";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				areas.add(area);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return areas;
	}

	/**
	 * Obtiene las materias que corresponden al area dada.
	 * 
	 * @param nombreArea
	 * @return
	 */
	public ArrayList<Materia> obtenerMaterias(String nombreArea) {
		ArrayList<Materia> materias = new ArrayList<Materia>();

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Asignaturas ASG ON (ASG.id_area = A.id_area) "
				+ "WHERE A.nombre_area = '" + nombreArea + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				materias.add(materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materias;
	}

	/**
	 * Obtiene las areas a las que tiene acceso el docente dado.
	 * 
	 * @param docente
	 * @return
	 */
	public ArrayList<Area> obtenerAreasDocente(Docente docente) {
		ArrayList<Area> areas = new ArrayList<Area>();

		oracle.conectar();

		String sql = "SELECT DISTINCT A.id_area, A.orden orden_area, A.nombre_area "
				+ "FROM Areas A LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Asignaturas ASG ON (ASG.id_area = A.id_area OR ASG.id_docente = D.id_docente) "
				+ "WHERE ASG.id_docente = '" + docente.getId() + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				areas.add(area);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return areas;
	}

	/**
	 * Obtiene las materias que corresponden al area dada, a las que tiene
	 * acceso el docente dado.
	 * 
	 * @param nombreArea
	 * @param docente
	 * @return
	 */
	public ArrayList<Materia> obtenerMateriasDocente(String nombreArea,
			Docente docente) {
		ArrayList<Materia> materias = new ArrayList<Materia>();

		oracle.conectar();

		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Asignaturas ASG ON (ASG.id_area = A.id_area OR ASG.id_docente = D.id_docente OR ASG.id_materia = M.codigo_materia) "
				+ "WHERE ASG.id_docente = '" + docente.getId() + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				materias.add(materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materias;
	}

	/**
	 * Obtiene las opciones de la pregunta dada.
	 * 
	 * @param pregunta
	 * @return
	 */
	public ArrayList<String[]> obtenerOpciones(String  idPregunta) {
		ArrayList<String[]> opciones = new ArrayList<String[]>();

		oracle.conectar();
		String sql = "SELECT DISTINCT * FROM Opciones WHERE idPregunta = '"+ idPregunta + "' ORDER BY idOpcion";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
			 String[] datos = new String[5];
			 int e = 1;
			 	for (int i = 0; i < 5; i++) {
					if (rs.getString(e) == null) {
						datos[i]="";
					}else{
						datos[i]=rs.getString(e);
					}
					e++;
				}
			 	opciones.add(datos);
//				IDOPCION
//				RESPUESTA
//				CORRECTA
//				IDPREGUNTA
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return opciones;
	}

	/**
	 * Obtiene las preguntas que corresponden al area, materia y grado dados.
	 * 
	 * @param area
	 * @param materia
	 * @param grado
	 * @param nombreTema
	 *            puede ser null.
	 * @param nombreSubtema
	 *            puede ser null.
	 * @return
	 */
	public ArrayList<String[]> obtenerPreguntas(Area area, Materia materia,	Grado grado, String competencia ,String nombreTema, String nombreSubtema, String nivel, String idDocente) {
		ArrayList<String[]> preguntas = new ArrayList<String[]>();
		
		String inicio= "";;
		
		oracle.conectar();
		String condicionArea="";
		if (area == null) {
			condicionArea ="";
		}else{
			condicionArea= " and A.id_area = '"+area.getId()+"' ";
			inicio ="A";
		}
		String condicionAsignatura="";
		if (materia == null) {
			condicionAsignatura ="";
		}else{
			condicionAsignatura= " and M.codigo_materia = '"+materia.getCodigo()+"' ";
			if (inicio.isEmpty()) {
				inicio = "M";
			}
		}
		String condicionGrado ="";
		if (grado == null) {
			condicionGrado ="";
		}else{
			condicionGrado = " and G.nombre ='"+ grado.getNombre()+"' ";
			if (inicio.isEmpty()) {
				inicio = "G";
			}
		}
		String condicionCompetencia ="";
		if (competencia.equals("")) {
			condicionCompetencia ="";
		}else{
			condicionCompetencia = " and P.competencia ='"+ competencia+"' ";
			if (inicio.isEmpty()) {
				inicio = "P";
			}
		}
		String condicionTema ="";
		if (nombreTema.equals("")) {
			condicionTema ="";
		}else{
			condicionTema = " and T.nombre ='"+ nombreTema+"' ";
			if (inicio.isEmpty()) {
				inicio = "T";
			}
		}
		String condicionSubtema="";
		if (nombreSubtema.equals("")) {
			condicionSubtema ="";
		}else{
			condicionSubtema = " and S.nombre ='"+ nombreSubtema+"' ";
			if (inicio.isEmpty()) {
				inicio = "S";
			}
		}
		String condicionNivel= "";
		if (nivel.equals("")) {
			condicionNivel ="";
		}else{
			condicionNivel = " and  P.NIVEL ='"+ nivel+"' ";
			if (inicio.isEmpty()) {
				inicio = "N";
			}
		}
		
		String condicionDocente= "";
		if (idDocente.equals("")) {
			condicionDocente ="";
		}else{
			condicionDocente = " and  P.IDDOCENTE ='"+ idDocente+"' AND P.FECHACREACION like sysdate";
			if (inicio.isEmpty()) {
				inicio = "D";
			}
		}
		
		String donde = "";
		if (!condicionArea.isEmpty() || !condicionAsignatura.isEmpty() || !condicionCompetencia.isEmpty() || !condicionGrado.isEmpty() || !condicionNivel.isEmpty() || !condicionSubtema.isEmpty() || !condicionTema.isEmpty() || !condicionDocente.isEmpty()) {
			donde = " where ";
		}
		
		String condicionFinal ="";
		if (inicio.equals("A")) {
			condicionArea = condicionArea.replace("and", "");
			condicionFinal = donde + condicionArea +condicionAsignatura + condicionCompetencia+ condicionGrado+ condicionNivel + condicionSubtema + condicionTema + condicionDocente;
		}else if(inicio.equals("M")){
			condicionAsignatura = condicionAsignatura.replace("and", "");
			condicionFinal = donde + condicionAsignatura + condicionArea  + condicionCompetencia+ condicionGrado+ condicionNivel + condicionSubtema + condicionTema + condicionDocente;
		}else if(inicio.equals("G")){
			condicionGrado = condicionGrado.replace("and", "");
			condicionFinal = donde + condicionGrado+ condicionAsignatura + condicionArea  + condicionCompetencia+ condicionNivel + condicionSubtema + condicionTema + condicionDocente;
		}else if(inicio.equals("P")){
			condicionCompetencia = condicionCompetencia.replace("and", "");
			condicionFinal = donde + condicionCompetencia+ condicionGrado+ condicionAsignatura + condicionArea  +  condicionNivel + condicionSubtema + condicionTema + condicionDocente;
		}else if(inicio.equals("T")){
			condicionTema = condicionTema.replace("and", "");
			condicionFinal = donde +  condicionTema + condicionCompetencia+ condicionGrado+ condicionAsignatura + condicionArea  +  condicionNivel + condicionSubtema  + condicionDocente;
		}else if(inicio.equals("S")){
			condicionSubtema = condicionSubtema.replace("and", "");
			condicionFinal = donde  + condicionSubtema +  condicionTema + condicionCompetencia+ condicionGrado+ condicionAsignatura + condicionArea  +  condicionNivel + condicionDocente ;
		}else if(inicio.equals("N")){
			condicionNivel = condicionNivel.replace("and", "");
			condicionFinal = donde  +  condicionNivel + condicionSubtema +  condicionTema + condicionCompetencia+ condicionGrado+ condicionAsignatura + condicionArea + condicionDocente ;
		}else if(inicio.equals("D")){
			condicionDocente = condicionDocente.replace("and", "");
			condicionFinal = donde  + condicionDocente +  condicionNivel + condicionSubtema +  condicionTema + condicionCompetencia+ condicionGrado+ condicionAsignatura + condicionArea ;

		}

		
		//                      1                 2         3         4              5               6          7          8             9             10          11                   12                   13             14           15            16           17             18            19                   20               21               22        23             24                   25                       26                          27                        28                       29                            30                   31           32               33                                                                                                                                                                                                 
		String sql = "select P.IDPREGUNTA, P.COMPETENCIA, P.NIVEL, P.IDDOCENTE IDDOCENTEPREGUNTA, P.IDGRADO, P.IDAREA, P.IDMATERIA, P.IDSUBTEMA, P.ENUNCIADO, P.ENUNCIADO2, A.ID_DOCENTE IDDOCENTEAREA , A.NOMBRE_AREA, A.ORDEN, M.CODIGO_MATERIA, M.ID_AREA, M.NOMBRE_MATERIA, M.ORDEN, M.TIENECOMPETENCIAS, M.TIENEPARCIALES, M.TIENEPRUEBAPI, G.COD_GRADO, G.ID_GRADO, G.NIVEL NIVELGRADO, G.NOMBRE NOMBREGRADO, S.IDMATERIA IDMATERIASUBTEMA , S.IDTEMA IDTEMASUBTEMA, S.NOMBRE NOMBRESUBTEMA, S.TEMAPADRE TEMAPADRESUBTEMA, T.IDMATERIA IDMATERISTEMA, T.IDTEMA , T.NOMBRE NOMBRETEMA, P.IMAGEN  from PREGUNTAS P left join AREAS A on(P.IDAREA = A.ID_AREA) LEFT JOIN MATERIAS M ON(P.IDMATERIA = M.CODIGO_MATERIA) LEFT JOIN GRADOS G  on(P.IDGRADO = G.ID_GRADO) LEFT JOIN TEMAS S ON(P.IDSUBTEMA = S.IDTEMA) LEFT JOIN TEMAS T ON(S.TEMAPADRE = T.IDTEMA )"+
						condicionFinal;
				try {
			System.out.println(sql);
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String[] datos = new String[33];
				int e = 1;
				for (int i = 0; i < 32; i++) {
					System.out.println(e);
					if (rs.getString(e) == null) {
						datos[i] = "";
					}else{
						datos[i] = rs.getString(e);
					}
					e++;
					
					}
				preguntas.add(datos);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return preguntas;
	}

	/**
	 * Obtiene la pregunta dado su id.
	 * 
	 * @param idPregunta
	 * @return
	 */
	public Pregunta obtenerPregunta(String idPregunta) {
		Pregunta pregunta = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, T2.idTema idTemaPadre, T2.nombre nombreTemaPadre, "
				+ "P.idPregunta, P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE P.idPregunta = '" + idPregunta + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Grado grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				Tema subtema = new Tema(rs.getString("idTemaPadre"),
						rs.getString("nombreTemaPadre"), materia, tema);
				pregunta = new Pregunta(rs.getString("idPregunta"),
						rs.getString("competencia"), rs.getString("nivel"),
						docente, grado, area, materia, subtema,
						rs.getString("enunciado"), rs.getString("imagen"),
						rs.getString("enunciado2"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return pregunta;
	}

	/**
	 * Obtiene el area dado su nombre.
	 * 
	 * @param nombreArea
	 * @return
	 */
	public Area obtenerArea(String nombreArea) {
		Area area = null;

		oracle.conectar();

		String sql = "SELECT DISTINCT A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Areas A LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE A.nombre_area = '" + nombreArea + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return area;
	}

	/**
	 * Obtiene el grado dado su nombre.
	 * 
	 * @param nombre
	 * @return
	 */
	public Grado obtenerGrado(String nombre) {
		Grado grado = null;

		oracle.conectar();
		String sql = "SELECT * FROM Grados WHERE nombre = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return grado;
	}

	/**
	 * Obtiene la materia dado su nombre.
	 * 
	 * @param nombre
	 * @return
	 */
	public Materia obtenerMateria(String nombre) {
		Materia materia = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE M.nombre_materia = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materia;
	}

	/**
	 * Obtiene el tema dado su id.
	 * 
	 * @param idTema
	 * @return El tema cuyo padre [3] <b>NO</b> sea <b>null</b>.
	 */
	public Tema obtenerSubtema(String idTema) {
		Tema subtema = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, T2.idTema idTemaPadre, T2.nombre nombreTemaPadre, "
				+ "P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NOT NULL AND T2.idTema = '"
				+ idTema
				+ "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				subtema = new Tema(rs.getString("idTemaPadre"),
						rs.getString("nombreTemaPadre"), materia, tema);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return subtema;
	}

	/**
	 * Obtiene el tema dado su id.
	 * 
	 * @param idTema
	 * @return El tema cuyo padre [3] sea <b>null</b>.
	 */
	public Tema obtenerTema(String idTema) {
		Tema tema = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, "
				+ "P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NULL AND T.idTema = '" + idTema + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return tema;
	}

	/**
	 * Obtiene los temas que corresponden al nombre de materia dado.
	 * 
	 * @param nombreMateria
	 * @return El tema cuyo padre [3] sea <b>null</b>.
	 */
	public ArrayList<Tema> obtenerTemas(String nombreMateria) {
		ArrayList<Tema> temas;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "LEFT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "WHERE T.temaPadre IS NULL AND M.nombre_materia = '"
				+ nombreMateria + "'";
		try {
			temas = new ArrayList<Tema>();
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				temas.add(tema);
			}
			rs.close();
		} catch (Exception e) {
			temas = null;
			e.printStackTrace();
		}

		oracle.desconectar();

		return temas;
	}

	/**
	 * Obtiene los subtemas que corresponden al nombre de tema dado.
	 * 
	 * @param nombreTema
	 * @return
	 */
	public ArrayList<Tema> obtenerSubtemas(String nombreTema) {
		ArrayList<Tema> subtemas = new ArrayList<Tema>();

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, T.idTema, T.nombre nombre_tema "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "WHERE T.temaPadre IS NOT NULL AND T2.nombre = '"
				+ nombreTema + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema subtema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				subtemas.add(subtema);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return subtemas;
	}

	/**
	 * Obtiene el tema dado su nombre.
	 * 
	 * @param nombre
	 * @return El tema cuyo padre sea <b>null</b>.
	 */
	public Tema obtenerTemaNombre(String nombre) {
		Tema tema = null;

		// oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, "
				+ "P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NULL AND T.nombre = '" + nombre + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// oracle.desconectar();

		return tema;
	}

	/**
	 * Obtiene el tema dado el nombre de la materia.
	 * 
	 * @param nombre
	 * @return El tema cuyo padre <b>NO</b> sea <b>null</b>.
	 */
	public Tema obtenerSubtemaNombre(String nombre) {
		Tema subtema = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, T2.idTema idTemaPadre, T2.nombre nombreTemaPadre, "
				+ "P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NOT NULL AND T2.nombre = '"
				+ nombre
				+ "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				subtema = new Tema(rs.getString("idTemaPadre"),
						rs.getString("nombreTemaPadre"), materia, tema);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// oracle.desconectar();

		return subtema;
	}
	
	/**
	 * Obtiene el id maximo de la tabla de opciones.
	 * 
	 * @return El id maximo. Si no hay nada (solo la cabecera del archivo [==
	 *         1]) => retorna 1.
	 */
	public int obtenerIdMaximoCuestionario() {
		int id = -1;

		oracle.conectar();

		String sql = "SELECT MAX(IDCUESTIONARIO) FROM CUESTIONARIO";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				String maxId = rs.getString(1);
				if (maxId != null) {
					id = Integer.parseInt(maxId) + 1;
				} else {
					id = 1;
				}
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return id;
	}
	
	
	/**
	 * metodo que angrega un cuestionario
	 * @param idPregunta
	 * @throws SQLException
	 */
	public boolean agregarCuestionario(int idCuestionario, String[] idPreguntas){
		oracle.conectar();
		ArrayList<String> querys = new ArrayList<String>();
 		querys.add( "INSERT INTO CUESTIONARIO (IDCUESTIONARIO)" + "VALUES ('" + idCuestionario + "')");
 		int en=1;
		for (int i = 0; i < idPreguntas.length; i++) {
			querys.add("insert into CUESTIONARIOPREGUNTA ( IDCUESTIONARIO, IDPREGUNTA, ORDEN ) values ( '"+idCuestionario+"', '"+idPreguntas[i]+"', '"+en+"')");
			en++;
		}
		boolean result;
		try {
			result = oracle.escribirMulti(querys);
		} catch (SQLException e) {
			oracle.desconectar();
			e.printStackTrace();
			return false;
		}
		oracle.desconectar();
		return result;
	}
	
}
