package controller;

import java.sql.*;
import java.util.*;

import model.*;
import persistance.*;

public class ConsultarPreguntaController {

	private Oracle oracle;

	public ConsultarPreguntaController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Obtiene la pregunta dado su id.
	 * 
	 * @param idPregunta
	 * @return
	 */
	public Pregunta obtenerPregunta(String idPregunta) {
		Pregunta pregunta = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, T2.idTema idTemaPadre, T2.nombre nombreTemaPadre, "
				+ "P.idPregunta, P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE P.idPregunta = '" + idPregunta + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Grado grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				Tema subtema = new Tema(rs.getString("idTemaPadre"),
						rs.getString("nombreTemaPadre"), materia, tema);
				pregunta = new Pregunta(rs.getString("idPregunta"),
						rs.getString("competencia"), rs.getString("nivel"),
						docente, grado, area, materia, subtema,
						rs.getString("enunciado"), rs.getString("imagen"),
						rs.getString("enunciado2"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return pregunta;
	}

	/**
	 * Obtiene las opciones de la pregunta dada.
	 * 
	 * @param pregunta
	 * @return Todas las opciones que esten enlazadas a la pregunta dada.
	 */
	public ArrayList<Opcion> obtenerOpciones(Pregunta pregunta) {
		ArrayList<Opcion> opciones = new ArrayList<Opcion>();

		oracle.conectar();
		String sql = "SELECT O.* FROM Opciones O "
				+ "LEFT JOIN Preguntas P ON P.idPregunta = O.idPregunta "
				+ "WHERE P.idPregunta = '" + pregunta.getId() + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Opcion opcion = new Opcion(rs.getString("idOpcion"),
						rs.getString("respuesta"), rs.getString("correcta"),
						pregunta);
				opciones.add(opcion);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return opciones;
	}

	/**
	 * Obtiene las preguntas registradas.
	 * 
	 * @return
	 */
	public ArrayList<Pregunta> obtenerPreguntas() {
		ArrayList<Pregunta> preguntas = new ArrayList<Pregunta>();

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, T2.idTema idTemaPadre, T2.nombre nombreTemaPadre, "
				+ "P.idPregunta, P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NOT NULL";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Grado grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				Tema subtema = new Tema(rs.getString("idTemaPadre"),
						rs.getString("nombreTemaPadre"), materia, tema);
				Pregunta pregunta = new Pregunta(rs.getString("idPregunta"),
						rs.getString("competencia"), rs.getString("nivel"),
						docente, grado, area, materia, subtema,
						rs.getString("enunciado"), rs.getString("imagen"),
						rs.getString("enunciado2"));
				preguntas.add(pregunta);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return preguntas;
	}

	/**
	 * Obtiene el grado dado su id.
	 * 
	 * @param idGrado
	 * @return
	 */
	public Grado obtenerGrado(String idGrado) {
		Grado grado = null;

		oracle.conectar();
		String sql = "SELECT * FROM Grados WHERE id_grado = '" + idGrado + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				grado = new Grado(rs.getString("id_grado"),
						rs.getString("cod_grado"), rs.getString("nombre"),
						rs.getString("nivel"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return grado;
	}

	/**
	 * Obtiene el area dado su id.
	 * 
	 * @param idArea
	 * @return
	 */
	public Area obtenerArea(String idArea) {
		Area area = null;

		oracle.conectar();

		String sql = "SELECT DISTINCT A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Areas A LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE A.id_area = '" + idArea + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return area;
	}

	/**
	 * Obtiene la materia dado su id.
	 * 
	 * @param idMateria
	 * @return
	 */
	public Materia obtenerMateria(String idMateria) {
		Materia materia = null;

		oracle.conectar();

		String sql = "SELECT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.* "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "WHERE M.codigo_materia = '" + idMateria + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return materia;
	}

	/**
	 * Obtiene el tema dado su id.
	 * 
	 * @param idTema
	 * @return El tema cuyo padre <b>NO</b> sea <b>null</b>.
	 */
	public Tema obtenerSubtema(String idTema) {
		Tema subtema = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, T2.idTema idTemaPadre, T2.nombre nombreTemaPadre, "
				+ "P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "LEFT JOIN Temas T2 ON T.temaPadre = T2.idTema "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NOT NULL AND T2.idTema = '"
				+ idTema
				+ "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				Tema tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
				subtema = new Tema(rs.getString("idTemaPadre"),
						rs.getString("nombreTemaPadre"), materia, tema);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return subtema;
	}

	/**
	 * Obtiene el tema dado su id.
	 * 
	 * @param idTema
	 * @return El tema cuyo padre sea <b>null</b>.
	 */
	public Tema obtenerTema(String idTema) {
		Tema tema = null;

		oracle.conectar();
		String sql = "SELECT DISTINCT M.codigo_materia, M.nombre_materia, M.orden orden_materia, M.tieneCompetencias, M.tieneParciales, M.tienePruebaPi, "
				+ "A.id_area, A.orden orden_area, A.nombre_area, D.*, G.*, T.idTema, T.nombre nombre_tema, "
				+ "P.competencia, P.nivel nivel_pregunta, P.enunciado, imagen, enunciado2 "
				+ "FROM Materias M LEFT JOIN Areas A ON M.id_area = A.id_area "
				+ "LEFT JOIN Docentes D ON A.id_docente = D.id_docente "
				+ "RIGHT JOIN Temas T ON T.idMateria = M.codigo_materia "
				+ "RIGHT JOIN Preguntas P ON (A.id_area = P.idArea AND M.codigo_materia = P.idMateria) "
				+ "LEFT JOIN Grados G ON G.id_grado = P.idGrado "
				+ "WHERE T.temaPadre IS NULL AND T.idTema = '" + idTema + "'";
		try {
			ResultSet rs = oracle.consultar(sql);
			while (rs.next()) {
				Docente docente = new Docente(rs.getString("id_docente"),
						rs.getString("tid_docente"),
						rs.getString("nid_docente"),
						rs.getString("contrasena"), rs.getString("escalafon"),
						rs.getString("titulo"), rs.getString("universidad"),
						rs.getString("anio_graduacion"),
						rs.getString("fecha_contratacion"),
						rs.getString("nucleo_familiar"),
						rs.getString("colegio"), rs.getString("especialidad"));
				Area area = new Area(rs.getString("id_area"),
						rs.getString("orden_area"),
						rs.getString("nombre_area"), docente);
				Materia materia = new Materia(rs.getString("codigo_materia"),
						rs.getString("nombre_materia"),
						rs.getString("orden_materia"), area,
						rs.getString("tieneCompetencias"),
						rs.getString("tieneParciales"),
						rs.getString("tienePruebaPi"));
				tema = new Tema(rs.getString("idTema"),
						rs.getString("nombre_tema"), materia);
			}
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		oracle.desconectar();

		return tema;
	}
}
