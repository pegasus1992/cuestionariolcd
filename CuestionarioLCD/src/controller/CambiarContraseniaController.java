package controller;

import java.sql.SQLException;

import model.*;
import persistance.*;

public class CambiarContraseniaController {

	private Oracle oracle;

	public CambiarContraseniaController() {
		oracle = Oracle.getInstance();
	}

	/**
	 * Permite cambiar la contrasenia de un docente dado.
	 * 
	 * @param doc
	 *            docente.
	 * @param nueva
	 *            contrasenia nueva.
	 * @throws SQLException
	 */
	public void cambiarContrasenia(Docente doc, String nueva) throws SQLException {
		oracle.conectar();
		String sql = "UPDATE Docentes SET contrasena = '" + nueva
				+ "' WHERE id_docente = '" + doc.getId() + "'";
		oracle.escribir(sql);
		oracle.desconectar();
	}
}
