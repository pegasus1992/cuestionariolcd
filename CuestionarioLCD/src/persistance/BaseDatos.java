package persistance;

public abstract class BaseDatos {

	/**
	 * Permite conectar a la BD.
	 */
	public abstract void conectar();

	/**
	 * Permite desconectar la BD.
	 */
	public abstract void desconectar();
}
