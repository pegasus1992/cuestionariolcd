package persistance;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * Clase encargada de gestionar el acceso a una BD de Oracle.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Oracle extends BaseDatos {

	private static final String direccionConfig = "config.txt";
	private static Oracle instance = null;
	public static String rutaImagenes ="";
	public static String rutaRepositorioBaseReportes = "";
	private Connection conexion;
	private String direccion, puerto;

	public static Oracle getInstance() {
		if (instance == null) {
			String ip = null, puerto = null;
			try {
				File archivo = new File(direccionConfig);
				FileReader fr = new FileReader(archivo);
				BufferedReader br = new BufferedReader(fr);

				String linea;
				int fila = 0;

				while ((linea = br.readLine()) != null) {
					if (fila == 0) {
						ip = linea;
					} else if (fila == 1) {
						puerto = linea;
					}else if (fila == 2) {
						rutaImagenes = linea;
					}else if (fila == 3) {
						rutaRepositorioBaseReportes = linea;
					}
					fila++;
				}

				br.close();
				fr.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (ip != null && puerto != null) {
				instance = new Oracle(ip, puerto);
			}
		}
		return instance;
	}

	private Oracle(String ip, String puerto) {
		this.direccion = ip;
		this.puerto = puerto;
	}

	public Connection getConexion() {
		return conexion;
	}

	private void setConexion(Connection conexion) {
		this.conexion = conexion;
	}

	@Override
	public void conectar() {
		Connection conn = null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			String instruccionConectar = "jdbc:oracle:thin:@" + direccion + ":"
					+ puerto + ":xe";
			conn = DriverManager.getConnection(instruccionConectar, "system",
					"CD1986");
			conn.setAutoCommit(false);
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
		setConexion(conn);
	}

	public ResultSet consultar(String sql) throws Exception {
		ResultSet resultado = null;
		try {
			Statement sentencia = getConexion().createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			resultado = sentencia.executeQuery(sql);
		} catch (Exception e) {
			throw new SQLException(e);
		}
		return resultado;
	}

	public boolean escribir(String sql) throws SQLException {
		try {
			Statement sentencia = getConexion().createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			sentencia.executeUpdate(sql);
			getConexion().commit();
			sentencia.close();
		} catch (SQLException e) {
			e.printStackTrace();
			getConexion().rollback();
			return false;
		}
		return true;
	}

	public boolean escribirMulti(ArrayList<String> sqls) throws SQLException {
		try {
			Statement sentencia = getConexion().createStatement(
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			for (int i = 0; i < sqls.size(); i++) {
				String sql = sqls.get(i);
				sentencia.executeUpdate(sql);
			}
			getConexion().commit();
			sentencia.close();
		} catch (SQLException e) {
			getConexion().rollback();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void desconectar() {
		try {
			getConexion().close();
			setConexion(null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
