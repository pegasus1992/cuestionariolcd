package exception;

public class FechaException extends Exception {
	private static final long serialVersionUID = 3690221242326933021L;

	public FechaException(String message) {
		super(message);
	}
}
