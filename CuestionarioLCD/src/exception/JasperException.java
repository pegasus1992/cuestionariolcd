package exception;

public class JasperException extends Exception {
	private static final long serialVersionUID = -2119696880727481099L;

	public static final String mensajeErrorTablaVacia = "No hay nada que mostrar.";
	public static final String mensajeErrorCompilacion = "Ha ocurrido un error en la compilación del reporte.";
	public static final String mensajeErrorCarga = "Ha ocurrido un error en la carga del reporte.";

	public JasperException(String message) {
		super(message);
	}
}
