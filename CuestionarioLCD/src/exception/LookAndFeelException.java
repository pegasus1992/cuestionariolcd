package exception;

public class LookAndFeelException extends Exception {
	private static final long serialVersionUID = 3547231563750737389L;

	public LookAndFeelException(String message) {
		super(message);
	}
}
