package model;

/**
 * Clase encargada de almacenar las personas.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Persona {

	private String tid, nid, nombres, apellidos, lugarNacimiento, direccion,
			barrio, telefono, sexo, correo, celular, rol, estado,
			fechaRegistro, fechaInactivo;

	public Persona(String tid, String nid, String nombres, String apellidos,
			String lugarNacimiento, String direccion, String barrio,
			String telefono, String sexo, String correo, String celular,
			String rol, String estado, String fechaRegistro,
			String fechaInactivo) {

		setTid(tid);
		setNid(nid);
		setNombres(nombres);
		setApellidos(apellidos);
		setLugarNacimiento(lugarNacimiento);
		setDireccion(direccion);
		setBarrio(barrio);
		setTelefono(telefono);
		setSexo(sexo);
		setCorreo(correo);
		setCelular(celular);
		setRol(rol);
		setEstado(estado);
		setFechaRegistro(fechaRegistro);
		setFechaInactivo(fechaInactivo);
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFechaInactivo() {
		return fechaInactivo;
	}

	public void setFechaInactivo(String fechaInactivo) {
		this.fechaInactivo = fechaInactivo;
	}
}
