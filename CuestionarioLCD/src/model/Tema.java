package model;

/**
 * Clase encargada de almacenar los temas y subtemas.<br>
 * Tema: Cuyo tema padre <b>SI</b> sea <b>null</b>.<br>
 * Subtema: Cuyo tema padre <b>NO</b> sea <b>null</b>.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Tema {

	private String id, nombre;
	private Tema padre;
	private Materia materia;

	public Tema(String id, String nombre, Materia materia, Tema padre) {
		setId(id);
		setNombre(nombre);
		setPadre(padre);
		setMateria(materia);
	}

	public Tema(String id, String nombre, Materia materia) {
		this(id, nombre, materia, null);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Tema getPadre() {
		return padre;
	}

	public void setPadre(Tema padre) {
		this.padre = padre;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}
}
