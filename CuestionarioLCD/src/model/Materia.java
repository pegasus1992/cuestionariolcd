package model;

/**
 * Clase encargada de almacenar las materias.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Materia {

	private String codigo, nombre, orden, tieneCompetencias, tieneParciales,
			tienePruebaPi;
	private Area area;

	public Materia(String codigo, String nombre, String orden, Area area,
			String tieneCompetencias, String tieneParciales,
			String tienePruebaPi) {
		setCodigo(codigo);
		setNombre(nombre);
		setOrden(orden);
		setArea(area);
		setTieneCompetencias(tieneCompetencias);
		setTieneParciales(tieneParciales);
		setTienePruebaPi(tienePruebaPi);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public String getTieneCompetencias() {
		return tieneCompetencias;
	}

	public void setTieneCompetencias(String tieneCompetencias) {
		this.tieneCompetencias = tieneCompetencias;
	}

	public String getTieneParciales() {
		return tieneParciales;
	}

	public void setTieneParciales(String tieneParciales) {
		this.tieneParciales = tieneParciales;
	}

	public String getTienePruebaPi() {
		return tienePruebaPi;
	}

	public void setTienePruebaPi(String tienePruebaPi) {
		this.tienePruebaPi = tienePruebaPi;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
}
