package model;

/**
 * Clase encargada de almacenar las areas.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Area {

	private String id, orden, nombre;
	private Docente docente;

	public Area(String id, String orden, String nombre, Docente docente) {
		setId(id);
		setOrden(orden);
		setNombre(nombre);
		setDocente(docente);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Docente getDocente() {
		return docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}
}
