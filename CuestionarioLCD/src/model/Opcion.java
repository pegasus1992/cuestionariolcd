package model;

/**
 * Clase encargada de almacenar las opciones de respuesta.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Opcion {

	private String id, respuesta, correcta;
	private Pregunta pregunta;

	public Opcion(String id, String respuesta, String correcta,
			Pregunta pregunta) {

		setId(id);
		setRespuesta(respuesta);
		setCorrecta(correcta);
		setPregunta(pregunta);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getCorrecta() {
		return correcta;
	}

	public void setCorrecta(String correcta) {
		this.correcta = correcta;
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}
}
