package model;

/**
 * Clase encargada de almacenar los docentes.<br>
 * En la BD, hereda de Personas.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Docente {

	private String id, tid, nid, contrasena, escalafon, titulo, universidad,
			anioGraduacion, fechaContratacion, nucleoFamiliar, colegio,
			especialidad;

	public Docente(String id, String tid, String nid, String contrasena,
			String escalafon, String titulo, String universidad,
			String anioGraduacion, String fechaContratacion,
			String nucleoFamiliar, String colegio, String especialidad) {

		setId(id);
		setTid(tid);
		setNid(nid);
		setContrasena(contrasena);
		setEscalafon(escalafon);
		setTitulo(titulo);
		setUniversidad(universidad);
		setAnioGraduacion(anioGraduacion);
		setFechaContratacion(fechaContratacion);
		setNucleoFamiliar(nucleoFamiliar);
		setColegio(colegio);
		setEspecialidad(especialidad);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getEscalafon() {
		return escalafon;
	}

	public void setEscalafon(String escalafon) {
		this.escalafon = escalafon;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getUniversidad() {
		return universidad;
	}

	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}

	public String getAnioGraduacion() {
		return anioGraduacion;
	}

	public void setAnioGraduacion(String anioGraduacion) {
		this.anioGraduacion = anioGraduacion;
	}

	public String getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public String getNucleoFamiliar() {
		return nucleoFamiliar;
	}

	public void setNucleoFamiliar(String nucleoFamiliar) {
		this.nucleoFamiliar = nucleoFamiliar;
	}

	public String getColegio() {
		return colegio;
	}

	public void setColegio(String colegio) {
		this.colegio = colegio;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
}
