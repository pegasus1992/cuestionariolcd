package model;

/**
 * Clase encargada de almacenar los grados.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Grado {

	private String id, codigo, nombre, nivel;

	public Grado(String id, String codigo, String nombre, String nivel) {
		setId(id);
		setCodigo(codigo);
		setNombre(nombre);
		setNivel(nivel);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
}
