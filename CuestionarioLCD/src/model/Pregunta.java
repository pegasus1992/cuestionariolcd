package model;

/**
 * Clase encargada de almacenar las preguntas.
 * 
 * @author Julian Gonzalez Prieto(Avuuna, la Luz del Alba)
 *
 */
public class Pregunta {

	private String id, competencia, nivel, enunciado, rutaImagen,
			enunciadoEspecifico;
	private Docente docente;
	private Grado grado;
	private Area area;
	private Materia materia;
	private Tema subtema;

	public Pregunta(String id, String competencia, String nivel,
			Docente docente, Grado grado, Area area, Materia materia,
			Tema subtema, String enunciado, String rutaImagen,
			String enunciadoEspecifico) {

		setId(id);
		setCompetencia(competencia);
		setNivel(nivel);
		setDocente(docente);
		setGrado(grado);
		setArea(area);
		setMateria(materia);
		setSubtema(subtema);
		setEnunciado(enunciado);
		setRutaImagen(rutaImagen);
		setEnunciadoEspecifico(enunciadoEspecifico);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompetencia() {
		return competencia;
	}

	public void setCompetencia(String competencia) {
		this.competencia = competencia;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getEnunciado() {
		return enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

	public String getEnunciadoEspecifico() {
		return enunciadoEspecifico;
	}

	public void setEnunciadoEspecifico(String enunciadoEspecifico) {
		this.enunciadoEspecifico = enunciadoEspecifico;
	}

	public Docente getDocente() {
		return docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}

	public Grado getGrado() {
		return grado;
	}

	public void setGrado(Grado grado) {
		this.grado = grado;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Tema getSubtema() {
		return subtema;
	}

	public void setSubtema(Tema subtema) {
		this.subtema = subtema;
	}
}
